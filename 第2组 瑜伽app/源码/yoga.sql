/*
Navicat MySQL Data Transfer

Source Server         : yoga
Source Server Version : 50728
Source Host           : rm-bp12yzr6xqsaco059ko.mysql.rds.aliyuncs.com:3306
Source Database       : yoga

Target Server Type    : MYSQL
Target Server Version : 50728
File Encoding         : 65001

Date: 2020-06-14 18:54:17
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for buy_classes
-- ----------------------------
DROP TABLE IF EXISTS `buy_classes`;
CREATE TABLE `buy_classes` (
  `bc_id` int(255) NOT NULL AUTO_INCREMENT COMMENT '用户购买会馆课程id',
  `trainee_id` int(255) DEFAULT NULL COMMENT '学员id',
  `class_id` int(255) DEFAULT NULL COMMENT '课程id',
  `buy_time` datetime DEFAULT NULL COMMENT '购买时间',
  `status` int(3) DEFAULT NULL COMMENT '1:已完成 2:申请退款 3:已退款 4:退款失败 5:已过期',
  `pay_money` double(10,0) DEFAULT NULL COMMENT '实付金额',
  `get_money` double(10,0) DEFAULT NULL COMMENT '实收金额',
  PRIMARY KEY (`bc_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of buy_classes
-- ----------------------------
INSERT INTO `buy_classes` VALUES ('1', '1', '1', '2020-06-06 16:34:25', '1', '300', '500');
INSERT INTO `buy_classes` VALUES ('2', '2', '2', '2020-06-07 22:20:15', '1', '400', '600');
INSERT INTO `buy_classes` VALUES ('3', '3', '3', '2020-06-07 22:20:34', '1', '400', '400');
INSERT INTO `buy_classes` VALUES ('4', '4', '4', '2020-06-08 00:33:19', '1', '600', '600');

-- ----------------------------
-- Table structure for buy_product
-- ----------------------------
DROP TABLE IF EXISTS `buy_product`;
CREATE TABLE `buy_product` (
  `bp_id` int(255) NOT NULL AUTO_INCREMENT COMMENT '用户购买会馆产品表id',
  `trainee_id` int(255) DEFAULT NULL COMMENT '学员id',
  `product_id` int(255) DEFAULT NULL COMMENT '产品id',
  `dead_time` datetime DEFAULT NULL COMMENT '到期时间',
  `buy_time` datetime DEFAULT NULL COMMENT '购买时间',
  `pay_money` double(10,0) DEFAULT NULL COMMENT '实付金额',
  `get_money` double(10,0) DEFAULT NULL COMMENT '实收金额',
  PRIMARY KEY (`bp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of buy_product
-- ----------------------------
INSERT INTO `buy_product` VALUES ('33', '2', '9', '2020-06-20 15:25:11', '2020-06-13 15:25:11', '500', null);

-- ----------------------------
-- Table structure for charge
-- ----------------------------
DROP TABLE IF EXISTS `charge`;
CREATE TABLE `charge` (
  `charge_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `money` double(10,0) DEFAULT NULL,
  `charge_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`charge_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of charge
-- ----------------------------
INSERT INTO `charge` VALUES ('1', '1', '2000', null);
INSERT INTO `charge` VALUES ('2', '2', '4000', null);
INSERT INTO `charge` VALUES ('3', '3', '3000', null);
INSERT INTO `charge` VALUES ('4', '4', '5000', null);

-- ----------------------------
-- Table structure for circle_friends
-- ----------------------------
DROP TABLE IF EXISTS `circle_friends`;
CREATE TABLE `circle_friends` (
  `bbs_id` int(255) NOT NULL AUTO_INCREMENT COMMENT ' 盆友圈id',
  `user_id` int(255) DEFAULT NULL COMMENT '发帖人',
  `img` varchar(255) DEFAULT NULL COMMENT '资源地址',
  `content` varchar(255) DEFAULT NULL COMMENT '发帖内容',
  `publish_time` datetime DEFAULT NULL COMMENT '发帖日期',
  `father_id` int(255) DEFAULT NULL COMMENT '跟帖id',
  PRIMARY KEY (`bbs_id`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of circle_friends
-- ----------------------------
INSERT INTO `circle_friends` VALUES ('1', '3', 'http://qax8jdy42.bkt.clouddn.com/te3.jpg', '发发发', '2020-06-01 15:20:06', null);
INSERT INTO `circle_friends` VALUES ('2', '2', 'http://qax8jdy42.bkt.clouddn.com/te3.jpg', '打发', '2020-06-02 15:20:11', '1');
INSERT INTO `circle_friends` VALUES ('3', '5', 'http://qax8jdy42.bkt.clouddn.com/te3.jpg', '忽高忽低', '2020-05-21 15:20:13', '1');
INSERT INTO `circle_friends` VALUES ('4', '4', 'http://qax8jdy42.bkt.clouddn.com/te3.jpg', '小白嘻嘻嘻', '2020-05-21 15:20:13', '1');
INSERT INTO `circle_friends` VALUES ('5', '10', 'http://qax8jdy42.bkt.clouddn.com/te3.jpg', '小白笑啥呢？', '2020-06-04 16:48:11', null);
INSERT INTO `circle_friends` VALUES ('6', '2', 'http://qax8jdy42.bkt.clouddn.com/te3.jpg', '傻逼吗？', '2020-06-04 17:11:34', '5');
INSERT INTO `circle_friends` VALUES ('7', '3', 'http://qax8jdy42.bkt.clouddn.com/te3.jpg', '申诉', '2020-06-04 18:17:53', '5');
INSERT INTO `circle_friends` VALUES ('8', '4', 'http://qax8jdy42.bkt.clouddn.com/te3.jpg', '垃圾教练', '2020-06-04 18:19:28', '5');
INSERT INTO `circle_friends` VALUES ('44', '3', null, '经历过', '2020-06-10 14:33:15', '5');
INSERT INTO `circle_friends` VALUES ('45', '3', null, '不好', '2020-06-10 18:58:17', '5');
INSERT INTO `circle_friends` VALUES ('46', '3', null, '哎呀', '2020-06-10 21:57:15', '5');
INSERT INTO `circle_friends` VALUES ('47', '3', null, '垃圾', '2020-06-10 22:10:11', '5');
INSERT INTO `circle_friends` VALUES ('48', '3', null, '555', '2020-06-11 13:25:17', '5');
INSERT INTO `circle_friends` VALUES ('49', '3', null, '办公', '2020-06-11 17:24:37', '5');
INSERT INTO `circle_friends` VALUES ('50', '3', null, '考虑考虑', '2020-06-11 19:31:22', '5');
INSERT INTO `circle_friends` VALUES ('51', '3', null, '图图他', '2020-06-11 23:48:02', '5');
INSERT INTO `circle_friends` VALUES ('52', '3', null, '8', '2020-06-12 00:29:35', '5');
INSERT INTO `circle_friends` VALUES ('53', '3', null, '滚滚滚', '2020-06-12 01:13:14', '5');
INSERT INTO `circle_friends` VALUES ('54', '10', null, '号楼', '2020-06-13 14:55:31', '1');
INSERT INTO `circle_friends` VALUES ('55', '3', null, '才', '2020-06-13 15:00:50', '5');
INSERT INTO `circle_friends` VALUES ('56', '10', null, '虎牙', '2020-06-13 15:03:27', '1');
INSERT INTO `circle_friends` VALUES ('57', '10', null, '垃圾', '2020-06-13 15:07:42', '1');

-- ----------------------------
-- Table structure for classes
-- ----------------------------
DROP TABLE IF EXISTS `classes`;
CREATE TABLE `classes` (
  `classes_id` int(255) NOT NULL AUTO_INCREMENT COMMENT '场馆排课id',
  `class_name` varchar(50) DEFAULT NULL COMMENT '课程名字',
  `des` varchar(255) DEFAULT NULL COMMENT '流派',
  `hall_id` int(5) DEFAULT NULL,
  `coach_id` int(5) DEFAULT NULL COMMENT '教练id',
  `class_price` double(255,2) DEFAULT NULL COMMENT '课程价格',
  `start_time` date DEFAULT NULL COMMENT '上课时间',
  `end_time` date DEFAULT NULL,
  `week_time` varchar(255) DEFAULT NULL,
  `day_time` varchar(255) DEFAULT NULL,
  `total_people` int(50) DEFAULT NULL COMMENT '课程总人数',
  `note` varchar(255) DEFAULT NULL,
  `status` int(3) DEFAULT NULL,
  `coach_money` double(10,0) DEFAULT NULL,
  PRIMARY KEY (`classes_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of classes
-- ----------------------------
INSERT INTO `classes` VALUES ('1', '古典瑜伽', '古典瑜伽是所有瑜伽之源，也是最bai系统全面的基du础课程。它不仅详解哈他瑜zhi伽，而且囊括古印度六大传统瑜伽学派，能最大限度地探究瑜伽的广泛含义，打开各类瑜伽之门。', '1', '1', '1000.00', '2020-05-20', '2020-05-27', '2,4,6', '8:00-9:00', '50', '瑜伽大师亲自授课', '4', '500');
INSERT INTO `classes` VALUES ('2', '阿斯汤嘎瑜伽', '阿斯汤嘎瑜伽一般指流瑜伽，流瑜伽在练习的过程中以行如流水般流畅的动作组合来强健身体，它侧重伸展性，力量性，柔韧性，耐力，平衡性，专注力。', '2', '2', '2000.00', '2020-06-03', '2020-06-09', '1,3,4,6', '8:00-10:00', '60', '小姐姐多', '2', '600');
INSERT INTO `classes` VALUES ('3', '艾扬格瑜伽', '艾扬格瑜伽非常注重人体正确的摆放，生理结构，骨胳肌肉的功能等，强调体位动作的精准，到位，讲究身心放空。', '3', '3', '4000.00', '2020-05-11', '2020-05-18', '1,3,5', '8:00-12:00', '40', '大家好渣渣辉啊', '3', '400');
INSERT INTO `classes` VALUES ('4', '热瑜伽', '热瑜伽，也叫高温瑜伽、热力瑜伽，就是在38℃-40℃的高温环境中做瑜伽。它由26种伸展动作组成，属于柔韧性运动，能改善脊椎柔软度，适合办公室一族', '3', '4', '6000.00', '2020-06-01', '2020-06-29', '1,3,5', '8:00-12:00', '50', '小姐姐多', '2', '600');
INSERT INTO `classes` VALUES ('5', '热瑜伽', '热瑜伽，也叫高温瑜伽、热力瑜伽，就是在38℃-40℃的高温环境中做瑜伽。它由26种伸展动作组成，属于柔韧性运动，能改善脊椎柔软度，适合办公室一族', '3', '4', '1000.00', '2020-07-01', '2020-07-07', '2,4,6', '8:00-12:00', '50', '打死小姐姐', '4', '200');
INSERT INTO `classes` VALUES ('6', '热瑜伽', '热的流汗的瑜伽', '3', '4', '3000.00', '2020-07-08', '2020-07-12', '2,4,6', '10:00-12:00', '50', '打死小姐姐的瑜伽', '4', '200');
INSERT INTO `classes` VALUES ('7', '热瑜伽', '热瑜伽就是好', '4', '4', '2000.00', '2020-08-01', '2020-08-05', '1,3,5', '10:00-12:00', '50', '打死小姐姐', '4', '400');
INSERT INTO `classes` VALUES ('8', '热瑜伽', '热瑜伽好热啊  是的好热', '4', '2', '400.00', '2020-06-26', '2020-07-06', '1,3,5', '8:30-9:30', '50', '小姐姐多哦', '5', '100');

-- ----------------------------
-- Table structure for coach_class
-- ----------------------------
DROP TABLE IF EXISTS `coach_class`;
CREATE TABLE `coach_class` (
  `cc_id` int(255) NOT NULL AUTO_INCREMENT COMMENT '教练对学员发起的私教课程订单id',
  `trainee_id` int(255) DEFAULT NULL COMMENT '学员id',
  `coahe_id` int(255) DEFAULT NULL COMMENT '教练id',
  `start_time` datetime DEFAULT NULL COMMENT '课程开始时间',
  `end_time` datetime DEFAULT NULL COMMENT '课程结束时间',
  `hall_id` int(255) DEFAULT NULL COMMENT '场馆id',
  `coach_money` double(255,0) DEFAULT NULL COMMENT '教练费用',
  `hall_money` double(255,0) DEFAULT NULL COMMENT '场地费用',
  `total_money` double(255,0) DEFAULT NULL COMMENT '总费用',
  `coach_class_status` int(3) DEFAULT NULL COMMENT '教练课程订单状态(1:未完成,2:完成,3:已退款,4:已取消)',
  `trainee_sure` int(3) DEFAULT NULL COMMENT '学员确认(1:已确认,2:未确认,3:未完成)',
  `coach_sure` int(3) DEFAULT NULL COMMENT '学员确认(1:已确认,2:未确认,3:未完成)',
  `evaluation` int(255) DEFAULT NULL COMMENT '评价,好,中,差',
  `note` varchar(255) DEFAULT NULL COMMENT '备注:用于申述附带',
  `pay_money` double(255,0) DEFAULT NULL,
  PRIMARY KEY (`cc_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of coach_class
-- ----------------------------
INSERT INTO `coach_class` VALUES ('14', '2', '2', '2020-06-14 00:00:00', '2020-06-14 02:00:00', '3', '600', '0', '600', '6', '2', '2', null, '垃圾教练', null);
INSERT INTO `coach_class` VALUES ('18', '2', '2', '2020-06-13 13:00:00', '2020-06-13 15:00:00', '3', '600', '0', '600', '2', '1', '1', '1', null, null);

-- ----------------------------
-- Table structure for coach_genre
-- ----------------------------
DROP TABLE IF EXISTS `coach_genre`;
CREATE TABLE `coach_genre` (
  `coach_genre_id` int(255) NOT NULL AUTO_INCREMENT COMMENT '教练流派表',
  `genre_id` int(255) DEFAULT NULL COMMENT '流派id',
  `genre_name` varchar(255) DEFAULT NULL,
  `coach_id` int(255) DEFAULT NULL COMMENT '教练id',
  PRIMARY KEY (`coach_genre_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of coach_genre
-- ----------------------------
INSERT INTO `coach_genre` VALUES ('1', '1', '古典瑜伽', '1');
INSERT INTO `coach_genre` VALUES ('2', '2', '阿斯汤嘎瑜伽', '2');
INSERT INTO `coach_genre` VALUES ('3', '3', '艾扬格瑜伽', '3');
INSERT INTO `coach_genre` VALUES ('4', '4', '热瑜伽', '4');
INSERT INTO `coach_genre` VALUES ('5', '5', '昆达利尼瑜伽', '5');
INSERT INTO `coach_genre` VALUES ('6', '1', '古典瑜伽', '6');
INSERT INTO `coach_genre` VALUES ('7', '2', '阿斯汤嘎瑜伽', '7');
INSERT INTO `coach_genre` VALUES ('8', '3', '艾扬格瑜伽', '8');

-- ----------------------------
-- Table structure for coach_hall
-- ----------------------------
DROP TABLE IF EXISTS `coach_hall`;
CREATE TABLE `coach_hall` (
  `th_id` int(255) NOT NULL AUTO_INCREMENT COMMENT '教练,场馆签约id',
  `coach_id` int(255) DEFAULT NULL COMMENT '教练id',
  `hall_id` int(255) DEFAULT NULL COMMENT '场馆id',
  `start_time` date DEFAULT NULL COMMENT '发起时间',
  `coach_hall_status` int(3) DEFAULT NULL COMMENT '教练和会馆签约状态(1:未确认,2:已签约,3:已过期,4:已拒绝)',
  `sign_message` varchar(255) DEFAULT NULL COMMENT '教练与场馆之间的签约请求说明(请求一方)',
  `requester` varchar(255) DEFAULT NULL COMMENT '1:教练发起的,2:会馆发起的',
  PRIMARY KEY (`th_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of coach_hall
-- ----------------------------
INSERT INTO `coach_hall` VALUES ('1', '1', '3', '2020-06-10', '2', '会馆美女多', '2:柔雅瑜伽');
INSERT INTO `coach_hall` VALUES ('3', '3', '3', '2020-06-11', '2', '小姐姐多', '1:梦里花');
INSERT INTO `coach_hall` VALUES ('4', '4', '4', '2020-06-07', '2', '小姐姐多', '2:梵谷瑜伽');
INSERT INTO `coach_hall` VALUES ('5', '5', '5', '2020-06-01', '5', '小姐姐多', '1:楚君莫');
INSERT INTO `coach_hall` VALUES ('6', '2', '3', '2020-06-08', '5', '钱多', '2:韵柔瑜伽');
INSERT INTO `coach_hall` VALUES ('9', '1', '3', '2020-06-11', '5', '你来吧', '2:心逸瑜伽');
INSERT INTO `coach_hall` VALUES ('12', '2', '3', '2020-06-12', '5', '反反复复付', '2心逸瑜伽');
INSERT INTO `coach_hall` VALUES ('13', '2', '3', '2020-06-13', '3', '来啊\n', '2心逸瑜伽');
INSERT INTO `coach_hall` VALUES ('14', '2', '3', '2020-06-13', '5', 'guolai \n', '2心逸瑜伽');

-- ----------------------------
-- Table structure for coach_info
-- ----------------------------
DROP TABLE IF EXISTS `coach_info`;
CREATE TABLE `coach_info` (
  `coach_id` int(255) NOT NULL AUTO_INCREMENT,
  `user_id` int(255) DEFAULT NULL COMMENT '教练id',
  `age` int(3) DEFAULT NULL COMMENT '年龄',
  `sex` int(3) DEFAULT NULL COMMENT '性别',
  `head_portrait` varchar(255) DEFAULT NULL COMMENT '头像地址',
  `nickname` varchar(50) DEFAULT NULL COMMENT '昵称',
  `open_status` int(3) DEFAULT NULL COMMENT '信息是否公开',
  `money` double(255,0) DEFAULT NULL COMMENT '余额',
  `address` varchar(255) DEFAULT NULL COMMENT '地址',
  `certification` int(3) DEFAULT NULL COMMENT '教练认证(1:未认证,2:官方认证,3:场馆教练)',
  `price` double(10,2) DEFAULT NULL COMMENT '余额',
  `free_time` varchar(255) DEFAULT NULL COMMENT '私教时间(1at,1bt,1ct|2bf,)',
  `has_class` int(2) DEFAULT NULL,
  PRIMARY KEY (`coach_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of coach_info
-- ----------------------------
INSERT INTO `coach_info` VALUES ('1', '9', '23', '1', 'http://qax8jdy42.bkt.clouddn.com/te3.jpg', '沛菡', '1', '22222', '重庆市渝北区金渝大道88号', '2', '200.00', '1at,1bt,1ct|2bf,', '2');
INSERT INTO `coach_info` VALUES ('2', '10', '22', '2', 'http://qax8jdy42.bkt.clouddn.com/te1.jpg', '羽化尘', '1', '34953', '\r\n重庆市渝北区宝石路66号', '3', '300.00', '1at,1bt,1ct|2bf,', '2');
INSERT INTO `coach_info` VALUES ('3', '11', '24', '1', 'http://qax8jdy42.bkt.clouddn.com/n1.jpg', '梦里花', '1', '111111', '重庆市渝北区金渝大道96', '2', '250.00', '1at,1bt,1ct|2bf,', '2');
INSERT INTO `coach_info` VALUES ('4', '12', '21', '2', 'http://qax8jdy42.bkt.clouddn.com/n1.jpg', '离魂曲', '1', '44444', '重庆市渝北区金石大道369号', '3', '220.00', '1at,1bt,1ct|2bf,', '2');
INSERT INTO `coach_info` VALUES ('5', '13', '24', '1', 'http://qax8jdy42.bkt.clouddn.com/n1.jpg', 'sisi', '1', '55555', '重庆', '2', '200.00', '1at,1bt,1ct|2bf,', '2');
INSERT INTO `coach_info` VALUES ('6', '14', '23', '2', 'http://qax8jdy42.bkt.clouddn.com/n1.jpg', '轩辕绝', '1', '66666', '重庆市渝北区金石大道99号', '3', '400.00', '1at,1bt,1ct|2bf,', '2');
INSERT INTO `coach_info` VALUES ('7', '15', '22', '1', 'http://qax8jdy42.bkt.clouddn.com/n1.jpg', '宸影殇', '1', '67777', '渝北回兴上弯路风平停车场', '2', '150.00', '1at,1bt,1ct|2bf,', '2');
INSERT INTO `coach_info` VALUES ('8', '16', '25', '2', 'http://qax8jdy42.bkt.clouddn.com/n1.jpg', '枫叶羽', '1', '88888', '\r\n重庆市渝北区宝石路14号', '1', '100.00', '1at,1bt,1ct|2bf,', '2');
INSERT INTO `coach_info` VALUES ('12', '38', '28', '1', null, '老教练', '1', '1233', '重庆', '2', '200.00', null, null);

-- ----------------------------
-- Table structure for focus
-- ----------------------------
DROP TABLE IF EXISTS `focus`;
CREATE TABLE `focus` (
  `focus_id` int(255) NOT NULL AUTO_INCREMENT COMMENT '关注表id',
  `follower_id` int(255) DEFAULT NULL COMMENT '关注人id',
  `follower_name` varchar(255) DEFAULT NULL COMMENT '关注人名字',
  `fan_id` int(255) DEFAULT NULL COMMENT '被关注id',
  `fan_name` varchar(255) DEFAULT NULL COMMENT '被关注人名字',
  `if_friend` int(3) DEFAULT NULL COMMENT '1:不是,2:是',
  PRIMARY KEY (`focus_id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of focus
-- ----------------------------
INSERT INTO `focus` VALUES ('44', '10', '羽化尘', '3', '之桃', '1');

-- ----------------------------
-- Table structure for genre
-- ----------------------------
DROP TABLE IF EXISTS `genre`;
CREATE TABLE `genre` (
  `genre_id` int(255) NOT NULL AUTO_INCREMENT COMMENT '瑜伽流派表id',
  `genre_name` varchar(50) DEFAULT NULL COMMENT '瑜伽名字',
  `des` varchar(255) DEFAULT NULL COMMENT '瑜伽流派描述',
  PRIMARY KEY (`genre_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of genre
-- ----------------------------
INSERT INTO `genre` VALUES ('1', '古典瑜伽', '古典瑜伽是所有瑜伽之源，也是最bai系统全面的基du础课程。它不仅详解哈他瑜zhi伽，而且囊括古印度六大传统瑜伽学派，能最大限度地探究瑜伽的广泛含义，打开各类瑜伽之门。');
INSERT INTO `genre` VALUES ('2', '阿斯汤嘎瑜伽', '阿斯汤嘎瑜伽一般指流瑜伽，流瑜伽在练习的过程中以行如流水般流畅的动作组合来强健身体，它侧重伸展性，力量性，柔韧性，耐力，平衡性，专注力。');
INSERT INTO `genre` VALUES ('3', '艾扬格瑜伽', '艾扬格瑜伽非常注重人体正确的摆放，生理结构，骨胳肌肉的功能等，强调体位动作的精准，到位，讲究身心放空。');
INSERT INTO `genre` VALUES ('4', '热瑜伽', '热瑜伽，也叫高温瑜伽、热力瑜伽，就是在38℃-40℃的高温环境中做瑜伽。它由26种伸展动作组成，属于柔韧性运动，能改善脊椎柔软度，适合办公室一族。');
INSERT INTO `genre` VALUES ('5', '昆达利尼瑜伽', '又称为蛇王瑜伽。昆达利尼证明了人体周身存在72，000条气脉，七大梵穴轮，一根主通道和一条尚未唤醒而处在休眠状态的圣蛇。');

-- ----------------------------
-- Table structure for hall_info
-- ----------------------------
DROP TABLE IF EXISTS `hall_info`;
CREATE TABLE `hall_info` (
  `hall_id` int(255) NOT NULL AUTO_INCREMENT COMMENT '会馆详情id',
  `user_id` int(255) DEFAULT NULL COMMENT '会馆id',
  `hall_name` varchar(50) DEFAULT NULL COMMENT '会馆名称',
  `head_portrait` varchar(255) DEFAULT NULL COMMENT '会馆头像',
  `address` varchar(255) DEFAULT NULL COMMENT '会馆地址',
  `img` varchar(255) DEFAULT NULL COMMENT '会馆图片文件夹地址',
  `money` double(255,0) DEFAULT NULL COMMENT '余额',
  `introduce` varchar(255) DEFAULT NULL COMMENT '会馆介绍',
  `hall_status` varchar(255) DEFAULT NULL COMMENT '会馆审核状态(1:未审核,2:已审核)',
  PRIMARY KEY (`hall_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of hall_info
-- ----------------------------
INSERT INTO `hall_info` VALUES ('1', '17', '柔雅瑜伽', 'http://qax8jdy42.bkt.clouddn.com/n1.jpg', '重庆市九龙坡区杨家坪西郊三村46栋2-17号', 'http://qax8jdy42.bkt.clouddn.com/007e49bcffa6471295cc7bd7ca9ddbf1.jpeg,http://qax8jdy42.bkt.clouddn.com/__45478763__8944580.jpg,http://qax8jdy42.bkt.clouddn.com/p138407199.jpg', '12420', '轻柔、柔和、高雅不失高贵', '2');
INSERT INTO `hall_info` VALUES ('2', '18', '韵柔瑜伽', 'http://qax8jdy42.bkt.clouddn.com/s2.jpg', '重庆市九龙坡区杨家坪西城镜园', 'http://qax8jdy42.bkt.clouddn.com/20150906224058-aa34cba2-2s.jpg,http://qax8jdy42.bkt.clouddn.com/u%3D1117848328%2C3891568340%26fm%3D214%26gp%3D0.jpg', '4444462', '瑜伽的自然大方、有韵致', '2');
INSERT INTO `hall_info` VALUES ('3', '19', '心逸瑜伽', 'http://qax8jdy42.bkt.clouddn.com/s6.jpg', '重庆市九龙坡区西郊三村1号2幢附5号', 'http://qax8jdy42.bkt.clouddn.com/3-16012G501500-L.jpg,http://qax8jdy42.bkt.clouddn.com/__45478763__8944580.jpg', '13129031', '瑜伽能使人心里舒适、安逸', '2');
INSERT INTO `hall_info` VALUES ('4', '20', '俞乐瑜伽', 'http://qax8jdy42.bkt.clouddn.com/te3.jpg', '重庆市九龙坡区西郊路杨家坪', 'http://qax8jdy42.bkt.clouddn.com/900_800_20160201033707143.jpg,http://qax8jdy42.bkt.clouddn.com/007e49bcffa6471295cc7bd7ca9ddbf1.jpeg', '2430243900', '给人一种这是女性练习瑜伽的地方', '2');
INSERT INTO `hall_info` VALUES ('6', '39', '鹏鹏瑜伽', '', '重庆', '', '12222', '生活服务', '2');

-- ----------------------------
-- Table structure for hall_products
-- ----------------------------
DROP TABLE IF EXISTS `hall_products`;
CREATE TABLE `hall_products` (
  `product_id` int(255) NOT NULL AUTO_INCREMENT COMMENT '会馆产品id',
  `product_name` varchar(50) DEFAULT NULL COMMENT '产品名字',
  `product_price` double(255,2) DEFAULT NULL COMMENT '产品价格',
  `period` varchar(255) DEFAULT NULL COMMENT '产品周期',
  `hall_id` int(11) DEFAULT NULL COMMENT '会馆id',
  `discount` double DEFAULT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of hall_products
-- ----------------------------
INSERT INTO `hall_products` VALUES ('1', '年卡', '2000.00', '一年', '1', '0.5');
INSERT INTO `hall_products` VALUES ('2', '月卡', '1000.00', '一月', '1', '0.7');
INSERT INTO `hall_products` VALUES ('3', '周卡', '500.00', '一周', '1', '0.9');
INSERT INTO `hall_products` VALUES ('4', '年卡', '8888.00', '一年', '2', '0.5');
INSERT INTO `hall_products` VALUES ('5', '月卡', '6666.00', '一月', '2', '0.7');
INSERT INTO `hall_products` VALUES ('6', '周卡', '500.00', '一周', '2', '0.9');
INSERT INTO `hall_products` VALUES ('7', '洗脚卡', '3000.00', '一年', '3', '0.9');
INSERT INTO `hall_products` VALUES ('8', '会员卡', '1000.00', '一个月', '3', '0.8');
INSERT INTO `hall_products` VALUES ('9', '白银卡', '500.00', '一周', '3', '0.7');
INSERT INTO `hall_products` VALUES ('10', '帝王卡', '10000.00', '一年', '4', '0.2');
INSERT INTO `hall_products` VALUES ('11', '伯爵卡', '8000.00', '六个月', '4', '0.3');
INSERT INTO `hall_products` VALUES ('12', '公爵卡', '5055.00', '一个月', '4', '0.5');
INSERT INTO `hall_products` VALUES ('13', '超神卡', '100000.00', '一年', '4', '0.1');

-- ----------------------------
-- Table structure for leave_message
-- ----------------------------
DROP TABLE IF EXISTS `leave_message`;
CREATE TABLE `leave_message` (
  `lm_id` int(255) NOT NULL AUTO_INCREMENT COMMENT '留言表id',
  `luser_id` int(255) DEFAULT NULL COMMENT '留言人id',
  `luser_name` varchar(255) DEFAULT NULL COMMENT '留言人名字',
  `ruser_id` int(255) DEFAULT NULL COMMENT '被留言的人id',
  `ruser_name` varchar(255) DEFAULT NULL COMMENT '被留言人名字',
  `content` varchar(255) DEFAULT NULL,
  `if_read` int(3) DEFAULT NULL COMMENT '1:未读2:已读',
  `leave_time` datetime DEFAULT NULL COMMENT '留言时间',
  PRIMARY KEY (`lm_id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of leave_message
-- ----------------------------
INSERT INTO `leave_message` VALUES ('32', '3', '之桃', '10', '羽化尘', '不挂科', '1', '2020-06-13 16:06:37');
INSERT INTO `leave_message` VALUES ('33', '3', '之桃', '10', '羽化尘', '不挂科', '1', '2020-06-13 16:06:39');
INSERT INTO `leave_message` VALUES ('34', '3', '之桃', '10', '羽化尘', '赫拉克勒斯', '1', '2020-06-13 16:07:13');
INSERT INTO `leave_message` VALUES ('35', '3', '之桃', '10', '羽化尘', '赫拉克勒斯', '1', '2020-06-13 16:07:39');
INSERT INTO `leave_message` VALUES ('36', '3', '之桃', '10', '羽化尘', '考虑考虑', '1', '2020-06-13 16:09:38');
INSERT INTO `leave_message` VALUES ('37', '3', '之桃', '10', '羽化尘', '兔崽子', '1', '2020-06-13 16:10:55');
INSERT INTO `leave_message` VALUES ('38', '3', '之桃', '10', '羽化尘', '哈子', '1', '2020-06-13 16:58:39');
INSERT INTO `leave_message` VALUES ('42', '10', '羽化尘', '3', '之桃', '就看记录', '1', '2020-06-13 17:11:05');

-- ----------------------------
-- Table structure for notice
-- ----------------------------
DROP TABLE IF EXISTS `notice`;
CREATE TABLE `notice` (
  `notice_id` int(11) DEFAULT NULL,
  `byuser_id` int(11) DEFAULT NULL,
  `touser_id` int(11) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `type` int(3) DEFAULT NULL,
  `connect_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of notice
-- ----------------------------

-- ----------------------------
-- Table structure for pay
-- ----------------------------
DROP TABLE IF EXISTS `pay`;
CREATE TABLE `pay` (
  `pay_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `pay_type` varchar(255) DEFAULT NULL COMMENT '付款方式(余额,第三方?)',
  `order_type` int(3) DEFAULT NULL COMMENT '订单类型(私教?产品?)',
  `money` double(10,0) DEFAULT NULL,
  `get_user_id` int(11) DEFAULT NULL COMMENT '收款方id',
  PRIMARY KEY (`pay_id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pay
-- ----------------------------
INSERT INTO `pay` VALUES ('1', '1', '支付宝', '2', '12', '2');
INSERT INTO `pay` VALUES ('2', '1', '余额支付', '1', '20', '10');
INSERT INTO `pay` VALUES ('3', '1', '余额支付', '1', '20', '2');
INSERT INTO `pay` VALUES ('4', '2', '余额支付', '1', '23', '10');
INSERT INTO `pay` VALUES ('5', '2', '余额支付', '1', '23', '4');
INSERT INTO `pay` VALUES ('6', '3', '余额支付', '1', '100', '10');
INSERT INTO `pay` VALUES ('7', '3', '余额支付', '1', '100', '2');
INSERT INTO `pay` VALUES ('8', '2', '余额支付', '1', '23', '10');
INSERT INTO `pay` VALUES ('9', '2', '余额支付', '1', '23', '4');
INSERT INTO `pay` VALUES ('10', '2', '余额支付', '1', '2', '10');
INSERT INTO `pay` VALUES ('11', '2', '余额支付', '1', '2', '2');
INSERT INTO `pay` VALUES ('12', '2', '余额支付', '1', '2', '10');
INSERT INTO `pay` VALUES ('13', '2', '余额支付', '1', '2', '2');
INSERT INTO `pay` VALUES ('14', '2', '余额支付', '1', '2', '10');
INSERT INTO `pay` VALUES ('15', '2', '余额支付', '1', '2', '2');
INSERT INTO `pay` VALUES ('16', '3', '余额支付', '1', '300', '10');
INSERT INTO `pay` VALUES ('17', '3', '余额支付', '1', '300', '18');
INSERT INTO `pay` VALUES ('18', '3', '余额支付', '1', '600', '10');
INSERT INTO `pay` VALUES ('19', '3', '余额支付', '1', '600', '18');
INSERT INTO `pay` VALUES ('20', '2', '余额支付', '1', '500', '16');
INSERT INTO `pay` VALUES ('21', '2', '余额支付', '1', '500', '20');
INSERT INTO `pay` VALUES ('22', '3', '余额支付', '1', '300', '10');
INSERT INTO `pay` VALUES ('23', '3', '余额支付', '1', '300', '18');
INSERT INTO `pay` VALUES ('24', '3', '余额支付', '1', '600', '10');
INSERT INTO `pay` VALUES ('25', '3', '余额支付', '1', '600', '18');
INSERT INTO `pay` VALUES ('41', '3', '余额支付', '1', '300', '10');
INSERT INTO `pay` VALUES ('42', '3', '余额支付', '1', '300', '18');
INSERT INTO `pay` VALUES ('43', '3', '余额支付', '1', '300', '10');
INSERT INTO `pay` VALUES ('44', '3', '余额支付', '1', '300', '19');

-- ----------------------------
-- Table structure for trainee_coach
-- ----------------------------
DROP TABLE IF EXISTS `trainee_coach`;
CREATE TABLE `trainee_coach` (
  `tc_id` int(255) NOT NULL AUTO_INCREMENT COMMENT '签约表id',
  `trainee_id` int(255) DEFAULT NULL COMMENT '学员id',
  `coach_id` int(255) DEFAULT NULL COMMENT '教练id',
  `start_time` varchar(255) DEFAULT NULL COMMENT '发起时间',
  `trainee_coach_status` int(255) DEFAULT NULL COMMENT '教练签约状态(1:未确认,2:已签约,3:已过期,4:已拒绝,5:解约)',
  `sign_message` varchar(255) DEFAULT NULL COMMENT '教练和学员签约状态',
  PRIMARY KEY (`tc_id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of trainee_coach
-- ----------------------------
INSERT INTO `trainee_coach` VALUES ('47', '2', '2', '2020-06-13 16:15:17', '5', '垃圾');
INSERT INTO `trainee_coach` VALUES ('48', '2', '2', '2020-06-13 16:16:43', '5', '阿里河');
INSERT INTO `trainee_coach` VALUES ('49', '2', '2', '2020-06-13 16:17:11', '5', '亏');
INSERT INTO `trainee_coach` VALUES ('50', '2', '2', '2020-06-13 17:05:57', '2', '过来看看');

-- ----------------------------
-- Table structure for trainee_info
-- ----------------------------
DROP TABLE IF EXISTS `trainee_info`;
CREATE TABLE `trainee_info` (
  `trainee_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(255) DEFAULT NULL COMMENT '用户id',
  `age` int(4) DEFAULT NULL COMMENT '年龄',
  `head_portrait` varchar(255) DEFAULT NULL COMMENT '头像地址',
  `nickname` varchar(50) DEFAULT NULL COMMENT '昵称',
  `open_status` int(4) DEFAULT NULL COMMENT '信息是否公开',
  `sex` int(4) DEFAULT NULL COMMENT '性别(1:男,2:女,3:保密)',
  `money` double(255,0) DEFAULT NULL COMMENT '余额',
  `address` varchar(255) DEFAULT NULL COMMENT '地址',
  PRIMARY KEY (`trainee_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of trainee_info
-- ----------------------------
INSERT INTO `trainee_info` VALUES ('1', '2', '22', 'http://qax8jdy42.bkt.clouddn.com/n1.jpg', '忆柳', '1', '1', '18940', '重庆市渝北区民心路555号');
INSERT INTO `trainee_info` VALUES ('2', '3', '33', 'http://qax8jdy42.bkt.clouddn.com/n1.jpg', '之桃', '1', '2', '331933', '九龙坡区华岩新城华福大道北段29号');
INSERT INTO `trainee_info` VALUES ('3', '4', '11', 'http://img0.imgtn.bdimg.com/it/u=3521319392,1160740190&fm=26&gp=0.jpg', '慕青', '1', '1', '11111', '重庆市渝北区民心路555号');
INSERT INTO `trainee_info` VALUES ('4', '5', '44', 'http://img1.imgtn.bdimg.com/it/u=91218753,1846445374&fm=11&gp=0.jpg', '问兰', '1', '2', '32833', '重庆市江北区大石坝石马河');
INSERT INTO `trainee_info` VALUES ('6', '7', '23', 'http://img5.imgtn.bdimg.com/it/u=3095823371,2737858048&fm=26&gp=0.jpg', '元香', '1', '2', '231231', '渝北龙塔街兴盛大道55号');
INSERT INTO `trainee_info` VALUES ('7', '8', '21', 'http://img0.imgtn.bdimg.com/it/u=695164091,430070587&fm=11&gp=0.jpg', '初夏', '1', '1', '44444', '重庆市渝北区渝鲁大道188号(海尔路交汇处)');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `user_id` int(255) NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `phone_number` varchar(18) DEFAULT NULL COMMENT '电话号码',
  `pwd` varchar(20) DEFAULT NULL COMMENT '密码',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `nick_name` varchar(255) DEFAULT NULL COMMENT '账户名称',
  `third_account` varchar(50) DEFAULT NULL COMMENT '第三方账号',
  `role` int(255) DEFAULT NULL COMMENT '角色(1:管理员,2:用户,3:教练,4:场馆)',
  `user_status` int(255) DEFAULT NULL COMMENT '可用:1,不可用2',
  `cid` varchar(255) DEFAULT NULL COMMENT '手机端唯一标识',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', '15171983450', '123456', '1234567@qq.com', '梦琪', '111111111', '2', '0', '');
INSERT INTO `user` VALUES ('2', '13109299063', '123456', '2234567@qq.com', '忆柳', '2222222222', '2', '0', 'fc985f11179c36300d6587c9dd2a074e');
INSERT INTO `user` VALUES ('3', '15902381845', '123456', '3234567@qq.com', '之桃', '333333333', '2', '0', '0204abf858bb825b981ab6658982e429');
INSERT INTO `user` VALUES ('4', '13818100858', '123456', '4234567@qq.com', '慕青', '44444444', '2', '0', 'fc985f11179c36300d6587c9dd2a074e');
INSERT INTO `user` VALUES ('5', '13299779999', '123456', '51234567@qq.com', '问兰', '2222211111', '2', '0', null);
INSERT INTO `user` VALUES ('6', '15171983452', '123456', '61234567@qq.com', '尔岚', '233344444', '2', '0', null);
INSERT INTO `user` VALUES ('7', '13633102850', '123456', '71234567@qq.com', '元香', '44444444222', '2', '0', null);
INSERT INTO `user` VALUES ('8', '13653899999', '123456', '81234567@qq.com', '初夏', '111111123445', '2', '0', null);
INSERT INTO `user` VALUES ('9', '15910318888', '123456', '91234567@qq.com', '沛菡', '222211123455', '3', '0', 'fc985f11179c36300d6587c9dd2a074e');
INSERT INTO `user` VALUES ('10', '15902381846', '123456', '101234567@qq.com', '羽化尘', '556532222', '3', '0', '49269e906548625c2eeaec25f9ddb81e');
INSERT INTO `user` VALUES ('11', '18291619999', '123456', '111234567@qq.com', '梦里花', '1112334566667', '3', '0', null);
INSERT INTO `user` VALUES ('12', '18723959999', '123456', '121234567@qq.com', '离魂曲', '3342342', '3', '0', null);
INSERT INTO `user` VALUES ('13', '13387051063', '123456', '131234567@qq.com', '楚君莫', '13131313455', '3', '0', null);
INSERT INTO `user` VALUES ('14', '15560399999', '123456', '141234567@qq.com', '轩辕绝', '12312324555', '3', '0', null);
INSERT INTO `user` VALUES ('15', '13857744168', '123456', '151234567@qq.com', '宸影殇', '455555666', '3', '0', null);
INSERT INTO `user` VALUES ('16', '13805514168', '123456', '161234567@qq.com', '枫叶羽', '44477777990', '3', '0', null);
INSERT INTO `user` VALUES ('17', '13855414168', '123456', '171234567@qq.com', '沐云霄', '5565333455', '4', '0', null);
INSERT INTO `user` VALUES ('18', '15250759999', '123456', '181234567@qq.com', '落影痕', '990998888', '4', '0', null);
INSERT INTO `user` VALUES ('19', '13097885888', '123456', '119234567@qq.com', '习羽皇', '666556743', '4', '0', null);
INSERT INTO `user` VALUES ('20', '13897233597', '123456', '123204567@qq.com', '慕无苏', '223432211', '4', '0', null);
INSERT INTO `user` VALUES ('21', '13097885822', '123456', '123204567@qq.com', '元霸', '211221', '4', '0', null);
INSERT INTO `user` VALUES ('38', '13323232323', '123456', '93828222@qq.com', '老教练', null, '3', '0', null);
INSERT INTO `user` VALUES ('39', '13323233232', '123456', '2123232@qq.com', '鹏鹏瑜伽', null, '4', '0', null);
