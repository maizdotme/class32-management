/*
Navicat MySQL Data Transfer

Source Server         : yueke
Source Server Version : 50618
Source Host           : localhost:3306
Source Database       : yueke

Target Server Type    : MYSQL
Target Server Version : 50618
File Encoding         : 65001

Date: 2020-06-09 23:45:28
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for activity
-- ----------------------------
DROP TABLE IF EXISTS `activity`;
CREATE TABLE `activity` (
  `act_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键、自增',
  `camp_id` int(255) DEFAULT NULL COMMENT '营地id',
  `price` double DEFAULT NULL COMMENT '价格',
  `introduce` varchar(255) DEFAULT NULL COMMENT '活动简介',
  `act_name` varchar(255) DEFAULT NULL COMMENT '活动名',
  `time` datetime DEFAULT NULL COMMENT '活动时间',
  `status` int(11) DEFAULT NULL COMMENT '订单状态',
  `type_id` int(11) DEFAULT NULL COMMENT '类别id',
  `number` int(11) DEFAULT NULL COMMENT '库存',
  PRIMARY KEY (`act_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of activity
-- ----------------------------
INSERT INTO `activity` VALUES ('1', '1', '1', '', '天王音乐狂欢', '2020-06-02 14:05:37', '0', '1', '111');
INSERT INTO `activity` VALUES ('2', '1', '2', '啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊阿啊啊啊', '来旅游', '2020-06-02 17:45:56', '0', '2', '111');

-- ----------------------------
-- Table structure for activity_order
-- ----------------------------
DROP TABLE IF EXISTS `activity_order`;
CREATE TABLE `activity_order` (
  `activity_order_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键、自增',
  `act_id` int(11) DEFAULT NULL COMMENT '活动id',
  `number` int(11) DEFAULT NULL COMMENT '购买数量',
  `order_time` datetime DEFAULT NULL COMMENT '订单时间',
  `price` double(255,0) DEFAULT NULL COMMENT '单价',
  `total` double(255,0) DEFAULT NULL COMMENT '总价',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `camp_id` int(11) DEFAULT NULL COMMENT '营地id',
  `status` int(11) DEFAULT NULL COMMENT '订单状态',
  PRIMARY KEY (`activity_order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of activity_order
-- ----------------------------
INSERT INTO `activity_order` VALUES ('1', '1', '1', '2020-06-04 15:44:36', '1', '1', '2', '1', '2');
INSERT INTO `activity_order` VALUES ('2', '1', '1', '2020-06-04 17:01:36', '1', '1', '2', '1', '2');
INSERT INTO `activity_order` VALUES ('3', '1', '1', '2020-06-04 17:58:55', '1', '1', '2', '1', '2');
INSERT INTO `activity_order` VALUES ('4', '1', '1', '2020-06-04 17:58:57', '1', '1', '2', '1', '2');
INSERT INTO `activity_order` VALUES ('5', '1', '1', '2020-06-04 17:58:59', '1', '1', '2', '1', '2');
INSERT INTO `activity_order` VALUES ('6', '1', '1', '2020-06-04 17:59:01', '1', '1', '2', '1', '2');
INSERT INTO `activity_order` VALUES ('7', '1', '1', '2020-06-04 17:59:03', '1', '1', '2', '1', '2');
INSERT INTO `activity_order` VALUES ('8', '1', '1', '2020-06-04 17:59:05', '1', '1', '2', '1', '2');
INSERT INTO `activity_order` VALUES ('9', '1', '1', '2020-06-04 17:59:08', '1', '1', '2', '1', '2');
INSERT INTO `activity_order` VALUES ('10', '1', '1', '2020-06-04 17:59:09', '1', '1', '2', '1', '2');
INSERT INTO `activity_order` VALUES ('11', '1', '1', '2020-06-04 17:59:11', '1', '1', '2', '1', '2');
INSERT INTO `activity_order` VALUES ('12', '1', '1', '2020-06-04 17:59:13', '1', '1', '2', '1', '2');
INSERT INTO `activity_order` VALUES ('13', '1', '1', '2020-06-04 17:59:15', '1', '1', '2', '1', '2');
INSERT INTO `activity_order` VALUES ('14', '1', '1', '2020-06-04 17:59:17', '1', '1', '2', '1', '2');
INSERT INTO `activity_order` VALUES ('15', '1', '1', '2020-06-04 17:59:19', '1', '1', '2', '1', '2');
INSERT INTO `activity_order` VALUES ('16', '1', '1', '2020-06-04 17:59:21', '1', '1', '2', '1', '2');
INSERT INTO `activity_order` VALUES ('17', '1', '1', '2020-06-04 17:59:24', '1', '1', '2', '1', '2');
INSERT INTO `activity_order` VALUES ('18', '1', '1', '2020-06-04 17:59:26', '1', '1', '2', '1', '2');
INSERT INTO `activity_order` VALUES ('19', '1', '111', '2020-06-04 17:59:34', '1', '111', '2', '1', '2');
INSERT INTO `activity_order` VALUES ('20', '1', '111', '2020-06-04 17:59:51', '1', '111', '2', '1', '2');
INSERT INTO `activity_order` VALUES ('21', '1', '111', '2020-06-04 18:03:31', '1', '111', '2', '1', '2');
INSERT INTO `activity_order` VALUES ('22', '1', '1', '2020-06-04 19:02:02', '1', '1', '2', '1', '2');

-- ----------------------------
-- Table structure for activity_type
-- ----------------------------
DROP TABLE IF EXISTS `activity_type`;
CREATE TABLE `activity_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键、自增',
  `type_name` varchar(255) DEFAULT NULL COMMENT '活动类别名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of activity_type
-- ----------------------------
INSERT INTO `activity_type` VALUES ('1', '音乐');
INSERT INTO `activity_type` VALUES ('2', '美食');
INSERT INTO `activity_type` VALUES ('3', '旅游');

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `camp_id` int(11) DEFAULT NULL COMMENT '营地id',
  `name` varchar(255) DEFAULT NULL COMMENT '管理员用户名',
  `password` varchar(255) DEFAULT NULL,
  `tel` varchar(255) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL COMMENT '角色id',
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES ('1', '1', '翠花', 'chuihua', '17712345678', '1');

-- ----------------------------
-- Table structure for appinfo
-- ----------------------------
DROP TABLE IF EXISTS `appinfo`;
CREATE TABLE `appinfo` (
  `appinfo_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `tel` varchar(255) DEFAULT NULL,
  `idcard` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`appinfo_id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of appinfo
-- ----------------------------
INSERT INTO `appinfo` VALUES ('1', '李四', '122357218', '2731632163218');
INSERT INTO `appinfo` VALUES ('22', '123', '456', '789');
INSERT INTO `appinfo` VALUES ('23', '李四', '2345678', '1253872163489');
INSERT INTO `appinfo` VALUES ('24', '2', '34', '4');
INSERT INTO `appinfo` VALUES ('25', '1', '1', '23');
INSERT INTO `appinfo` VALUES ('26', '', '', '');
INSERT INTO `appinfo` VALUES ('27', '12', '34', '56');

-- ----------------------------
-- Table structure for appointment
-- ----------------------------
DROP TABLE IF EXISTS `appointment`;
CREATE TABLE `appointment` (
  `appointment_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键、自增',
  `camp_id` int(11) DEFAULT NULL COMMENT '营地id',
  `car_id` int(11) DEFAULT NULL COMMENT '房车id',
  `start_time` datetime DEFAULT NULL COMMENT '开始时间',
  `days` varchar(255) DEFAULT NULL COMMENT '预约天数',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `end_time` datetime DEFAULT NULL COMMENT '结束时间',
  `total` double(255,0) DEFAULT NULL COMMENT '总价',
  `deposit` double(255,0) DEFAULT NULL COMMENT '定金',
  `status` int(255) DEFAULT NULL COMMENT '预定状态1、待支付，2、已预约，3、已完成',
  `wait` varchar(255) DEFAULT NULL COMMENT '备用',
  `appinfo_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`appointment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of appointment
-- ----------------------------
INSERT INTO `appointment` VALUES ('1', '1', '1', '2020-06-04 00:00:00', '1', '2', '2020-06-04 12:13:40', '1158', '200', '3', null, '1');
INSERT INTO `appointment` VALUES ('2', '1', '1', '2020-06-05 00:00:00', '3', '3', '2020-06-08 00:00:00', '3474', '200', '3', null, '2');
INSERT INTO `appointment` VALUES ('6', '1', '2', '2020-06-05 00:00:00', '1', '2', '2020-06-05 15:07:06', '899', '200', '4', null, '6');
INSERT INTO `appointment` VALUES ('34', '1', '1', '2020-06-14 00:00:00', '1', '2', '2020-06-15 00:00:00', '1158', '200', '4', '已取消', '22');
INSERT INTO `appointment` VALUES ('35', '1', '3', '2020-06-09 00:00:00', '2', '2', '2020-06-11 00:00:00', '998', '200', '2', null, '23');
INSERT INTO `appointment` VALUES ('36', '1', '4', '2020-06-24 00:00:00', '3', '2', '2020-06-27 00:00:00', '3474', '200', '2', null, '24');
INSERT INTO `appointment` VALUES ('37', '1', '2', '2020-06-09 00:00:00', '2', '2', '2020-06-11 00:00:00', '1798', '200', '2', null, '25');
INSERT INTO `appointment` VALUES ('38', '1', '3', '2020-06-25 00:00:00', '1', '2', '2020-06-26 00:00:00', '499', '200', '2', null, '26');
INSERT INTO `appointment` VALUES ('39', '1', '3', '2020-06-16 00:00:00', '1', '2', '2020-06-17 00:00:00', '499', '200', '2', null, '27');

-- ----------------------------
-- Table structure for camp
-- ----------------------------
DROP TABLE IF EXISTS `camp`;
CREATE TABLE `camp` (
  `camp_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '营地id',
  `campname` varchar(255) DEFAULT NULL COMMENT '营地名字',
  `address` varchar(255) DEFAULT NULL COMMENT '营地地址',
  `tel` varchar(11) DEFAULT NULL COMMENT '营地电话',
  `introduce` varchar(255) DEFAULT NULL COMMENT '营地介绍',
  `city_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`camp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of camp
-- ----------------------------
INSERT INTO `camp` VALUES ('1', '悦客沙坪坝区花马山房车露营基地', '重庆市沙坪坝区大学城重庆\r\n师范大学西部', '023-6500211', '本项目位于重庆市沙坪坝区大学城重庆师范大学西部。\r\n								规划范围包括石码山水库全境及周边山体,规划地面积约160公顷。\r\n								重庆花马车露营基地项目是重庆路约客房车营地有限公司规划布局建设的100个房:车露营地总部示范基地。', '1');
INSERT INTO `camp` VALUES ('2', '悦客茶山竹海房车营地', '重庆市永川区城北茶山竹海街道办事处辖区', '023-6500211', '重庆茶山竹海国家森林公园位于重市永川区城北茶山竹海街道办事处辖区,东距重庆市区55公里，西距大足石刻50公里、距成都272公里,南至泸州72公里，总面积9979.00公顷森林公园地处中亚热带湿润气候区年平均气温14摄氏度,植被类型为中亚热带常绿阔叶林，景区森林覆盖率达97%,同时也是国家AAAA级风景区。', '1');
INSERT INTO `camp` VALUES ('3', '悦客南川金佛山房车营地', '重庆市南川区金佛山自然保\r\n护\r\n', '023-6500211', '金佛山位于重庆南部南川市境内，大山山脉，金佛山又名金山，古称九递山，金佛、箐坝、柏枝三山108峰组成。总面积1300平方公里，风景区规划面积441平方公里，保护区面积522平方公里。主峰风吹岭海拔2251米,大娄山脉最高峰,金佛山就如高昂的龙头雄踞在这条山系的北端。金佛山素为蜀中四大名之-而闻名遐迩。“ 金佛何崔嵬，飘渺云霞间”对它最美好的写照。每当夏秋晚晴，落日斜晖把层层山崖映染得金壁辉煌，如一尊錦大佛闪射紡道光，异常壮观而美丽，”金佛山” 因此而得名。金佛山属典型的喀斯特地质地貌，由于特殊的', '1');
INSERT INTO `camp` VALUES ('4', '悦客万盛黑山谷房车营地', '重庆市南川区綦江区万盛经济开发区黑山谷1098 ,近414省道', '023-6500211', '重庆黑山谷语房车营地帐篷酒店位于重庆市万盛经开区\r\n山八角小镇，相邻国家5A级景区黑山谷景区。海拔约\r\n1100米， 常年气温23度，气清新、色宜人、凉爽宜.\r\n居，题季休闲避暑胜地。庆谷语房车营地:帐篷\r\n店房内各种设备齐全，可以满足不同组合的游客居\r\n住,整个房车营地可容纳40-50人同时入住。帐篷房借\r\n鉴帐篷的外形特征，色的预，流线型的帆布屋顶从\r\n中间悬挂下来显得格外醒目，搭配优美的自然的环境,\r\n让入住其中的游客不经产生一种野营的错觉\r\n', '1');

-- ----------------------------
-- Table structure for car
-- ----------------------------
DROP TABLE IF EXISTS `car`;
CREATE TABLE `car` (
  `car_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '房车id',
  `camp_id` int(11) DEFAULT NULL COMMENT '营地id',
  `car_name` varchar(255) DEFAULT NULL COMMENT '房车描述',
  `type_id` int(11) DEFAULT NULL COMMENT '房车类别id',
  `status` int(11) DEFAULT NULL COMMENT '房车是否被预定或被使用',
  `code_url` varchar(255) DEFAULT NULL COMMENT '房车二维码图片地址',
  `license` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`car_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of car
-- ----------------------------
INSERT INTO `car` VALUES ('1', '1', 'Fendt Saphir 560 FD豪华版', '1', '0', '1', 'ABCDEF');
INSERT INTO `car` VALUES ('2', '1', 'Hobby 455 UF精致版', '2', '1', '1', 'ABCDEF');
INSERT INTO `car` VALUES ('3', '1', 'Fendt Saphir 560 FD简约版', '3', '1', '1', 'ABCDEF');
INSERT INTO `car` VALUES ('4', '1', 'Hobby 455 UF豪华版', '1', '1', '1', 'ABCDEF');

-- ----------------------------
-- Table structure for car_order
-- ----------------------------
DROP TABLE IF EXISTS `car_order`;
CREATE TABLE `car_order` (
  `car_order_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '订单id',
  `order_num` varchar(255) DEFAULT NULL COMMENT '订单编号',
  `order_name` varchar(255) DEFAULT NULL COMMENT '订单名称',
  `order_total` double(255,2) DEFAULT NULL COMMENT '订单金额',
  `order_time` datetime DEFAULT NULL COMMENT '订单时间',
  `order_status` int(11) DEFAULT NULL COMMENT '订单状态',
  `order_discount` double(255,2) DEFAULT NULL COMMENT '余额抵消',
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`car_order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of car_order
-- ----------------------------
INSERT INTO `car_order` VALUES ('23', '1591706955469', 'Fendt Saphir 560 FD简约版', '1178.00', '2020-06-09 20:49:15', '0', '20.00', '2');
INSERT INTO `car_order` VALUES ('24', '1591707279228', 'Hobby 455 UF豪华版', '2416.00', '2020-06-09 20:54:39', '0', '1258.00', '2');
INSERT INTO `car_order` VALUES ('25', '1591710215787', 'Hobby 455 UF精致版', '0.01', '2020-06-09 21:43:36', '0', '1998.00', '1');
INSERT INTO `car_order` VALUES ('26', '1591710269847', 'Fendt Saphir 560 FD简约版', '0.01', '2020-06-09 21:44:30', '0', '699.00', '1');
INSERT INTO `car_order` VALUES ('27', '1591715686826', 'Fendt Saphir 560 FD简约版', '0.01', '2020-06-09 23:14:47', '0', '699.00', '1');

-- ----------------------------
-- Table structure for car_type
-- ----------------------------
DROP TABLE IF EXISTS `car_type`;
CREATE TABLE `car_type` (
  `type_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '房车类型id',
  `level` varchar(255) DEFAULT NULL COMMENT '房车的等级',
  `price` double DEFAULT NULL COMMENT '房车多少钱一天',
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of car_type
-- ----------------------------
INSERT INTO `car_type` VALUES ('1', '豪华版', '1158');
INSERT INTO `car_type` VALUES ('2', '精致版', '899');
INSERT INTO `car_type` VALUES ('3', '简约版', '499');

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `cate_name` varchar(255) DEFAULT NULL COMMENT '集市类别名',
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of category
-- ----------------------------

-- ----------------------------
-- Table structure for city
-- ----------------------------
DROP TABLE IF EXISTS `city`;
CREATE TABLE `city` (
  `city_id` int(11) NOT NULL AUTO_INCREMENT,
  `city_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`city_id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of city
-- ----------------------------
INSERT INTO `city` VALUES ('1', '重庆');
INSERT INTO `city` VALUES ('2', '北京');
INSERT INTO `city` VALUES ('3', '辽宁');
INSERT INTO `city` VALUES ('4', '吉林');
INSERT INTO `city` VALUES ('5', '黑龙江');
INSERT INTO `city` VALUES ('6', '河北');
INSERT INTO `city` VALUES ('7', '山西');
INSERT INTO `city` VALUES ('8', '陕西');
INSERT INTO `city` VALUES ('9', '甘肃');
INSERT INTO `city` VALUES ('10', '青海');
INSERT INTO `city` VALUES ('11', '山东');
INSERT INTO `city` VALUES ('12', '安徽');
INSERT INTO `city` VALUES ('13', '江苏');
INSERT INTO `city` VALUES ('14', '浙江');
INSERT INTO `city` VALUES ('15', '河南');
INSERT INTO `city` VALUES ('16', '湖北');
INSERT INTO `city` VALUES ('17', '湖南');
INSERT INTO `city` VALUES ('18', '江西');
INSERT INTO `city` VALUES ('19', '台湾');
INSERT INTO `city` VALUES ('20', '福建');
INSERT INTO `city` VALUES ('21', '云南');
INSERT INTO `city` VALUES ('22', '海南');
INSERT INTO `city` VALUES ('23', '四川');
INSERT INTO `city` VALUES ('24', '贵州');
INSERT INTO `city` VALUES ('25', '广东');
INSERT INTO `city` VALUES ('26', '内蒙古');
INSERT INTO `city` VALUES ('27', '新疆');
INSERT INTO `city` VALUES ('28', '广西');
INSERT INTO `city` VALUES ('29', '西藏');
INSERT INTO `city` VALUES ('30', '宁夏');
INSERT INTO `city` VALUES ('31', '上海');
INSERT INTO `city` VALUES ('32', '天津');
INSERT INTO `city` VALUES ('33', '香港');
INSERT INTO `city` VALUES ('34', '澳门');

-- ----------------------------
-- Table structure for communicate
-- ----------------------------
DROP TABLE IF EXISTS `communicate`;
CREATE TABLE `communicate` (
  `communicate_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `pulish_time` datetime DEFAULT NULL,
  `share` varchar(255) DEFAULT NULL COMMENT '发布消息内容',
  `status` int(11) DEFAULT '0' COMMENT '消息审核状态',
  `permisson` int(255) DEFAULT NULL COMMENT '可见权限0所有人1仅好友2营地3营地及好友',
  `camp_id` int(11) DEFAULT NULL,
  `longitude` double(255,5) DEFAULT NULL COMMENT '经度',
  `latitude` varchar(255) DEFAULT NULL COMMENT '纬度',
  PRIMARY KEY (`communicate_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of communicate
-- ----------------------------
INSERT INTO `communicate` VALUES ('9', '3', '2020-05-21 02:32:16', '789798898', '0', '0', '1', '106.55073', '29.56471');
INSERT INTO `communicate` VALUES ('10', '2', '2020-05-22 21:33:16', '123sdjkahlkds', '0', '0', '1', '106.55073', '29.56471');
INSERT INTO `communicate` VALUES ('11', '2', '2020-05-23 17:06:37', '123123', '0', '0', '1', '106.55063', '29.563424258617054');
INSERT INTO `communicate` VALUES ('12', '2', '2020-05-29 00:36:41', 'aasasdfsa', '0', '0', '1', '106.55159', '29.56518');
INSERT INTO `communicate` VALUES ('13', '3', '2020-05-29 17:06:34', '1', '0', '0', '1', '106.55073', '29.56471');
INSERT INTO `communicate` VALUES ('14', '3', '2020-05-29 17:06:32', '1', '0', '0', '1', '106.55073', '29.56471');
INSERT INTO `communicate` VALUES ('15', '3', '2020-05-29 16:56:12', '1', '0', '0', '1', '106.55073', '29.56471');
INSERT INTO `communicate` VALUES ('16', '3', '2020-05-29 16:56:25', '1', '0', '0', '1', '106.55073', '29.56471');
INSERT INTO `communicate` VALUES ('17', '3', '2020-05-29 16:57:30', '123123123', '0', '0', '1', '106.55159', '29.56518');
INSERT INTO `communicate` VALUES ('18', '3', '2020-05-29 16:58:42', '12', '0', '0', '1', '106.55073', '29.56471');
INSERT INTO `communicate` VALUES ('19', '3', '2020-05-29 16:58:50', '12', '0', '0', '1', '106.55073', '29.56471');
INSERT INTO `communicate` VALUES ('20', '3', '2020-05-29 16:58:57', '12', '0', '0', '1', '106.55073', '29.56471');
INSERT INTO `communicate` VALUES ('21', '3', '2020-05-30 13:44:11', '我的评论功能成功了', '0', '1', '1', '106.63043', '29.71798');
INSERT INTO `communicate` VALUES ('22', '2', '2020-05-30 13:46:42', '哈哈哈', '0', '0', '1', '106.63043', '29.71798');

-- ----------------------------
-- Table structure for communicate_comment
-- ----------------------------
DROP TABLE IF EXISTS `communicate_comment`;
CREATE TABLE `communicate_comment` (
  `comment_id` int(11) NOT NULL AUTO_INCREMENT,
  `communicate_id` int(11) DEFAULT NULL COMMENT '交流id',
  PRIMARY KEY (`comment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of communicate_comment
-- ----------------------------
INSERT INTO `communicate_comment` VALUES ('1', '20');
INSERT INTO `communicate_comment` VALUES ('3', '20');
INSERT INTO `communicate_comment` VALUES ('4', '22');
INSERT INTO `communicate_comment` VALUES ('5', '22');
INSERT INTO `communicate_comment` VALUES ('6', '22');
INSERT INTO `communicate_comment` VALUES ('7', '22');
INSERT INTO `communicate_comment` VALUES ('8', '19');
INSERT INTO `communicate_comment` VALUES ('9', '22');
INSERT INTO `communicate_comment` VALUES ('10', '13');
INSERT INTO `communicate_comment` VALUES ('11', '22');

-- ----------------------------
-- Table structure for communicate_context
-- ----------------------------
DROP TABLE IF EXISTS `communicate_context`;
CREATE TABLE `communicate_context` (
  `context_id` int(11) NOT NULL AUTO_INCREMENT,
  `comment_id` int(11) DEFAULT NULL COMMENT '评论id',
  `user_id` int(11) DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '评论时间',
  `comment_text` varchar(255) DEFAULT NULL COMMENT '评论内容',
  PRIMARY KEY (`context_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of communicate_context
-- ----------------------------
INSERT INTO `communicate_context` VALUES ('1', '1', '3', '2020-05-29 01:11:49', 'huifu');
INSERT INTO `communicate_context` VALUES ('2', '1', '2', '2020-05-30 01:58:30', '123');
INSERT INTO `communicate_context` VALUES ('3', '1', '2', '2020-05-30 13:42:02', '回复lisi:回复测试');
INSERT INTO `communicate_context` VALUES ('4', '3', '2', '2020-05-30 13:42:45', '评论测试');
INSERT INTO `communicate_context` VALUES ('5', '4', '3', '2020-05-30 13:47:01', '自己嗨');
INSERT INTO `communicate_context` VALUES ('6', '5', '3', '2020-05-30 13:47:16', '测试');
INSERT INTO `communicate_context` VALUES ('7', '6', '3', '2020-06-05 12:26:20', '111');
INSERT INTO `communicate_context` VALUES ('8', '7', '2', '2020-06-05 15:02:37', '111');
INSERT INTO `communicate_context` VALUES ('9', '8', '3', '2020-06-06 12:15:55', '春');
INSERT INTO `communicate_context` VALUES ('10', '9', '3', '2020-06-06 12:35:47', '11');
INSERT INTO `communicate_context` VALUES ('11', '10', '2', '2020-06-06 19:29:14', '啦啦啦啦');
INSERT INTO `communicate_context` VALUES ('12', '11', '2', '2020-06-08 10:17:16', '。。。。');

-- ----------------------------
-- Table structure for coordinate
-- ----------------------------
DROP TABLE IF EXISTS `coordinate`;
CREATE TABLE `coordinate` (
  `coordinate_id` int(11) NOT NULL AUTO_INCREMENT,
  `camp_name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `latitude` double(10,6) DEFAULT NULL,
  `longitude` double(10,6) DEFAULT NULL,
  PRIMARY KEY (`coordinate_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of coordinate
-- ----------------------------
INSERT INTO `coordinate` VALUES ('1', '悦客茶山竹海房车营地', '重庆市永川区城北茶山竹海街道办事处辖区', '重庆市', '29.356117', '105.927376');
INSERT INTO `coordinate` VALUES ('2', '悦客沙坪坝区花马山房车露营基地', '重庆市沙坪坝区大学城重庆\r\n师范大学西部', '重庆市', '29.555868', '106.459004');
INSERT INTO `coordinate` VALUES ('3', '悦客南川金佛山房车营地', '重庆市南川区金佛山自然保\r\n护', '重庆市', '29.048890', '107.127850');
INSERT INTO `coordinate` VALUES ('4', '悦客万盛黑山谷房车营地', '重庆市南川区綦江区万盛经济开发区黑山谷1098 ,近414省道', '重庆市', '28.928079', '106.965552');

-- ----------------------------
-- Table structure for food
-- ----------------------------
DROP TABLE IF EXISTS `food`;
CREATE TABLE `food` (
  `food_id` int(11) NOT NULL AUTO_INCREMENT,
  `camp_id` int(11) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `start_time` datetime DEFAULT NULL COMMENT '开始营业时间',
  `tel` varchar(20) DEFAULT NULL,
  `describe` varchar(255) DEFAULT NULL,
  `food_name` varchar(255) DEFAULT NULL,
  `price` double DEFAULT NULL COMMENT '价格',
  `stock` int(255) DEFAULT NULL COMMENT '库存',
  `end_time` datetime DEFAULT NULL COMMENT '营业结束时间',
  PRIMARY KEY (`food_id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of food
-- ----------------------------
INSERT INTO `food` VALUES ('1', '1', '重庆市永川区城北茶山竹海街道办事处辖区', '1970-01-01 08:00:00', '14345788973', '鱼香肉丝（英语：Yuxiang shredded pork），是川菜中的一道名品，主要是用鲫鱼腌制的泡辣椒炒制猪里脊肉丝而成，以此将肉丝炒出鱼香味，故而叫鱼香肉丝。鱼香肉丝的历史不长，...', '鱼香肉丝', '22', '2000', '1970-01-01 22:00:00');
INSERT INTO `food` VALUES ('3', '1', '悦客沙坪坝区花马山房车露营基地', '2020-06-08 08:00:00', '14345788973', '火爆双脆是一道四川省的特色传统名菜，属于川菜系。主料为猪肚和鸡肫，其口味特别，脆嫩爽口，咸鲜味美，是一道简单易做但是味道可口的家常菜。', '火爆双脆', '100', '678', '2020-06-03 22:00:00');
INSERT INTO `food` VALUES ('4', '2', '悦客茶山竹海房车营地', '2020-06-08 08:00:00', '15343562576', '土豆肉丝土豆炒肉丝 蒜、土豆、肉丝、青椒、料酒、淀粉、盐、生抽、老抽、糖 0 做过 朵朵的大好熊 土豆炒肉丝 土豆、肉丝、老抽、盐、淀粉、花椒、干...', '土豆肉丝', '55', '1000', '2020-06-03 22:00:00');
INSERT INTO `food` VALUES ('5', '2', '悦客茶山竹海房车营地', '2020-06-08 08:00:00', '15343562576', '水煮鱼也称为江水煮江鱼、水煮鱼片，最早流行于重庆市渝北区翠云乡。水煮鱼通常由新鲜草鱼、豆芽、辣椒等食材制作而成。“油而不腻、辣而不燥、麻而不苦、肉质滑嫩”是其特色。', '水煮鱼', '32', '1000', '2020-06-03 22:00:00');
INSERT INTO `food` VALUES ('6', '2', '悦客茶山竹海房车营地', '2020-06-08 08:00:00', '15343562576', '重庆火锅又称为毛肚火锅或麻辣火锅，是中国传统饮食方式，起源于明末清初的重庆嘉陵江畔、朝天门等码头船工纤夫的粗放餐饮方式，原料主要是牛毛肚、猪黄喉、鸭肠、牛血旺等。...', '重庆火锅', '34', '1000', '2020-06-03 22:00:00');
INSERT INTO `food` VALUES ('7', '3', '悦客南川金佛山房车营地', '2020-06-08 08:00:00', '16343561439', '辣子鸡，是一道经典的川菜。一般以整鸡为主料，加上葱、干辣椒、花椒、盐、胡椒、味精等多种材料精制而成，虽然是同一道菜，各地制作也各有特色。辣子鸡因各地的不同制作方法也有不同的特色，深受各地人们的喜爱。此菜成菜色泽棕红油亮，麻辣味浓。', '辣子鸡', '23', '1000', '2020-06-03 22:00:00');
INSERT INTO `food` VALUES ('8', '3', '悦客南川金佛山房车营地', '2020-06-08 08:00:00', '16343561439', '卤鸡翅选用上等鸡翅为主料，使用传统配方和现代食品生产工艺精制而成。色泽自然，鲜香浓郁、风味独特，口感俱佳，老少皆宜。是佐酒伴餐之佳肴，休闲旅游之上品。', '卤鸡翅', '24', '1000', '2020-06-03 22:00:00');
INSERT INTO `food` VALUES ('9', '3', '悦客南川金佛山房车营地', '2020-06-08 08:00:00', '16343561439', '重庆黔江鸡杂的做法,重庆黔江鸡杂怎么做请看重庆黔江鸡杂的做法步骤图:1、鸡胗、鸡心、鸡肝切成片,鸡肠切成长段,加入盐、料酒,小苏打,生粉腌制 2、大蒜切成片,...', '黔江鸡杂', '55', '1000', '2020-06-03 22:00:00');
INSERT INTO `food` VALUES ('10', '4', '悦客万盛黑山谷房车营地', '2020-06-08 08:00:00', '15712323424', '白市驿板鸭是重庆著名的特色小吃，以产于重庆九龙坡区白市驿镇而得名，素以色、香、味、型俱佳而闻名于世。白市驿板鸭形呈蒲扇状，色泽金黄，清香鲜美。食法多种多样，蒸、煮、...', '白市驿板鸭', '22', '1000', '2020-06-03 22:00:00');
INSERT INTO `food` VALUES ('11', '4', '悦客万盛黑山谷房车营地', '2020-06-08 08:00:00', '15712323424', '重庆蝶花牌怪味胡豆具有特有的工艺、独有的风味，集麻、辣、甜、咸、鲜、酥为一体，堪称一绝。', '重庆蝶花牌怪味胡豆', '34', '1000', '2020-06-03 22:00:00');
INSERT INTO `food` VALUES ('12', '4', '悦客万盛黑山谷房车营地', '2020-06-08 08:00:00', '15712323424', '麻辣烤鱼是四川及重庆的传统名吃，制作原料主要有草鱼（鲤鱼等其它鱼也可以）、香菜、青笋等。属于川菜系。', '麻辣烤鱼', '65', '1000', '2020-06-03 22:00:00');
INSERT INTO `food` VALUES ('13', '4', '悦客万盛黑山谷房车营地', '2020-06-08 08:00:00', '15712323424', '“九园包子”起源于重庆民国时期的 1931 年，享有中华名小吃、重庆老字号、重庆非物质文化遗产、中国十大包子名店及中国特色包子等荣誉的“九园包子”，具有较好的品牌塑造力和..', '九园包子', '77', '1000', '2020-06-03 22:00:00');
INSERT INTO `food` VALUES ('21', '1', '', '2020-06-04 18:08:35', '123123123', '123123', '1231', '132', '13', '2020-06-04 18:30:00');
INSERT INTO `food` VALUES ('22', null, '重庆市沙坪坝区大学城重庆\r\n师范大学西部', null, null, null, null, null, null, null);
INSERT INTO `food` VALUES ('23', '1', '', '2020-06-04 18:12:03', '123', '123', '123', '123', '123', '2020-06-04 18:30:00');
INSERT INTO `food` VALUES ('24', '1', '', '2020-06-04 18:12:03', '123', '132123', '123', '123', '123', '2020-06-04 18:30:00');
INSERT INTO `food` VALUES ('25', '2', '', '2020-06-04 18:18:50', '123123', '132213', '123', '123', '132', '2020-06-04 18:30:00');
INSERT INTO `food` VALUES ('26', '2', '重庆市永川区城北茶山竹海街道办事处辖区', '2020-06-04 18:20:56', '123123', '123123', '123', '123', '123', '2020-06-04 18:30:00');

-- ----------------------------
-- Table structure for food_collection
-- ----------------------------
DROP TABLE IF EXISTS `food_collection`;
CREATE TABLE `food_collection` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL COMMENT '用户Id',
  `food_id` int(11) DEFAULT NULL COMMENT '美食Id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of food_collection
-- ----------------------------

-- ----------------------------
-- Table structure for food_comment
-- ----------------------------
DROP TABLE IF EXISTS `food_comment`;
CREATE TABLE `food_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `food_id` int(11) DEFAULT NULL COMMENT '美食id',
  `user_id` int(11) DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '评论时间',
  `comment` varchar(255) DEFAULT NULL COMMENT '评论内容',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of food_comment
-- ----------------------------

-- ----------------------------
-- Table structure for food_order
-- ----------------------------
DROP TABLE IF EXISTS `food_order`;
CREATE TABLE `food_order` (
  `food_order_id` int(11) NOT NULL AUTO_INCREMENT,
  `food_id` int(11) DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  `order_time` datetime DEFAULT NULL,
  `price` double DEFAULT NULL COMMENT '单价',
  `total` double DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `camp_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `car_id` int(11) DEFAULT NULL COMMENT '房车id',
  PRIMARY KEY (`food_order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of food_order
-- ----------------------------
INSERT INTO `food_order` VALUES ('1', '1', '1', '2020-06-16 21:46:48', '22', '22', '1', '1', '1', '1');
INSERT INTO `food_order` VALUES ('2', '3', '2', '2020-06-17 11:34:37', '100', '200', '1', '2', '2', '1');
INSERT INTO `food_order` VALUES ('3', '4', '1', '2020-06-17 11:35:23', '55', '55', '1', '3', '3', '1');

-- ----------------------------
-- Table structure for friend_ship
-- ----------------------------
DROP TABLE IF EXISTS `friend_ship`;
CREATE TABLE `friend_ship` (
  `friend_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `f_user_id` int(11) DEFAULT NULL,
  `status` int(255) DEFAULT NULL COMMENT '好友申请状态，0未处理，1成功，2拒绝',
  PRIMARY KEY (`friend_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of friend_ship
-- ----------------------------

-- ----------------------------
-- Table structure for img
-- ----------------------------
DROP TABLE IF EXISTS `img`;
CREATE TABLE `img` (
  `img_id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `other_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`img_id`)
) ENGINE=InnoDB AUTO_INCREMENT=98 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of img
-- ----------------------------
INSERT INTO `img` VALUES ('1', '/static/Liu/fangchemodel/fangche1.png', '5', '1');
INSERT INTO `img` VALUES ('2', '/static/Liu/fangchemodel/fangche2.png', '5', '1');
INSERT INTO `img` VALUES ('3', '/static/Liu/fangchemodel/fangche3.png', '5', '1');
INSERT INTO `img` VALUES ('4', '/static/Liu/fangchemodel/fangche4.png', '5', '2');
INSERT INTO `img` VALUES ('5', '/static/Liu/fangchemodel/fangche5.png', '5', '2');
INSERT INTO `img` VALUES ('6', '/static//Liu/fangchemodel/fangche6.png', '5', '2');
INSERT INTO `img` VALUES ('7', '/static//Liu/wangyuanjing/wyj1.png', '5', '4');
INSERT INTO `img` VALUES ('8', '/static//Liu/wangyuanjing/wyj2.png', '5', '4');
INSERT INTO `img` VALUES ('9', '/static//Liu/wangyuanjing/wyj3.png', '5', '5');
INSERT INTO `img` VALUES ('10', '/static//Liu/wangyuanjing/wyj4.png', '5', '5');
INSERT INTO `img` VALUES ('11', '/static//Liu/wangyuanjing/wyj5.png', '5', '6');
INSERT INTO `img` VALUES ('12', '/static//Liu/wangyuanjing/wyj1.png', '5', '6');
INSERT INTO `img` VALUES ('13', '/static/Liu/fangchemodel/fangche7.png', '5', '3');
INSERT INTO `img` VALUES ('14', '/static/Liu/fangchemodel/fangche8.png', '5', '3');
INSERT INTO `img` VALUES ('15', '/static/Liu/tutechan/ttc1.png', '5', '7');
INSERT INTO `img` VALUES ('16', '/static/Liu/tutechan/ttc2.png', '5', '7');
INSERT INTO `img` VALUES ('17', '/static/Liu/tutechan/ttc3.png', '5', '7');
INSERT INTO `img` VALUES ('18', '/static/Liu/tutechan/ttc4.png', '5', '8');
INSERT INTO `img` VALUES ('19', '/static/Liu/tutechan/ttc5.png', '5', '9');
INSERT INTO `img` VALUES ('20', '/static/Liu/tutechan/ttc6.png', '5', '9');
INSERT INTO `img` VALUES ('21', '/static/Liu/tutechan/ttc7.png', '5', '9');
INSERT INTO `img` VALUES ('22', '/static/Liu/tutechan/ttc8.png', '5', '9');
INSERT INTO `img` VALUES ('23', '/static/Liu/tutechan/ttc9.png', '5', '9');
INSERT INTO `img` VALUES ('24', '/static/Liu/tutechan/ttc10.png', '5', '8');
INSERT INTO `img` VALUES ('25', '/static/Liu/fangchemodel/fangche11.png', '5', '10');
INSERT INTO `img` VALUES ('26', '/static/Liu/fangchemodel/fangche12.png', '5', '10');
INSERT INTO `img` VALUES ('27', '/static/Liu/fangchemodel/fangche1.png', '5', '11');
INSERT INTO `img` VALUES ('28', '/static/Liu/fangchemodel/fangche8.png', '5', '11');
INSERT INTO `img` VALUES ('29', '/static/Liu/fangchemodel/fangche10.png', '5', '12');
INSERT INTO `img` VALUES ('30', '/static/Liu/fangchemodel/fangche9.png', '5', '12');
INSERT INTO `img` VALUES ('31', '/static/Liu/fangchemodel/fangche4.png', '5', '13');
INSERT INTO `img` VALUES ('32', '/static/Liu/fangchemodel/fangche3.png', '5', '13');
INSERT INTO `img` VALUES ('33', '/static/Liu/fangchemodel/fangche2.png', '5', '14');
INSERT INTO `img` VALUES ('34', '/static/Liu/fangchemodel/fangche6.png', '5', '14');
INSERT INTO `img` VALUES ('35', '/static/Liu/fangchemodel/fangche8.png', '5', '15');
INSERT INTO `img` VALUES ('36', '/static/Liu/fangchemodel/fangche9.png', '5', '15');
INSERT INTO `img` VALUES ('37', '/static/Liu/fangchemodel/fangche7.png', '5', '16');
INSERT INTO `img` VALUES ('38', '/static/Liu/fangchemodel/fangche10.png', '5', '16');
INSERT INTO `img` VALUES ('39', '/static/Liu/fangchemodel/fangche5.png', '5', '17');
INSERT INTO `img` VALUES ('40', '/static/Liu/fangchemodel/fangche3.png', '5', '17');
INSERT INTO `img` VALUES ('41', '/static/Liu/fangchemodel/fangche2.png', '5', '18');
INSERT INTO `img` VALUES ('42', '/static/Liu/fangchemodel/fangche11.png', '5', '18');
INSERT INTO `img` VALUES ('43', '/static/Liu/fangchemodel/fangche12.png', '5', '19');
INSERT INTO `img` VALUES ('44', '/static/Liu/fangchemodel/fangche3.png', '5', '19');
INSERT INTO `img` VALUES ('45', '/static/Liu/fangchemodel/fangche6.png', '5', '10');
INSERT INTO `img` VALUES ('46', '/static/Liu/fangchemodel/fangche8.png', '5', '11');
INSERT INTO `img` VALUES ('47', '/static/Liu/fangchemodel/fangche9.png', '5', '12');
INSERT INTO `img` VALUES ('48', '/static/Liu/chengkoularou/cklr1.jpg', '5', '20');
INSERT INTO `img` VALUES ('49', '/static/Liu/chengkoularou/cklr2.jpg', '5', '20');
INSERT INTO `img` VALUES ('50', '/static/Liu/chengkoularou/cklr3.jpg', '5', '20');
INSERT INTO `img` VALUES ('51', '/static/Liu/chenmahua/cmh1.jpg', '5', '21');
INSERT INTO `img` VALUES ('52', '/static/Liu/chenmahua/cmh2.jpg', '5', '21');
INSERT INTO `img` VALUES ('53', '/static/Liu/chenmahua/cmh3.jpg', '5', '21');
INSERT INTO `img` VALUES ('54', '/static/Liu/xianggudougan/xgdg1.jpg', '5', '22');
INSERT INTO `img` VALUES ('55', '/static/Liu/xianggudougan/xgdg2.jpg', '5', '22');
INSERT INTO `img` VALUES ('56', '/static/Liu/xianggudougan/xgdg3.jpg', '5', '22');
INSERT INTO `img` VALUES ('57', 'http://www.joyrv.cn//Public/Uploads/yk/5ba48f373ed27.jpg', '1', '2');
INSERT INTO `img` VALUES ('58', 'http://www.joyrv.cn//Public/Uploads/yk/5ba4911837771.jpg', '1', '4');
INSERT INTO `img` VALUES ('59', 'http://www.joyrv.cn//Public/Uploads/yk/5ba48f91bba27.jpg', '1', '3');
INSERT INTO `img` VALUES ('60', 'http://www.joyrv.cn//Public/Uploads/yk/5ba46784607e5.jpg', '1', '1');
INSERT INTO `img` VALUES ('61', 'http://www.joyrv.cn//Public/Uploads/yk/5ba46784607e5.jpg', '1', '1');
INSERT INTO `img` VALUES ('62', 'http://www.joyrv.cn//Public/Uploads/yk/5ba48f91bba27.jpg', '1', '3');
INSERT INTO `img` VALUES ('63', 'http://www.joyrv.cn//Public/Uploads/shop/store/goods/105/105_05888552483801947_345.png', '2', '1');
INSERT INTO `img` VALUES ('64', 'http://www.joyrv.cn//Public/Uploads/shop/store/goods/105/105_05943125226911003_345.png', '2', '2');
INSERT INTO `img` VALUES ('65', 'http://www.joyrv.cn//Public/Uploads/shop/store/goods/105/105_05942095702423465_345.png', '2', '3');
INSERT INTO `img` VALUES ('66', 'http://www.joyrv.cn//Public/Uploads/shop/store/goods/105/105_05943124059656133_345.png', '2', '4');
INSERT INTO `img` VALUES ('67', '/static/food/dishes/Dishes1.png', '4', '1');
INSERT INTO `img` VALUES ('68', '/static/food/dishes/Dishes2.png', '4', '2');
INSERT INTO `img` VALUES ('69', '/static/food/dishes/Dishes3.png', '4', '3');
INSERT INTO `img` VALUES ('70', '/static/food/dishes/Dishes3.png', '4', '4');
INSERT INTO `img` VALUES ('71', '/static/food/dishes/Dishes3.png', '4', '5');
INSERT INTO `img` VALUES ('72', '/static/food/dishes/Dishes3.png', '4', '6');
INSERT INTO `img` VALUES ('73', '/static/food/dishes/Dishes3.png', '4', '7');
INSERT INTO `img` VALUES ('74', '/static/food/dishes/Dishes3.png', '4', '8');
INSERT INTO `img` VALUES ('75', '/static/food/dishes/Dishes3.png', '4', '9');
INSERT INTO `img` VALUES ('77', '/static/food/dishes/Dishes3.png', '4', '10');
INSERT INTO `img` VALUES ('78', '/static/food/dishes/Dishes3.png', '4', '11');
INSERT INTO `img` VALUES ('79', '/static/food/dishes/Dishes3.png', '4', '12');
INSERT INTO `img` VALUES ('80', '/static/food/dishes/Dishes3.png', '4', '13');
INSERT INTO `img` VALUES ('89', 'http://qbc7j9pld.bkt.clouddn.com/2020-06-04T18:08:55.01_QQ截图20191116195432.png', '4', '21');
INSERT INTO `img` VALUES ('90', 'http://qbc7j9pld.bkt.clouddn.com/2020-06-04T18:12:13.269_QQ截图20191116195432.png', '4', '23');
INSERT INTO `img` VALUES ('91', 'http://qbc7j9pld.bkt.clouddn.com/2020-06-04T18:15:43.589_QQ截图20191116195432.png', '4', '24');
INSERT INTO `img` VALUES ('92', 'http://qbc7j9pld.bkt.clouddn.com/2020-06-04T18:19:01.045_QQ截图20191116195432.png', '4', '25');
INSERT INTO `img` VALUES ('93', 'http://qbc7j9pld.bkt.clouddn.com/2020-06-04T18:21:05.108_QQ截图20191116195432.png', '4', '26');
INSERT INTO `img` VALUES ('94', 'http://www.joyrv.cn//Public/Uploads/shop/store/goods/105/105_05943124059656133_345.png', '7', '1');
INSERT INTO `img` VALUES ('95', 'http://www.joyrv.cn//Public/Uploads/shop/store/goods/105/105_05943124059656133_345.png', '7', '1');
INSERT INTO `img` VALUES ('96', 'http://www.joyrv.cn//Public/Uploads/shop/store/goods/105/105_05943124059656133_345.png', '7', '2');
INSERT INTO `img` VALUES ('97', 'http://www.joyrv.cn//Public/Uploads/shop/store/goods/105/105_05943124059656133_345.png', '7', '2');

-- ----------------------------
-- Table structure for img_type
-- ----------------------------
DROP TABLE IF EXISTS `img_type`;
CREATE TABLE `img_type` (
  `type_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) DEFAULT NULL COMMENT '种类',
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of img_type
-- ----------------------------
INSERT INTO `img_type` VALUES ('1', '营地');
INSERT INTO `img_type` VALUES ('2', '美食');

-- ----------------------------
-- Table structure for js_collection
-- ----------------------------
DROP TABLE IF EXISTS `js_collection`;
CREATE TABLE `js_collection` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `user_id` int(255) DEFAULT NULL,
  `proId` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of js_collection
-- ----------------------------
INSERT INTO `js_collection` VALUES ('2', '2', '2');
INSERT INTO `js_collection` VALUES ('49', '1', '20');
INSERT INTO `js_collection` VALUES ('55', '2', '1');

-- ----------------------------
-- Table structure for js_shopping_car
-- ----------------------------
DROP TABLE IF EXISTS `js_shopping_car`;
CREATE TABLE `js_shopping_car` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `user_id` int(255) DEFAULT NULL,
  `pro_Id` int(255) DEFAULT NULL,
  `number` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of js_shopping_car
-- ----------------------------
INSERT INTO `js_shopping_car` VALUES ('43', '1', '1', '10');
INSERT INTO `js_shopping_car` VALUES ('47', '1', '20', '8');
INSERT INTO `js_shopping_car` VALUES ('48', '2', '3', '1');
INSERT INTO `js_shopping_car` VALUES ('49', '2', '1', '3');
INSERT INTO `js_shopping_car` VALUES ('51', '1', '8', '5');
INSERT INTO `js_shopping_car` VALUES ('52', '1', '4', '1');
INSERT INTO `js_shopping_car` VALUES ('53', '2', '5', '1');

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `m_id` int(11) NOT NULL AUTO_INCREMENT,
  `m_name` varchar(255) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  PRIMARY KEY (`m_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES ('1', '房车营地', '0', 'javascript:', '1');
INSERT INTO `menu` VALUES ('2', '查询营地', '1', 'javascript:', '2');
INSERT INTO `menu` VALUES ('3', '新增营地', '2', 'javascript:', '3');

-- ----------------------------
-- Table structure for point_like
-- ----------------------------
DROP TABLE IF EXISTS `point_like`;
CREATE TABLE `point_like` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '点赞id',
  `user_id` int(11) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `communicate_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of point_like
-- ----------------------------
INSERT INTO `point_like` VALUES ('4', '2', 'lisi', '12');
INSERT INTO `point_like` VALUES ('6', '2', 'lisi', '10');
INSERT INTO `point_like` VALUES ('7', '2', 'lisi', '9');
INSERT INTO `point_like` VALUES ('8', '2', 'lisi', '11');
INSERT INTO `point_like` VALUES ('11', '3', 'zhangsan', '9');
INSERT INTO `point_like` VALUES ('13', '2', 'lisi', '19');
INSERT INTO `point_like` VALUES ('35', '2', 'lisi', '17');
INSERT INTO `point_like` VALUES ('41', '2', 'lisi', '20');
INSERT INTO `point_like` VALUES ('44', '3', 'zhangsan', '22');
INSERT INTO `point_like` VALUES ('45', '3', 'zhangsan', '21');
INSERT INTO `point_like` VALUES ('53', '2', '李四', '13');
INSERT INTO `point_like` VALUES ('55', '2', '李四', '22');
INSERT INTO `point_like` VALUES ('59', '3', '王五', '22');

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `pro_id` int(11) NOT NULL AUTO_INCREMENT,
  `pro_name` varchar(255) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL COMMENT '商品类别id',
  `price` double(255,2) DEFAULT NULL,
  `stock` int(255) DEFAULT NULL,
  `describe` varchar(255) DEFAULT NULL COMMENT '商品描述',
  `status` int(255) DEFAULT NULL COMMENT '上架 下架',
  PRIMARY KEY (`pro_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES ('1', '悍马警车1:32声光回力合金车模警车仿嗔汽车模', '1', '67.50', '890', '如果说能够拥有一辆房车就已经足够让人幸福，那么那些既拥有房车又拥有房车模型的人，简直是令人羡慕嫉妒恨。说到房车模型，它算得上是房车的周边衍生品，在很多人都因为房车价格高昂过于高昂而无法购买时，另一部分人则先将房车模型购买回来，既当做收藏品，又当做一种激励自己购买房车的小物件！其实，一些限量版的房车模型，除了有收藏价值外，更有一些经济价值。世界上知名的房车产品，都会有相应的模型供人欣赏与收藏，下面我们就来看一看！', '1');
INSERT INTO `product` VALUES ('2', '彩珀合金房车儿童玩具旅行巴士合金小汽车仿真开门]声光力小汽车模型合金房车', '1', '56.50', '780', '彩珀合金房车儿童玩具旅行巴士合金小汽车仿真开门]声光力小汽车模型合金房车彩珀合金房车儿童玩具旅行巴士合金小汽车仿真开门]声光力小汽车模型合金房车彩珀合金房车儿童玩具旅行巴士合金小汽车仿真开门]声光力小汽车模型合金房车彩珀合金房车儿童玩具旅行巴士合金小汽车仿真开门]声光力小汽车模型合金房车彩珀合金房车儿童玩具旅行巴士合金小汽车仿真开门]声光力小汽车模型合金房车彩珀合金房车儿童玩具旅行巴士合金小汽车仿真开门]声光力小汽车模型合金房车彩珀合金房车儿童玩具旅行巴士合金小汽车仿真开门]声光力小汽车模型合金房车', '1');
INSERT INTO `product` VALUES ('3', '仿真开门]声光力小汽车模型合金房车', '1', '45.00', '564', '仿真开门]声光力小汽车模型合金房车仿真开门]声光力小汽车模型合金房车仿真开门]声光力小汽车模型合金房车仿真开门]声光力小汽车模型合金房车仿真开门]声光力小汽车模型合金房车仿真开门]声光力小汽车模型合金房车仿真开门]声光力小汽车模型合金房车仿真开门]声光力小汽车模型合金房车仿真开门]声光力小汽车模型合金房车仿真开门]声光力小汽车模型合金房车仿真开门]声光力小汽车模型合金房车仿真开门]声光力小汽车模型合金房车仿真开门]声光力小汽车模型合金房车仿真开门]声光力小汽车模型合金房车仿真开门]声光力小汽车模型合金房车', '1');
INSERT INTO `product` VALUES ('4', '望远镜1', '2', '159.00', '345', '望远镜1望远镜1望远镜1望远镜1望远镜1望远镜1望远镜1望远镜1望远镜1望远镜1望远镜1望远镜1望远镜1望远镜1望远镜1', '1');
INSERT INTO `product` VALUES ('5', '望远镜2', '2', '188.00', '245', '望远镜2望远镜2望远镜2望远镜2望远镜2望远镜2望远镜2望远镜2望远镜2望远镜2望远镜2', '1');
INSERT INTO `product` VALUES ('6', '望远镜3', '2', '147.00', '146', '望远镜3望远镜3望远镜3望远镜3望远镜3望远镜3望远镜3望远镜3望远镜3望远镜3', '1');
INSERT INTO `product` VALUES ('7', '腊肉嘎儿1', '3', '149.00', '454', '腊肉嘎儿1腊肉嘎儿1腊肉嘎儿1腊肉嘎儿1腊肉嘎儿1腊肉嘎儿1腊肉嘎儿1腊肉嘎儿1腊肉嘎儿1腊肉嘎儿1', '1');
INSERT INTO `product` VALUES ('8', '回锅肉特产', '3', '199.00', '567', '回锅肉特产回锅肉特产回锅肉特产回锅肉特产回锅肉特产回锅肉特产回锅肉特产回锅肉特产回锅肉特产回锅肉特产', '1');
INSERT INTO `product` VALUES ('9', '豆干羊角', '3', '129.00', '899', '豆干羊角豆干羊角豆干羊角豆干羊角豆干羊角豆干羊角豆干羊角豆干羊角豆干羊角豆干羊角', '1');
INSERT INTO `product` VALUES ('10', '悍马警车2', '1', '123.00', '678', '悍马警车2悍马警车2悍马警车2悍马警车2悍马警车2悍马警车2悍马警车2悍马警车2悍马警车2悍马警车2悍马警车2悍马警车2悍马警车2悍马警车2悍马警车2', '1');
INSERT INTO `product` VALUES ('11', '悍马警车3', '1', '121.00', '453', '悍马警车2悍马警车2悍马警车2悍马警车2悍马警车2悍马警车2悍马警车2悍马警车2悍马警车2悍马警车2悍马警车2悍马警车2悍马警车2悍马警车2悍马警车2', '1');
INSERT INTO `product` VALUES ('12', '悍马警车4', '1', '145.00', '456', '悍马警车4悍马警车4悍马警车4悍马警车4悍马警车4悍马警车4悍马警车4悍马警车4悍马警车4悍马警车4悍马警车4悍马警车4悍马警车4悍马警车4悍马警车4', '1');
INSERT INTO `product` VALUES ('13', '悍马警车5', '1', '167.00', '578', '悍马警车5悍马警车5悍马警车5悍马警车5悍马警车5悍马警车5悍马警车5悍马警车5悍马警车5悍马警车5悍马警车5悍马警车5悍马警车5悍马警车5', '1');
INSERT INTO `product` VALUES ('14', '悍马警车6', '1', '123.00', '245', '悍马警车6悍马警车6悍马警车6悍马警车6悍马警车6悍马警车6悍马警车6悍马警车6悍马警车6悍马警车6悍马警车6悍马警车6悍马警车6悍马警车6悍马警车6', '1');
INSERT INTO `product` VALUES ('15', '悍马警车7', '1', '78.00', '689', '悍马警车7悍马警车7悍马警车7悍马警车7悍马警车7悍马警车7悍马警车7悍马警车7悍马警车7悍马警车7悍马警车7悍马警车7悍马警车7悍马警车7悍马警车7', '1');
INSERT INTO `product` VALUES ('16', '悍马警车8', '1', '67.00', '532', '悍马警车8悍马警车8悍马警车8悍马警车8悍马警车8悍马警车8悍马警车8悍马警车8悍马警车8悍马警车8悍马警车8悍马警车8悍马警车8悍马警车8悍马警车8', '1');
INSERT INTO `product` VALUES ('17', '悍马警车9', '1', '78.00', '689', '悍马警车9悍马警车9悍马警车9悍马警车9悍马警车9悍马警车9悍马警车9悍马警车9悍马警车9悍马警车9悍马警车9悍马警车9悍马警车9悍马警车9悍马警车9', '1');
INSERT INTO `product` VALUES ('18', '悍马警车10', '1', '59.00', '367', '悍马警车10悍马警车10悍马警车10悍马警车10悍马警车10悍马警车10悍马警车10悍马警车10悍马警车10悍马警车10悍马警车10悍马警车10悍马警车10悍马警车10悍马警车10', '1');
INSERT INTO `product` VALUES ('19', '悍马警车11', '1', '99.00', '567', '悍马警车11悍马警车11悍马警车11悍马警车11悍马警车11悍马警车11悍马警车11悍马警车11悍马警车11悍马警车11悍马警车11悍马警车11悍马警车11悍马警车11悍马警车11悍马警车11悍马警车11悍马警车11', '1');
INSERT INTO `product` VALUES ('20', '城口腊肉', '3', '199.00', '467', '城口“老腊肉”沿袭具有500多年历史的民间加工秘方，经特殊的传统工艺精心熏制而成，其肉质精良、香味纯正、营养丰富，是城口具有代表性的土特产之一。城口土猪种，喂养饲料具有地方特色；腌制过程除加有定量的食盐之外，还要添加适量的白酒及细辛、白芷等十几种中草药作调味料。', '1');
INSERT INTO `product` VALUES ('21', '陈麻花', '3', '69.00', '999', '陈麻花是重庆市知名的特色传统小吃。清朝末年，古镇陈麻花凭借其独特的口味从此在巴渝大地流传开来；因其选料上乘，采用全手工制作，具有香、酥、脆、爽，久放不棉等特点。椒盐麻花，口味纯正，酥脆化渣；麻辣麻花，重庆口味，集甜、麻、辣于一体，回味无穷；蜂蜜麻花，口味纯正，含有丰富的矿物质元素。', '1');
INSERT INTO `product` VALUES ('22', '重庆香菇豆干', '3', '88.00', '1034', '豆干是一种历史悠久的民间小吃，也是豆腐干的简称；豆腐干中含有丰富蛋白质，而且豆腐蛋白属完全蛋白，不仅含有人体必需的8种氨基酸，而且其比例也接近人体需要，营养价值较高；含有的卵磷脂可除掉附在血管壁上的胆固醇，防止血管硬化，预防心血管疾病，保护心脏；含有多种矿物质，补充钙质，防止因缺钙引起的骨质疏松，促进骨骼发育，对小儿、老人的骨骼生长极为有利.', '1');

-- ----------------------------
-- Table structure for product_comment
-- ----------------------------
DROP TABLE IF EXISTS `product_comment`;
CREATE TABLE `product_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pro_id` int(11) DEFAULT NULL COMMENT '商品id',
  `user_id` int(11) DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '评论时间',
  `comment` varchar(255) DEFAULT NULL COMMENT '评论内容',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of product_comment
-- ----------------------------
INSERT INTO `product_comment` VALUES ('1', '1', '1', '2020-05-05 21:46:52', '这款车只适合摆放着，不能玩，才拿到货倒车镜掉了一个在盒子里，问客服，说是这款倒车镜是胶粘的，给孩子玩了不到一个月车轮子又掉了一个，轮子捡起来一看，中间的轴也是胶粘的，想修都没法修，质量这么差，价格还不低');
INSERT INTO `product_comment` VALUES ('2', '1', '2', '2020-05-08 21:47:19', '这个模型还不错，做工也还算精细，只是没有防滑垫觉得缺点什么！');
INSERT INTO `product_comment` VALUES ('3', '1', '3', '2020-05-13 21:47:40', '车模还可以吧，细节做的很粗糙。尾翼随时都会掉下来，自己期望过高了，要不是孩子喜欢我会选择退了');
INSERT INTO `product_comment` VALUES ('4', '2', '1', '2020-05-01 23:52:14', '东西还是不错的，但是做的车贴错了，店家补贴了几块钱，赞一个');
INSERT INTO `product_comment` VALUES ('5', '2', '2', '2020-04-28 23:52:20', '不能开灯，不能叫。之前买了一个AE86可以开灯可以叫。这个没那功能。');
INSERT INTO `product_comment` VALUES ('6', '2', '3', '2020-05-09 23:52:24', '很多都有小问题，小瑕疵，品质不怎么好');
INSERT INTO `product_comment` VALUES ('7', '3', '1', '2020-04-26 23:52:28', '小瑕疵，有零星半点边缘位置掉漆，这个要减分！底盘等于玩具车吧，但是这是价位和厂家决定的，也没问题。总体还算满意');
INSERT INTO `product_comment` VALUES ('8', '3', '2', '2020-04-15 23:52:31', '整体不值100的价格，车身塑料感十足。只能开两个车门，也没有回力装置。就一个空壳。因为拿回来被小子看到就拆开了，懒得退。那变敞篷的设计，无力吐槽。以后还是考虑买合金的吧。网页图片都很漂亮，实物真的很失望。质量太差。');
INSERT INTO `product_comment` VALUES ('9', '3', '3', '2020-05-01 23:52:38', '挺好的，大小正好，18比例的偏大了，24比例刚刚好');
INSERT INTO `product_comment` VALUES ('10', '4', '1', '2020-05-02 23:52:41', '很不错，孩子特别喜欢，偶尔可以看到星星，最近天气不是很好，但今天还是看到了心心念念的月亮！很清楚！月坑也很清楚，等到满月的时候相信更壮观！好评！');
INSERT INTO `product_comment` VALUES ('11', '4', '2', '2020-05-20 23:52:45', '东西看起来挺高大上的，小孩一天到晚的拿的到处看，东西很不错');
INSERT INTO `product_comment` VALUES ('12', '4', '3', '2020-05-22 23:52:48', '非常好的一款天文望远镜，物美价廉，送给儿子的生日礼物，儿子超级喜欢，祝店家生意兴隆');
INSERT INTO `product_comment` VALUES ('13', '5', '3', '2020-04-30 23:52:53', '\r\n今天收到快递了，太喜欢了下一次还会来的，真的真的太好了。');
INSERT INTO `product_comment` VALUES ('14', '5', '2', '2020-04-27 23:52:57', '看月球表面细节是够用了，看星云也非常不错，木星还在寻找中。。。');
INSERT INTO `product_comment` VALUES ('15', '5', '1', '2020-04-28 23:53:00', '用了几天才来评价，望远镜很好，很清楚现在孩子也很喜欢我用，疫情期间快递也快好评');
INSERT INTO `product_comment` VALUES ('16', '6', '2', '2020-05-15 23:53:03', '望远镜到货，孩子很喜欢，爱上了它，这个套餐很划算，东西很多，都让孩子爱不择手，真不错');
INSERT INTO `product_comment` VALUES ('17', '6', '1', '2020-05-16 23:53:07', '外观看起来比较精细，晚上用它看夜空好美！好清晰！非常满意，五星好评！！！');
INSERT INTO `product_comment` VALUES ('18', '6', '3', '2020-05-21 23:53:11', '材质质量挺好，外观精美，做工精致，孩子很喜欢');
INSERT INTO `product_comment` VALUES ('19', '7', '3', '2020-04-27 23:53:15', '昨天收到，今天腊肉炒芦笋、香茹真的好吃。下次做扬洲炒饭肯定也好吃。');
INSERT INTO `product_comment` VALUES ('20', '7', '1', '2020-04-29 23:53:18', '确实是很好，真正的五花肉。打开袋子就一股烟熏肉香，切肉的时候都想生吃了。图片光线不太好，总之值得这个价！');
INSERT INTO `product_comment` VALUES ('21', '7', '2', '2020-04-30 23:53:22', '太好的腊肉 抱着试试看的态度买的 没想到肉的味道和品质都是一流的 以后吃腊肉就在你家买啦 希望多搞点活动');
INSERT INTO `product_comment` VALUES ('22', '8', '1', '2020-04-06 23:53:27', '这个腊肉除了好吃，找不到其他缺点，瘦而不材，肥而不腻，放在水里煮了10分钟，就这样蒸，口感稍微咸了一点点，再煮久一点，可能会更好，湖南的腊肉加广东的腊肠，好吃，好吃，个人觉得，这个腊肉，要是再干一点，那就完美了');
INSERT INTO `product_comment` VALUES ('23', '8', '2', '2020-05-13 23:53:35', '肉比较干，也比较咸，但是味道很好。建议开袋后拿水泡一晚拔掉盐分，再蒸上2小时就可以了。松松客服很热情，给他打call');
INSERT INTO `product_comment` VALUES ('24', '8', '3', '2020-05-15 23:53:38', '煮了一下刚切好，肥瘦相间，煮过之后就不咸了，可以根据自己口味来适当的调整煮的时间，拍了三斤，很满意，下次再回购哟');
INSERT INTO `product_comment` VALUES ('25', '9', '1', '2020-04-27 23:53:43', '包装品质：包装很好，严实！真空密闭。 商品分量：份量足 新鲜度：挺新鲜的 口感味道：味道较淡！感觉不如素肉味道好吃！');
INSERT INTO `product_comment` VALUES ('26', '9', '3', '2020-04-26 23:53:52', '豆腐干有20小袋，每种口味都有，试了下麻辣味的，豆腐软软的，有香菇在里面，不辣');
INSERT INTO `product_comment` VALUES ('27', '9', '2', '2020-05-05 23:53:56', '之前去重庆.在一个小卖店里看到.买了两袋回来尝尝.一吃真是好吃.赶紧在网上买了点.');
INSERT INTO `product_comment` VALUES ('28', '1', '1', '2020-05-06 13:53:44', '入了两辆z：brz涂装惊艳，细节满满；恶魔z做工精致，可玩性高。满昏！');
INSERT INTO `product_comment` VALUES ('29', '1', '2', '2020-05-15 13:53:49', '孩子看了多美卡玩具视频，非常喜欢，自己挑选了10个，质量很好，很有分量，轮子都带减震，大部分车门能打开，好评！');
INSERT INTO `product_comment` VALUES ('30', '1', '3', '2020-05-01 13:53:52', '适用年龄：儿子非常喜欢，发货也快，陆续购买中');
INSERT INTO `product_comment` VALUES ('31', '1', '3', '2020-05-02 13:53:57', '很喜欢，跟我以前买的一样！');
INSERT INTO `product_comment` VALUES ('32', '1', '2', '2020-05-03 13:54:00', '货品不错');
INSERT INTO `product_comment` VALUES ('33', '1', '1', '2020-04-27 13:54:05', '好精致的小车，前后灯保险杠都是单独配件，好评！！！');
INSERT INTO `product_comment` VALUES ('34', '1', '2', '2020-04-14 13:54:10', '都很喜欢，所以下定决心一下子下单买了43俩，一个个拆盒的感觉太棒了，比买手链还开心～现在准备去淘小柜子，专门放这些小车车～这款实物比图片好看多了。。不知为啥图片有种塑料感，这款的小车灯是可以调整角度的～');
INSERT INTO `product_comment` VALUES ('35', '1', '3', '2020-04-14 13:54:16', '好评好评！孩子喜欢思库的车模，现在觉得多美卡的更精细，合金车手感很好 有一定份量，被种草成功');
INSERT INTO `product_comment` VALUES ('36', '1', '1', '2020-05-22 13:54:20', '都很喜欢，所以下定决心一下子下单买了43俩，一个个拆盒的感觉太棒了，比买手链还开心～现在准备去淘小柜子，专门放这些小车车～好喜欢，冒粉红色泡泡～');
INSERT INTO `product_comment` VALUES ('37', '1', '2', '2020-05-01 13:54:24', '买了不少，基本没有拆开过，好像只是为了买而买。长此以往应该是支付不了其它爱好了');
INSERT INTO `product_comment` VALUES ('38', '1', '3', '2020-05-02 13:54:28', '物流很快，打包很结实，小车很喜欢，一直购买，性价比高');
INSERT INTO `product_comment` VALUES ('39', '1', '2', '2020-05-11 13:54:31', '是正品！孩子很喜欢，小贵的都是经典款，收藏比较值得！');
INSERT INTO `product_comment` VALUES ('40', '1', '3', '2020-05-16 13:54:37', '东西一如既往的好！！！！！');
INSERT INTO `product_comment` VALUES ('41', '1', '1', '2020-05-03 13:54:41', '东西很好，物流超快，一直购买，价格服务物流都很好');
INSERT INTO `product_comment` VALUES ('42', '20', '1', '2020-05-13 16:21:41', '第一次买腊肉，味道还行吧，用水煮过然后洗了以后跟菜一起炒还需要另外再加盐才行');
INSERT INTO `product_comment` VALUES ('43', '20', '2', '2020-05-04 16:22:12', '这个腊肉除了好吃，找不到其他缺点，瘦而不材，肥而不腻，放在水里煮了10分钟，就这样蒸，口感稍微咸了一点点，再煮久一点，可能会更好，湖南的腊肉加广东的腊肠，好吃，好吃，个人觉得，这个腊肉，要是再干一点，那就完美了');
INSERT INTO `product_comment` VALUES ('44', '20', '3', '2020-05-21 16:22:43', '腊肉特别的好吃，已经第四次拍了，每次都是拍3袋装的，卖家的服务态度非常好，我全家都喜欢，吃完继续');

-- ----------------------------
-- Table structure for product_order
-- ----------------------------
DROP TABLE IF EXISTS `product_order`;
CREATE TABLE `product_order` (
  `pro_order_id` int(11) NOT NULL AUTO_INCREMENT,
  `ifComment` int(255) DEFAULT NULL COMMENT '用于用户是否可见,1为可见,0为不可见(即用户删除后)',
  `total` double DEFAULT NULL COMMENT '总价',
  `status` int(11) NOT NULL COMMENT '状态',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `address` varchar(255) DEFAULT NULL COMMENT '收货地址',
  `time` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `tel` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`pro_order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of product_order
-- ----------------------------
INSERT INTO `product_order` VALUES ('1', '1', '282', '4', '1', '河北省石家庄市新华区石顺大道12-20', '2020-03-10 11:17:11', '周星星', '13972577438');
INSERT INTO `product_order` VALUES ('2', '1', '533.5', '4', '1', '重庆重庆市渝中区两路口xxxxx1', '2020-05-10 11:17:18', '张三', '17783026782');
INSERT INTO `product_order` VALUES ('3', '1', '1648', '2', '1', '北京北京市朝阳区xxxxx', '2020-05-16 14:18:01', '张大钱', '13972577438');
INSERT INTO `product_order` VALUES ('4', '1', '457', '1', '1', '北京北京市朝阳区xxxxx', '2020-05-22 14:26:41', '张三', '13972577438');
INSERT INTO `product_order` VALUES ('5', '1', '129', '1', '1', '北京北京市朝阳区xxxxx', '2020-05-31 19:55:41', '张三', '13972577438');

-- ----------------------------
-- Table structure for product_order_detail
-- ----------------------------
DROP TABLE IF EXISTS `product_order_detail`;
CREATE TABLE `product_order_detail` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `product_order_id` int(255) DEFAULT NULL,
  `pro_Id` int(255) DEFAULT NULL,
  `category_id` int(255) DEFAULT NULL,
  `proName` varchar(255) DEFAULT NULL,
  `price` double(255,2) DEFAULT NULL,
  `number` int(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `ifComment` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of product_order_detail
-- ----------------------------
INSERT INTO `product_order_detail` VALUES ('1', '1', '1', '1', '悍马警车1:32声光回力合金车模警车仿嗔汽车模', '67.50', '2', '/static/Liu/fangchemodel/fangche1.png', '0');
INSERT INTO `product_order_detail` VALUES ('2', '2', '2', '1', '彩珀合金房车儿童玩具旅行巴士合金小汽车仿真开门]声光力小汽车模型合金房车', '56.50', '7', '/static/Liu/fangchemodel/fangche4.png', '0');
INSERT INTO `product_order_detail` VALUES ('3', '2', '21', '3', '陈麻花', '69.00', '2', '/static/Liu/chenmahua/cmh1.jpg', '0');
INSERT INTO `product_order_detail` VALUES ('4', '3', '8', '3', '回锅肉特产', '199.00', '5', '/static/Liu/tutechan/ttc4.png', '0');
INSERT INTO `product_order_detail` VALUES ('5', '3', '4', '2', '望远镜1', '159.00', '3', '/static//Liu/wangyuanjing/wyj1.png', '0');
INSERT INTO `product_order_detail` VALUES ('6', '3', '22', '3', '重庆香菇豆干', '88.00', '2', '/static/Liu/xianggudougan/xgdg1.jpg', '0');
INSERT INTO `product_order_detail` VALUES ('7', '4', '18', '1', '悍马警车10', '59.00', '1', '/static/Liu/fangchemodel/fangche2.png', '0');
INSERT INTO `product_order_detail` VALUES ('8', '4', '20', '3', '城口腊肉', '199.00', '2', '/static/Liu/chengkoularou/cklr1.jpg', '0');
INSERT INTO `product_order_detail` VALUES ('22', '1', '6', '2', '望远镜3', '147.00', '1', '/static//Liu/wangyuanjing/wyj5.png', '0');
INSERT INTO `product_order_detail` VALUES ('23', '5', '9', '3', '豆干羊角', '129.00', '1', '/static/Liu/tutechan/ttc5.png', '0');

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of role
-- ----------------------------

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `username` varchar(255) DEFAULT NULL COMMENT '用户名',
  `password` varchar(255) DEFAULT NULL COMMENT '用户密码',
  `tel` varchar(11) DEFAULT NULL COMMENT '用户电话',
  `img_url` varchar(255) DEFAULT NULL COMMENT '图片地址',
  `status` int(11) DEFAULT NULL COMMENT '状态',
  `account` double(255,2) DEFAULT NULL COMMENT '余额',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', '张三', 'zhangsan', '17783700194', '/static/Liu/userimg/user.jpg', '1', '6594.00');
INSERT INTO `user` VALUES ('2', '李四', 'lisilisi', '12243536353', 'http://www.joyrv.cn/data/upload/5ecf42db77277_s.jpg', '1', '0.00');
INSERT INTO `user` VALUES ('3', '王五', 'wangwu', '142566275', 'http://www.joyrv.cn/data/upload/5ecf42db77277_s.jpg', '1', '0.00');
INSERT INTO `user` VALUES ('4', 'songfubing', 'songfubing', '13500345147', 'http://www.joyrv.cn/data/upload/5ecf42db77277_s.jpg', '0', '10.00');

-- ----------------------------
-- Table structure for user_address
-- ----------------------------
DROP TABLE IF EXISTS `user_address`;
CREATE TABLE `user_address` (
  `address_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '收货地址id',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `address` varchar(255) DEFAULT NULL COMMENT '详细地址信息',
  `name` varchar(255) DEFAULT NULL COMMENT '收货人姓名',
  `tel` varchar(11) DEFAULT NULL COMMENT '收货人电话',
  `status` int(255) DEFAULT NULL COMMENT '是否为默认的收货地址',
  PRIMARY KEY (`address_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_address
-- ----------------------------
INSERT INTO `user_address` VALUES ('4', '2', '重庆市渝北区', '张飞', '12989037896', '0');
INSERT INTO `user_address` VALUES ('9', '1', '重庆重庆市渝中区两路口xxxxx1', '张三', '17783026782', '0');
INSERT INTO `user_address` VALUES ('15', '1', '河北省石家庄市新华区石顺大道12-20', '周星星', '13972577438', '1');
INSERT INTO `user_address` VALUES ('16', '1', '北京北京市朝阳区xxxxx', '冰烧', '17783824738', '1');
INSERT INTO `user_address` VALUES ('17', '1', '北京北京市西城区4342', '3242', '43242342424', '1');
INSERT INTO `user_address` VALUES ('18', '1', '北京北京市东城区XXXX行', '年纪', '18796464646', '1');

-- ----------------------------
-- Table structure for user_info
-- ----------------------------
DROP TABLE IF EXISTS `user_info`;
CREATE TABLE `user_info` (
  `info_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户详细信息id',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `age` int(11) DEFAULT NULL COMMENT '年龄',
  `sex` int(11) DEFAULT NULL COMMENT '性别',
  `birthday` date DEFAULT NULL COMMENT '生日',
  `sign` varchar(255) DEFAULT NULL COMMENT '个性签名',
  PRIMARY KEY (`info_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_info
-- ----------------------------
INSERT INTO `user_info` VALUES ('1', '1', '18', '0', '2020-06-01', '我是肥猪流');
