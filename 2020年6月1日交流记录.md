考虑项目真实性：
- 业务问题
    - 支付时余额不足怎办？
    - 支付宝返回支付失败了如何处理？
    - 退货退款？退款时有优惠券？
    - 订单取消怎办？订单未取消怎办？订单收货超时？
    - 配送失败，联系不到人？
    - 配送API，配送费支付？
    - 瑜伽APP的教练解约，考虑是否有进行中的课程？解约=>退款; 不解约；
    - ...
- 技术问题
    - 并发，如何加锁？
    - 何处用事务？事务加了是否起效果了？
    - 跨域如何解决？
    - 重复提交如何解决？
    - 用户长期不登录如何解决？
    - 外部交互：支付宝回调机制和页面跳转冲突了怎么办？链接超时如何办？
    - redis / rabbitmq / elasticsearch（数据同步logstash） 项目中如何用？会带来哪些问题？如何解决？