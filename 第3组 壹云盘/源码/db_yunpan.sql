/*
Navicat MySQL Data Transfer

Source Server         : mysqlCound
Source Server Version : 80016
Source Host           : beiju951220.mysql.rds.aliyuncs.com:3306
Source Database       : db_yunpan

Target Server Type    : MYSQL
Target Server Version : 80016
File Encoding         : 65001

Date: 2020-06-14 13:01:11
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for add_friend
-- ----------------------------
DROP TABLE IF EXISTS `add_friend`;
CREATE TABLE `add_friend` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `launch_user_id` int(11) DEFAULT NULL COMMENT '发起好友id',
  `receive_user_id` int(11) DEFAULT NULL COMMENT '接收好友id',
  `describe` varchar(255) DEFAULT NULL COMMENT '描述',
  `spare1` varchar(255) DEFAULT NULL,
  `spare2` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of add_friend
-- ----------------------------
INSERT INTO `add_friend` VALUES ('24', '27', '3', 'halou', '1', '2020-06-11 00:00:00');
INSERT INTO `add_friend` VALUES ('25', '3', '26', 'ccc', '1', '2020-06-11 00:00:00');
INSERT INTO `add_friend` VALUES ('26', '2', '27', '哈喽', '1', '2020-06-11 00:00:00');
INSERT INTO `add_friend` VALUES ('27', '28', '27', '你好啊', '1', '2020-06-11 00:00:00');
INSERT INTO `add_friend` VALUES ('28', '28', '27', '我又加你了', '1', '2020-06-11 00:00:00');

-- ----------------------------
-- Table structure for administrator
-- ----------------------------
DROP TABLE IF EXISTS `administrator`;
CREATE TABLE `administrator` (
  `id` int(11) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of administrator
-- ----------------------------
INSERT INTO `administrator` VALUES ('1', 'admin', 'admin');

-- ----------------------------
-- Table structure for chat_record
-- ----------------------------
DROP TABLE IF EXISTS `chat_record`;
CREATE TABLE `chat_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_session_id` int(11) DEFAULT NULL COMMENT '用户会话id',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '文本内容',
  `type` int(11) DEFAULT NULL COMMENT '类型 0为文本，1为图片，2为音乐，3位视频，4位文件夹，5位文档，6位其他',
  `send_time` datetime DEFAULT NULL COMMENT '发送时间',
  `group_id` int(11) DEFAULT NULL COMMENT '群号id',
  `status` int(11) DEFAULT NULL COMMENT '状态 0未读 1已读',
  `dir_or_file_id` int(255) DEFAULT NULL COMMENT '文件或文件夹的id',
  `spare2` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备用2',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=521 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of chat_record
-- ----------------------------
INSERT INTO `chat_record` VALUES ('448', '27', '3', '我已经添加你为好友，可以开始聊天！', '0', '2020-06-11 13:17:35', '0', '1', null, null);
INSERT INTO `chat_record` VALUES ('449', '27', '27', null, '7', '2020-06-11 13:18:05', '0', '1', '109', null);
INSERT INTO `chat_record` VALUES ('450', '27', '27', '/img/corpse/14.gif', '8', '2020-06-11 13:18:53', '0', '1', '0', null);
INSERT INTO `chat_record` VALUES ('451', '27', '27', '你好啊', '0', '2020-06-11 13:19:05', '0', '1', '0', null);
INSERT INTO `chat_record` VALUES ('452', '27', '27', '/img/cat/2.gif', '8', '2020-06-11 13:19:11', '0', '1', '0', null);
INSERT INTO `chat_record` VALUES ('453', '27', '27', '/img/cat/4.gif', '8', '2020-06-11 13:19:13', '0', '1', '0', null);
INSERT INTO `chat_record` VALUES ('454', '27', '27', '/img/cat/11.gif', '8', '2020-06-11 13:19:15', '0', '1', '0', null);
INSERT INTO `chat_record` VALUES ('455', '27', '27', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/headImg3.jpeg', '1', '2020-06-11 13:19:22', '0', '1', '358', null);
INSERT INTO `chat_record` VALUES ('456', '27', '3', '/img/corpse/12.gif', '8', '2020-06-11 13:19:24', '0', '1', '0', null);
INSERT INTO `chat_record` VALUES ('457', '27', '27', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '2', '2020-06-11 13:19:25', '0', '1', '361', null);
INSERT INTO `chat_record` VALUES ('458', '27', '3', '/img/corpse/13.gif', '8', '2020-06-11 13:19:27', '0', '1', '0', null);
INSERT INTO `chat_record` VALUES ('459', '27', '27', '/img/cat/15.gif', '8', '2020-06-11 13:19:30', '0', '1', '0', null);
INSERT INTO `chat_record` VALUES ('460', '28', '26', '我已经添加你为好友，可以开始聊天！', '0', '2020-06-11 13:25:37', '0', '1', null, null);
INSERT INTO `chat_record` VALUES ('461', '27', '27', '/img/corpse/13.gif', '8', '2020-06-11 13:39:48', '0', '1', '0', null);
INSERT INTO `chat_record` VALUES ('462', '29', '27', '我已经添加你为好友，可以开始聊天！', '0', '2020-06-11 13:41:22', '0', '1', null, null);
INSERT INTO `chat_record` VALUES ('463', '29', '27', '/img/corpse/6.gif', '8', '2020-06-11 13:41:27', '0', '1', '0', null);
INSERT INTO `chat_record` VALUES ('464', '29', '27', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/headImg3.jpeg', '1', '2020-06-11 13:41:31', '0', '1', '358', null);
INSERT INTO `chat_record` VALUES ('465', '30', '27', '欢迎大家加入我的群聊', '0', '2020-06-11 13:41:42', '16', '1', null, null);
INSERT INTO `chat_record` VALUES ('466', '30', '27', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '3', '2020-06-11 13:41:56', '16', '1', '365', null);
INSERT INTO `chat_record` VALUES ('467', '30', '2', '/img/cat/4.gif', '8', '2020-06-11 13:42:18', '16', '1', '0', null);
INSERT INTO `chat_record` VALUES ('468', '30', '27', '/img/corpse/11.gif', '8', '2020-06-11 13:42:27', '16', '1', '0', null);
INSERT INTO `chat_record` VALUES ('469', '30', '2', '/img/corpse/1.gif', '8', '2020-06-11 13:42:35', '16', '1', '0', null);
INSERT INTO `chat_record` VALUES ('470', '29', '2', '/img/corpse/13.gif', '8', '2020-06-11 13:42:58', '0', '1', '0', null);
INSERT INTO `chat_record` VALUES ('471', '27', '27', '/img/corpse/13.gif', '8', '2020-06-11 13:43:19', '0', '1', '0', null);
INSERT INTO `chat_record` VALUES ('472', '27', '27', '/img/corpse/2.gif', '8', '2020-06-11 13:43:21', '0', '1', '0', null);
INSERT INTO `chat_record` VALUES ('473', '27', '27', '/img/corpse/14.gif', '8', '2020-06-11 13:43:22', '0', '1', '0', null);
INSERT INTO `chat_record` VALUES ('474', '27', '27', '/img/cat/3.gif', '8', '2020-06-11 13:43:25', '0', '1', '0', null);
INSERT INTO `chat_record` VALUES ('475', '27', '27', 'nidf ', '0', '2020-06-11 13:43:28', '0', '1', '0', null);
INSERT INTO `chat_record` VALUES ('476', '27', '27', '傻逼', '0', '2020-06-11 13:43:31', '0', '1', '0', null);
INSERT INTO `chat_record` VALUES ('477', '27', '27', '约吗、', '0', '2020-06-11 13:43:34', '0', '1', '0', null);
INSERT INTO `chat_record` VALUES ('478', '27', '27', '嗯嗯嗯', '0', '2020-06-11 13:43:35', '0', '1', '0', null);
INSERT INTO `chat_record` VALUES ('479', '27', '27', '哈哈哈', '0', '2020-06-11 13:43:36', '0', '1', '0', null);
INSERT INTO `chat_record` VALUES ('480', '27', '27', '哦哦', '0', '2020-06-11 13:43:37', '0', '1', '0', null);
INSERT INTO `chat_record` VALUES ('481', '27', '27', '的卡拉季l', '0', '2020-06-11 13:43:39', '0', '1', '0', null);
INSERT INTO `chat_record` VALUES ('482', '27', '27', '你', '0', '2020-06-11 13:44:04', '0', '1', '0', null);
INSERT INTO `chat_record` VALUES ('483', '27', '27', '/img/corpse/12.gif', '8', '2020-06-11 13:44:08', '0', '1', '0', null);
INSERT INTO `chat_record` VALUES ('484', '30', '2', '姐姐约吗', '0', '2020-06-11 13:47:15', '16', '1', '0', null);
INSERT INTO `chat_record` VALUES ('485', '27', '27', '滚', '0', '2020-06-11 13:47:18', '0', '1', '0', null);
INSERT INTO `chat_record` VALUES ('486', '30', '2', '/img/corpse/11.gif', '8', '2020-06-11 13:47:23', '16', '1', '0', null);
INSERT INTO `chat_record` VALUES ('487', '29', '2', '/img/corpse/14.gif', '8', '2020-06-11 13:48:14', '0', '1', '0', null);
INSERT INTO `chat_record` VALUES ('488', '29', '2', '女神', '0', '2020-06-11 13:49:03', '0', '1', '0', null);
INSERT INTO `chat_record` VALUES ('489', '29', '27', '滚', '0', '2020-06-11 13:49:07', '0', '1', '0', null);
INSERT INTO `chat_record` VALUES ('490', '29', '2', '/img/corpse/13.gif', '8', '2020-06-11 13:49:14', '0', '1', '0', null);
INSERT INTO `chat_record` VALUES ('491', '29', '2', null, '7', '2020-06-11 13:49:22', '0', '1', '110', null);
INSERT INTO `chat_record` VALUES ('492', '27', '3', '/img/corpse/20.gif', '8', '2020-06-11 14:16:26', '0', '1', '0', null);
INSERT INTO `chat_record` VALUES ('493', '27', '27', '/img/corpse/21.gif', '8', '2020-06-11 14:16:36', '0', '1', '0', null);
INSERT INTO `chat_record` VALUES ('494', '31', '27', '我已经添加你为好友，可以开始聊天！', '0', '2020-06-11 16:02:08', '0', '1', null, null);
INSERT INTO `chat_record` VALUES ('495', '31', '28', '/img/corpse/5.gif', '8', '2020-06-11 16:02:17', '0', '1', '0', null);
INSERT INTO `chat_record` VALUES ('496', '31', '28', '/img/corpse/7.gif', '8', '2020-06-11 16:02:26', '0', '1', '0', null);
INSERT INTO `chat_record` VALUES ('497', '31', '27', '/img/corpse/14.gif', '8', '2020-06-11 16:02:38', '0', '1', '0', null);
INSERT INTO `chat_record` VALUES ('498', '31', '27', 'nid ', '0', '2020-06-11 16:02:41', '0', '1', '0', null);
INSERT INTO `chat_record` VALUES ('499', '31', '27', '/img/corpse/13.gif', '8', '2020-06-11 16:03:02', '0', '1', '0', null);
INSERT INTO `chat_record` VALUES ('500', '31', '27', 'niho', '0', '2020-06-11 16:04:35', '0', '1', '0', null);
INSERT INTO `chat_record` VALUES ('501', '31', '27', '你少', '0', '2020-06-11 16:04:45', '0', '1', '0', null);
INSERT INTO `chat_record` VALUES ('502', '31', '27', null, '7', '2020-06-11 16:06:02', '0', '1', '112', null);
INSERT INTO `chat_record` VALUES ('503', '31', '28', '/img/corpse/10.gif', '8', '2020-06-11 16:07:06', '0', '1', '0', null);
INSERT INTO `chat_record` VALUES ('504', '31', '28', '/img/corpse/14.gif', '8', '2020-06-11 16:07:07', '0', '1', '0', null);
INSERT INTO `chat_record` VALUES ('505', '31', '28', '/img/cat/2.gif', '8', '2020-06-11 16:07:10', '0', '1', '0', null);
INSERT INTO `chat_record` VALUES ('506', '31', '28', '/img/cat/9.gif', '8', '2020-06-11 16:07:12', '0', '1', '0', null);
INSERT INTO `chat_record` VALUES ('507', '31', '28', '你', '0', '2020-06-11 16:07:56', '0', '1', '0', null);
INSERT INTO `chat_record` VALUES ('508', '31', '28', '奋斗撒', '0', '2020-06-11 16:07:57', '0', '1', '0', null);
INSERT INTO `chat_record` VALUES ('509', '31', '28', '发动机三奋斗撒', '0', '2020-06-11 16:07:58', '0', '1', '0', null);
INSERT INTO `chat_record` VALUES ('510', '31', '28', '奋斗撒fd fsa', '0', '2020-06-11 16:07:59', '0', '1', '0', null);
INSERT INTO `chat_record` VALUES ('511', '31', '28', '奋斗撒', '0', '2020-06-11 16:08:00', '0', '1', '0', null);
INSERT INTO `chat_record` VALUES ('512', '31', '28', '放大f地方asfd发', '0', '2020-06-11 16:08:01', '0', '1', '0', null);
INSERT INTO `chat_record` VALUES ('513', '32', '27', '欢迎大家加入我的群聊', '0', '2020-06-11 16:08:47', '17', '1', null, null);
INSERT INTO `chat_record` VALUES ('514', '32', '28', '/img/corpse/20.gif', '8', '2020-06-11 16:09:15', '17', '1', '0', null);
INSERT INTO `chat_record` VALUES ('515', '33', '27', '欢迎大家加入我的群聊', '0', '2020-06-11 16:09:46', '18', '1', null, null);
INSERT INTO `chat_record` VALUES ('516', '34', '27', '欢迎大家加入我的群聊', '0', '2020-06-11 16:10:04', '19', '1', null, null);
INSERT INTO `chat_record` VALUES ('517', '31', '27', '/img/corpse/6.gif', '8', '2020-06-11 16:37:58', '0', '0', '0', null);
INSERT INTO `chat_record` VALUES ('518', '31', '27', '/img/cat/9.gif', '8', '2020-06-11 16:47:05', '0', '0', '0', null);
INSERT INTO `chat_record` VALUES ('519', '29', '27', '/img/corpse/15.gif', '8', '2020-06-11 16:47:52', '0', '0', '0', null);
INSERT INTO `chat_record` VALUES ('520', '29', '27', '/img/corpse/11.gif', '8', '2020-06-11 16:47:57', '0', '0', '0', null);

-- ----------------------------
-- Table structure for feedback
-- ----------------------------
DROP TABLE IF EXISTS `feedback`;
CREATE TABLE `feedback` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` int(11) DEFAULT NULL COMMENT '用户Id',
  `content` varchar(255) DEFAULT NULL COMMENT '反馈内容',
  `tel` char(11) DEFAULT NULL COMMENT '电话',
  `status` int(255) DEFAULT NULL COMMENT '状态',
  `spare1` varchar(255) DEFAULT NULL COMMENT '备用1',
  `spare2` varchar(255) DEFAULT NULL COMMENT '备用2',
  `replay` varchar(255) DEFAULT NULL COMMENT '回复内容',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `replay_time` datetime DEFAULT NULL COMMENT '回复时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of feedback
-- ----------------------------

-- ----------------------------
-- Table structure for friend
-- ----------------------------
DROP TABLE IF EXISTS `friend`;
CREATE TABLE `friend` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user1_id` int(11) DEFAULT NULL COMMENT '用户1id',
  `user2_id` int(11) DEFAULT NULL COMMENT '用户2id',
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户1对用户2的备注',
  `status` int(11) DEFAULT NULL COMMENT '0为黑名单，1位好友',
  `spare1` varchar(255) DEFAULT NULL,
  `spare2` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of friend
-- ----------------------------
INSERT INTO `friend` VALUES ('31', '3', '27', null, '1', null, null);
INSERT INTO `friend` VALUES ('32', '27', '3', null, '1', null, null);
INSERT INTO `friend` VALUES ('33', '26', '3', null, '1', null, null);
INSERT INTO `friend` VALUES ('34', '3', '26', null, '1', null, null);
INSERT INTO `friend` VALUES ('35', '27', '2', null, '1', null, null);
INSERT INTO `friend` VALUES ('36', '2', '27', null, '1', null, null);
INSERT INTO `friend` VALUES ('37', '27', '28', '小', '1', null, null);
INSERT INTO `friend` VALUES ('38', '28', '27', '大', '1', null, null);

-- ----------------------------
-- Table structure for group
-- ----------------------------
DROP TABLE IF EXISTS `group`;
CREATE TABLE `group` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(255) DEFAULT NULL COMMENT '群名',
  `master` varchar(255) DEFAULT NULL COMMENT '群主',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `status` int(255) DEFAULT NULL COMMENT '0，为解散，1位正常',
  `spare1` varchar(255) DEFAULT NULL COMMENT '备用1',
  `spare2` varchar(255) DEFAULT NULL COMMENT '备用2',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of group
-- ----------------------------
INSERT INTO `group` VALUES ('16', 'sm***iu的群', '27', '2020-06-11 13:41:42', '0', null, null);
INSERT INTO `group` VALUES ('17', 'sm***iu的群', '27', '2020-06-11 16:08:47', '0', null, null);
INSERT INTO `group` VALUES ('18', 'sm***iu的群', '27', '2020-06-11 16:09:46', '0', null, null);
INSERT INTO `group` VALUES ('19', 'sm***iu的群', '27', '2020-06-11 16:10:04', '1', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/e9045f6d-f004-4e64-9f91-38134e47b72d.jpg', null);

-- ----------------------------
-- Table structure for group_user
-- ----------------------------
DROP TABLE IF EXISTS `group_user`;
CREATE TABLE `group_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `group_id` int(11) DEFAULT NULL COMMENT '群id',
  `status` int(255) DEFAULT NULL COMMENT '0位退群，1为正常',
  `spare1` varchar(255) DEFAULT NULL COMMENT '备用1',
  `spare2` varchar(255) DEFAULT NULL COMMENT '备用2',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of group_user
-- ----------------------------
INSERT INTO `group_user` VALUES ('37', '27', '16', '1', '27', null);
INSERT INTO `group_user` VALUES ('38', '3', '16', '1', '27', null);
INSERT INTO `group_user` VALUES ('39', '2', '16', '1', '27', null);
INSERT INTO `group_user` VALUES ('40', '27', '17', '1', '27', null);
INSERT INTO `group_user` VALUES ('41', '3', '17', '1', '27', null);
INSERT INTO `group_user` VALUES ('42', '2', '17', '1', '27', null);
INSERT INTO `group_user` VALUES ('43', '28', '17', '1', '27', null);
INSERT INTO `group_user` VALUES ('44', '27', '18', '1', '27', null);
INSERT INTO `group_user` VALUES ('45', '3', '18', '1', '27', null);
INSERT INTO `group_user` VALUES ('46', '2', '18', '1', '27', null);
INSERT INTO `group_user` VALUES ('47', '28', '18', '1', '27', null);
INSERT INTO `group_user` VALUES ('48', '27', '19', '1', '27', null);
INSERT INTO `group_user` VALUES ('49', '3', '19', '1', '27', null);
INSERT INTO `group_user` VALUES ('50', '2', '19', '1', '27', null);
INSERT INTO `group_user` VALUES ('51', '28', '19', '1', '27', null);

-- ----------------------------
-- Table structure for link_share
-- ----------------------------
DROP TABLE IF EXISTS `link_share`;
CREATE TABLE `link_share` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `overdue_time` datetime DEFAULT NULL COMMENT '链接过期时间',
  `code` char(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '提取码',
  `status` int(11) DEFAULT NULL COMMENT '0为公开 1为私密,2为好友分享,3位已失效',
  `spare1` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '分享名',
  `spare2` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=113 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of link_share
-- ----------------------------
INSERT INTO `link_share` VALUES ('109', '27', null, null, '2', 'Kalimba.mp3等多个文件', null);
INSERT INTO `link_share` VALUES ('110', '2', null, null, '2', 'i4Tools_v7.98.15_Setup.exe等多个文件', null);
INSERT INTO `link_share` VALUES ('112', '27', null, null, '2', 'video.mp4等多个文件', null);

-- ----------------------------
-- Table structure for link_share_file
-- ----------------------------
DROP TABLE IF EXISTS `link_share_file`;
CREATE TABLE `link_share_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `share_file_id` int(11) DEFAULT NULL COMMENT '分享文件及文件夹id',
  `link_share_id` int(11) DEFAULT NULL COMMENT '链接分享id',
  `type` int(11) DEFAULT NULL COMMENT '0为文件夹 1为文件',
  `status` int(11) DEFAULT NULL,
  `spare1` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备用1',
  `spare2` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备用2',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=207 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of link_share_file
-- ----------------------------
INSERT INTO `link_share_file` VALUES ('197', '358', '109', '1', null, null, null);
INSERT INTO `link_share_file` VALUES ('198', '361', '109', '1', null, null, null);
INSERT INTO `link_share_file` VALUES ('199', '357', '110', '1', null, null, null);
INSERT INTO `link_share_file` VALUES ('200', '359', '110', '1', null, null, null);
INSERT INTO `link_share_file` VALUES ('201', '585', '111', '0', null, null, null);
INSERT INTO `link_share_file` VALUES ('202', '389', '111', '1', null, null, null);
INSERT INTO `link_share_file` VALUES ('203', '391', '111', '1', null, null, null);
INSERT INTO `link_share_file` VALUES ('204', '358', '112', '1', null, null, null);
INSERT INTO `link_share_file` VALUES ('205', '361', '112', '1', null, null, null);
INSERT INTO `link_share_file` VALUES ('206', '365', '112', '1', null, null, null);

-- ----------------------------
-- Table structure for login_record
-- ----------------------------
DROP TABLE IF EXISTS `login_record`;
CREATE TABLE `login_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `ip_address` varchar(255) DEFAULT NULL COMMENT 'ip地址',
  `login_time` datetime DEFAULT NULL COMMENT '登录时间',
  `equipment` varchar(255) DEFAULT NULL COMMENT '登录设备',
  `status` int(255) DEFAULT NULL COMMENT '状态（0为成功，1为失败）',
  `os_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '设备系统名称',
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '登录地址',
  `latitude` varchar(255) DEFAULT NULL COMMENT '纬度',
  `longitude` varchar(255) DEFAULT NULL COMMENT '经度',
  `spare` varchar(255) DEFAULT NULL COMMENT '备用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=986 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of login_record
-- ----------------------------
INSERT INTO `login_record` VALUES ('858', '3', '192.168.120.79', '2020-06-10 19:54:21', 'COMPUTER', '0', 'Windows 10', '未知-未知', '2.4', '2.2', '备用');
INSERT INTO `login_record` VALUES ('859', '2', '183.67.21.153', '2020-06-10 19:54:42', 'COMPUTER', '0', 'Windows 10', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('860', '2', '183.67.21.153', '2020-06-10 20:05:04', 'COMPUTER', '0', 'Windows 10', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('861', '3', '192.168.120.79', '2020-06-10 20:05:08', 'COMPUTER', '0', 'Windows 10', '未知-未知', '2.4', '2.2', '备用');
INSERT INTO `login_record` VALUES ('862', '3', '192.168.120.79', '2020-06-10 20:16:04', 'COMPUTER', '0', 'Windows 10', '未知-未知', '2.4', '2.2', '备用');
INSERT INTO `login_record` VALUES ('863', '2', '183.67.21.153', '2020-06-10 20:16:18', 'COMPUTER', '0', 'Windows 10', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('864', '3', '192.168.120.79', '2020-06-10 20:26:12', 'COMPUTER', '0', 'Windows 10', '未知-未知', '2.4', '2.2', '备用');
INSERT INTO `login_record` VALUES ('865', '3', '192.168.120.16', '2020-06-10 20:41:18', 'COMPUTER', '0', 'Windows 10', '未知-未知', '2.4', '2.2', '备用');
INSERT INTO `login_record` VALUES ('866', '2', '192.168.120.16', '2020-06-10 20:41:20', 'COMPUTER', '0', 'Windows 7', '未知-未知', '2.4', '2.2', '备用');
INSERT INTO `login_record` VALUES ('867', '3', '183.67.21.153', '2020-06-10 20:59:13', 'COMPUTER', '0', 'Windows 10', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('868', '2', '192.168.120.79', '2020-06-10 21:00:05', 'COMPUTER', '0', 'Windows 10', '未知-未知', '2.4', '2.2', '备用');
INSERT INTO `login_record` VALUES ('869', '1', '183.67.21.153', '2020-06-10 21:25:06', 'COMPUTER', '0', 'Windows 10', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('870', '1', '183.67.19.219', '2020-06-10 21:35:42', 'COMPUTER', '0', 'Windows 10', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('871', '1', '183.67.19.219', '2020-06-10 21:46:37', 'COMPUTER', '0', 'Windows 10', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('872', '1', '183.67.19.219', '2020-06-10 21:59:20', 'COMPUTER', '0', 'Windows 10', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('873', '2', '183.67.21.153', '2020-06-10 22:07:14', 'COMPUTER', '0', 'Windows 10', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('874', '2', '183.67.21.153', '2020-06-10 22:09:23', 'COMPUTER', '0', 'Windows 10', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('875', '26', '183.67.21.153', '2020-06-10 22:23:33', 'COMPUTER', '0', 'Windows 7', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('876', '26', '183.67.21.153', '2020-06-10 22:29:48', 'COMPUTER', '0', 'Windows 7', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('877', '1', '183.67.21.153', '2020-06-10 22:39:43', 'COMPUTER', '0', 'Windows 10', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('878', '1', '183.67.21.153', '2020-06-10 22:42:01', 'COMPUTER', '0', 'Windows 10', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('879', '1', '183.67.21.153', '2020-06-10 22:43:35', 'COMPUTER', '0', 'Windows 10', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('880', '1', '183.67.21.153', '2020-06-10 22:44:41', 'COMPUTER', '0', 'Windows 10', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('881', '2', '183.67.21.153', '2020-06-10 22:45:41', 'COMPUTER', '0', 'Windows 10', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('882', '2', '183.67.21.153', '2020-06-10 22:46:28', 'COMPUTER', '0', 'Windows 10', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('883', '2', '183.67.21.153', '2020-06-10 22:50:32', 'COMPUTER', '0', 'Windows 10', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('884', '26', '183.67.21.153', '2020-06-11 00:38:41', 'COMPUTER', '0', 'Windows 10', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('885', '1', '183.67.21.153', '2020-06-11 00:51:53', 'COMPUTER', '0', 'Windows 10', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('886', '1', '183.67.21.153', '2020-06-11 00:53:21', 'COMPUTER', '0', 'Windows 10', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('887', '1', '183.67.21.153', '2020-06-11 01:13:44', 'COMPUTER', '0', 'Windows 7', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('888', '2', '183.67.21.153', '2020-06-11 09:09:30', 'COMPUTER', '0', 'Windows 10', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('889', '1', '183.67.21.153', '2020-06-11 09:16:59', 'COMPUTER', '0', 'Windows 7', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('890', '27', '192.168.120.16', '2020-06-11 09:27:16', 'COMPUTER', '0', 'Windows 7', '未知-未知', '2.4', '2.2', '备用');
INSERT INTO `login_record` VALUES ('891', '2', '183.67.21.153', '2020-06-11 09:45:01', 'COMPUTER', '0', 'Windows 10', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('892', '1', '183.67.21.153', '2020-06-11 10:10:07', 'COMPUTER', '0', 'Windows 10', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('893', '27', '192.168.120.16', '2020-06-11 10:19:19', 'COMPUTER', '0', 'Windows 7', '未知-未知', '2.4', '2.2', '备用');
INSERT INTO `login_record` VALUES ('894', '26', '192.168.120.16', '2020-06-11 10:21:01', 'COMPUTER', '0', 'Windows 7', '未知-未知', '2.4', '2.2', '备用');
INSERT INTO `login_record` VALUES ('895', '26', '183.67.21.153', '2020-06-11 10:38:00', 'COMPUTER', '0', 'Windows 10', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('896', '2', '183.67.21.153', '2020-06-11 10:39:32', 'COMPUTER', '0', 'Windows 10', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('897', '27', '192.168.120.16', '2020-06-11 10:42:58', 'COMPUTER', '0', 'Windows 7', '未知-未知', '2.4', '2.2', '备用');
INSERT INTO `login_record` VALUES ('898', '2', '183.67.21.153', '2020-06-11 10:57:11', 'COMPUTER', '0', 'Windows 10', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('899', '1', '183.67.21.153', '2020-06-11 10:58:29', 'COMPUTER', '0', 'Windows 10', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('900', '1', '183.67.21.153', '2020-06-11 10:58:34', 'COMPUTER', '0', 'Windows 10', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('901', '2', '183.67.21.153', '2020-06-11 11:05:21', 'COMPUTER', '0', 'Windows 10', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('902', '1', '192.168.120.16', '2020-06-11 12:57:32', 'COMPUTER', '0', 'Windows 7', '未知-未知', '2.4', '2.2', '备用');
INSERT INTO `login_record` VALUES ('903', '2', '192.168.120.16', '2020-06-11 12:59:38', 'COMPUTER', '0', 'Windows 7', '未知-未知', '2.4', '2.2', '备用');
INSERT INTO `login_record` VALUES ('904', '2', '183.67.21.153', '2020-06-11 13:05:44', 'COMPUTER', '0', 'Windows 10', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('905', '27', '192.168.120.16', '2020-06-11 13:08:36', 'COMPUTER', '0', 'Windows 7', '未知-未知', '2.4', '2.2', '备用');
INSERT INTO `login_record` VALUES ('906', '2', '183.67.21.153', '2020-06-11 13:10:33', 'COMPUTER', '0', 'Windows 10', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('907', '2', '183.67.21.153', '2020-06-11 13:10:35', 'COMPUTER', '0', 'Windows 10', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('908', '26', '183.67.21.153', '2020-06-11 13:15:40', 'COMPUTER', '0', 'Windows 7', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('909', '27', '192.168.120.16', '2020-06-11 13:16:54', 'COMPUTER', '0', 'Windows 7', '未知-未知', '2.4', '2.2', '备用');
INSERT INTO `login_record` VALUES ('910', '3', '192.168.120.16', '2020-06-11 13:17:06', 'COMPUTER', '0', 'Windows 10', '未知-未知', '2.4', '2.2', '备用');
INSERT INTO `login_record` VALUES ('911', '27', '192.168.120.16', '2020-06-11 13:22:52', 'COMPUTER', '0', 'Windows 7', '未知-未知', '2.4', '2.2', '备用');
INSERT INTO `login_record` VALUES ('912', '3', '192.168.120.16', '2020-06-11 13:23:56', 'COMPUTER', '0', 'Windows 10', '未知-未知', '2.4', '2.2', '备用');
INSERT INTO `login_record` VALUES ('913', '26', '192.168.120.16', '2020-06-11 13:28:17', 'COMPUTER', '0', 'Windows 7', '未知-未知', '2.4', '2.2', '备用');
INSERT INTO `login_record` VALUES ('914', '2', '183.67.21.153', '2020-06-11 13:28:24', 'COMPUTER', '0', 'Windows 10', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('915', '3', '183.67.21.153', '2020-06-11 13:28:39', 'COMPUTER', '0', 'Windows 10', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('916', '3', '192.168.120.79', '2020-06-11 13:35:13', 'COMPUTER', '0', 'Windows 10', '未知-未知', '2.4', '2.2', '备用');
INSERT INTO `login_record` VALUES ('917', '27', '183.67.21.153', '2020-06-11 13:35:13', 'COMPUTER', '0', 'Windows 7', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('918', '2', '183.67.21.153', '2020-06-11 13:40:49', 'COMPUTER', '0', 'Windows 7', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('919', '27', '183.67.21.153', '2020-06-11 13:52:22', 'COMPUTER', '0', 'Windows 7', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('920', '27', '183.67.21.153', '2020-06-11 13:55:41', 'COMPUTER', '0', 'Windows 7', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('921', '27', '183.67.21.153', '2020-06-11 13:58:28', 'COMPUTER', '0', 'Windows 7', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('922', '27', '183.67.21.153', '2020-06-11 14:05:38', 'COMPUTER', '0', 'Windows 7', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('923', '27', '183.67.21.153', '2020-06-11 14:06:42', 'COMPUTER', '0', 'Windows 7', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('924', '27', '183.67.21.153', '2020-06-11 14:06:49', 'COMPUTER', '0', 'Windows 7', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('925', '27', '183.67.21.153', '2020-06-11 14:06:58', 'COMPUTER', '0', 'Windows 7', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('926', '27', '183.67.21.153', '2020-06-11 14:07:17', 'COMPUTER', '0', 'Windows 7', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('927', '27', '183.67.21.153', '2020-06-11 14:07:42', 'COMPUTER', '0', 'Windows 7', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('928', '27', '183.67.21.153', '2020-06-11 14:08:31', 'COMPUTER', '0', 'Windows 7', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('929', '1', '183.67.21.153', '2020-06-11 14:08:38', 'COMPUTER', '0', 'Windows 10', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('930', '27', '183.67.21.153', '2020-06-11 14:08:50', 'COMPUTER', '0', 'Windows 7', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('931', '3', '183.67.21.153', '2020-06-11 14:11:27', 'COMPUTER', '0', 'Windows 7', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('932', '27', '183.67.21.153', '2020-06-11 14:11:46', 'COMPUTER', '0', 'Windows 7', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('933', '3', '192.168.120.79', '2020-06-11 14:14:38', 'COMPUTER', '0', 'Windows 10', '未知-未知', '2.4', '2.2', '备用');
INSERT INTO `login_record` VALUES ('934', '3', '183.67.21.153', '2020-06-11 14:16:15', 'COMPUTER', '0', 'Windows 7', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('935', '1', '183.67.21.153', '2020-06-11 14:45:55', 'COMPUTER', '0', 'Windows 7', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('936', '1', '183.67.21.153', '2020-06-11 14:48:41', 'COMPUTER', '0', 'Windows 10', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('937', '1', '183.67.21.153', '2020-06-11 14:51:42', 'COMPUTER', '0', 'Windows 10', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('938', '1', '183.67.21.153', '2020-06-11 14:54:15', 'COMPUTER', '0', 'Windows 10', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('939', '1', '183.67.21.153', '2020-06-11 14:55:38', 'COMPUTER', '0', 'Windows 7', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('940', '1', '183.67.21.153', '2020-06-11 15:04:30', 'COMPUTER', '0', 'Windows 7', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('941', '1', '183.67.21.153', '2020-06-11 15:06:20', 'COMPUTER', '0', 'Windows 7', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('942', '1', '183.67.21.153', '2020-06-11 15:17:26', 'COMPUTER', '0', 'Windows 7', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('943', '1', '183.67.21.153', '2020-06-11 15:18:29', 'COMPUTER', '0', 'Windows 7', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('944', '1', '183.67.21.153', '2020-06-11 15:21:03', 'COMPUTER', '0', 'Windows 7', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('945', '1', '183.67.21.153', '2020-06-11 15:21:12', 'COMPUTER', '0', 'Windows 7', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('946', '1', '183.67.21.153', '2020-06-11 15:28:39', 'COMPUTER', '0', 'Windows 7', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('947', '28', '183.67.21.153', '2020-06-11 15:47:24', 'COMPUTER', '0', 'Windows 7', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('948', '28', '183.67.21.153', '2020-06-11 15:55:05', 'COMPUTER', '0', 'Windows 7', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('949', '2', '183.67.21.153', '2020-06-11 15:57:27', 'COMPUTER', '0', 'Windows 7', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('950', '27', '183.67.21.153', '2020-06-11 16:01:40', 'COMPUTER', '0', 'Windows 7', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('951', '27', '192.168.120.16', '2020-06-11 16:17:25', 'COMPUTER', '0', 'Windows 7', '未知-未知', '2.4', '2.2', '备用');
INSERT INTO `login_record` VALUES ('952', '27', '183.67.21.153', '2020-06-11 16:32:09', 'COMPUTER', '0', 'Windows 7', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('953', '27', '183.67.21.153', '2020-06-11 16:36:57', 'COMPUTER', '0', 'Windows 7', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('954', '2', '183.67.21.153', '2020-06-11 16:51:17', 'COMPUTER', '0', 'Windows 7', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('955', '27', '183.67.21.153', '2020-06-11 16:51:50', 'COMPUTER', '0', 'Windows 7', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('956', '1', '183.67.21.153', '2020-06-11 16:53:11', 'COMPUTER', '0', 'Windows 7', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('957', '2', '183.67.21.153', '2020-06-11 16:53:18', 'COMPUTER', '0', 'Windows 10', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('958', '27', '183.67.21.153', '2020-06-11 17:03:37', 'COMPUTER', '0', 'Windows 7', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('959', '1', '183.67.21.153', '2020-06-11 17:19:36', 'COMPUTER', '0', 'Windows 10', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('960', '27', '183.67.21.153', '2020-06-11 17:20:55', 'COMPUTER', '0', 'Windows 7', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('961', '27', '183.67.21.153', '2020-06-11 17:46:42', 'COMPUTER', '0', 'Windows 7', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('962', '1', '183.67.21.153', '2020-06-11 17:48:20', 'COMPUTER', '0', 'Windows 7', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('963', '27', '183.67.21.153', '2020-06-11 18:33:34', 'COMPUTER', '0', 'Windows 7', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('964', '27', '183.67.21.153', '2020-06-11 18:35:30', 'COMPUTER', '0', 'Windows 7', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('965', '27', '183.67.21.153', '2020-06-11 18:42:08', 'COMPUTER', '0', 'Windows 7', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('966', '27', '183.67.21.153', '2020-06-11 18:56:02', 'COMPUTER', '0', 'Windows 7', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('967', '27', '183.67.21.153', '2020-06-11 19:01:25', 'COMPUTER', '0', 'Windows 7', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('968', '27', '183.67.21.153', '2020-06-11 19:05:47', 'COMPUTER', '0', 'Windows 7', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('969', '27', '183.67.21.153', '2020-06-11 19:12:31', 'COMPUTER', '0', 'Windows 7', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('970', '27', '183.67.21.153', '2020-06-11 19:17:34', 'COMPUTER', '0', 'Windows 7', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('971', '2', '183.67.21.153', '2020-06-12 10:23:57', 'COMPUTER', '0', 'Windows 7', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('972', '1', '183.67.21.153', '2020-06-12 11:21:28', 'COMPUTER', '0', 'Windows 7', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('973', '1', '183.67.17.61', '2020-06-12 16:11:34', 'COMPUTER', '0', 'Windows 10', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('974', '26', '183.67.17.61', '2020-06-12 16:16:47', 'COMPUTER', '0', 'Windows 7', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('975', '29', '183.67.17.61', '2020-06-12 16:16:50', 'COMPUTER', '0', 'Windows 10', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('976', '1', '183.67.17.61', '2020-06-12 16:18:16', 'COMPUTER', '0', 'Windows 7', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('977', '29', '183.67.17.61', '2020-06-12 16:36:15', 'COMPUTER', '0', 'Windows 10', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('978', '1', '183.67.21.153', '2020-06-12 16:39:28', 'COMPUTER', '0', 'Windows 7', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('979', '28', '183.67.17.61', '2020-06-12 16:42:23', 'COMPUTER', '0', 'Windows 10', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('980', '23', '183.67.17.61', '2020-06-12 16:42:42', 'COMPUTER', '0', 'Windows 10', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('981', '1', '183.67.21.153', '2020-06-12 16:49:24', 'COMPUTER', '0', 'Windows 7', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('982', '1', '183.67.21.153', '2020-06-12 16:49:26', 'COMPUTER', '0', 'Windows 7', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('983', '29', '183.67.21.153', '2020-06-12 17:14:14', 'COMPUTER', '0', 'Windows 10', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('984', '1', '183.67.21.153', '2020-06-12 17:31:01', 'COMPUTER', '0', 'Windows 10', '重庆-重庆', '29.5569', '106.5531', '备用');
INSERT INTO `login_record` VALUES ('985', '30', '218.108.147.102', '2020-06-12 20:15:43', 'COMPUTER', '0', 'Windows 10', '浙江省-杭州', '30.294', '120.1619', '备用');

-- ----------------------------
-- Table structure for pay_record
-- ----------------------------
DROP TABLE IF EXISTS `pay_record`;
CREATE TABLE `pay_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `pay_type` varchar(255) DEFAULT NULL COMMENT '支付类型',
  `pay_time` datetime DEFAULT NULL COMMENT '支付时间',
  `money` double(255,0) DEFAULT NULL COMMENT '支付金额',
  `vip_id` int(11) DEFAULT NULL COMMENT '套餐id',
  `status` varchar(255) DEFAULT NULL,
  `spare1` varchar(255) DEFAULT NULL,
  `spare2` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pay_record
-- ----------------------------

-- ----------------------------
-- Table structure for sensitive_operation
-- ----------------------------
DROP TABLE IF EXISTS `sensitive_operation`;
CREATE TABLE `sensitive_operation` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `operation_record` varchar(255) DEFAULT NULL COMMENT '操作记录',
  `operation_time` datetime DEFAULT NULL COMMENT '操作时间',
  `status` int(11) DEFAULT NULL COMMENT '状态',
  `spare1` varchar(255) DEFAULT NULL COMMENT '备用1',
  `spare2` varchar(255) DEFAULT NULL COMMENT '备用2',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sensitive_operation
-- ----------------------------
INSERT INTO `sensitive_operation` VALUES ('1', '1', '修改密码', '2020-06-10 11:08:13', '1', null, null);
INSERT INTO `sensitive_operation` VALUES ('2', '1', '使用邮箱验证修改密码', '2020-06-10 12:06:27', '1', '备用1', '备用2');
INSERT INTO `sensitive_operation` VALUES ('3', '1', '使用手机号验证修改密码', '2020-06-10 14:34:36', '1', '备用1', '备用2');
INSERT INTO `sensitive_operation` VALUES ('4', '1', '使用手机号验证修改密码', '2020-06-10 17:48:39', '1', '备用1', '备用2');
INSERT INTO `sensitive_operation` VALUES ('5', '1', '使用邮箱验证修改密码', '2020-06-10 17:49:41', '1', '备用1', '备用2');
INSERT INTO `sensitive_operation` VALUES ('6', '1', '购买超级会员一年', '2020-06-10 17:51:53', '1', '备用1', '备用2');
INSERT INTO `sensitive_operation` VALUES ('7', '27', '购买黄金会员一个月', '2020-06-11 13:59:36', '1', '备用1', '备用2');
INSERT INTO `sensitive_operation` VALUES ('8', '27', '购买黄金会员一个月', '2020-06-11 14:13:39', '1', '备用1', '备用2');
INSERT INTO `sensitive_operation` VALUES ('9', '28', '购买黄金会员一年', '2020-06-11 15:59:28', '1', '备用1', '备用2');
INSERT INTO `sensitive_operation` VALUES ('10', '28', '购买超级会员一年', '2020-06-11 16:00:32', '1', '备用1', '备用2');
INSERT INTO `sensitive_operation` VALUES ('11', '27', '使用手机号验证修改密码', '2020-06-11 16:12:23', '1', '备用1', '备用2');
INSERT INTO `sensitive_operation` VALUES ('12', '27', '购买黄金会员一年', '2020-06-11 16:24:49', '1', '备用1', '备用2');
INSERT INTO `sensitive_operation` VALUES ('13', '27', '购买超级会员一年', '2020-06-11 16:26:00', '1', '备用1', '备用2');
INSERT INTO `sensitive_operation` VALUES ('14', '1', '使用邮箱验证修改密码', '2020-06-11 17:20:00', '1', '备用1', '备用2');
INSERT INTO `sensitive_operation` VALUES ('15', '1', '使用手机号验证修改密码', '2020-06-11 17:20:27', '1', '备用1', '备用2');

-- ----------------------------
-- Table structure for system_notice
-- ----------------------------
DROP TABLE IF EXISTS `system_notice`;
CREATE TABLE `system_notice` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `content` varchar(255) DEFAULT NULL COMMENT '通知内容',
  `user_id` int(11) DEFAULT NULL COMMENT '用户Id',
  `send_time` datetime DEFAULT NULL COMMENT '发送时间',
  `status` int(11) DEFAULT NULL COMMENT '状态',
  `spare1` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '通知名称',
  `spare2` varchar(255) DEFAULT NULL COMMENT '备用2',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=157 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_notice
-- ----------------------------
INSERT INTO `system_notice` VALUES ('5', '你的会员快到期了，还不赶紧给劳资充值会员', '2', '2020-06-09 10:59:01', '2', '系统自动发送', null);
INSERT INTO `system_notice` VALUES ('6', '你的会员快到期了，还不赶紧给劳资充值会员', '2', '2020-06-09 10:59:01', '2', '系统自动发送', null);
INSERT INTO `system_notice` VALUES ('7', '你的会员快到期了，还不赶紧给劳资充值会员', '2', '2020-06-09 11:03:01', '2', '系统自动发送', null);
INSERT INTO `system_notice` VALUES ('8', '你的会员快到期了，还不赶紧给劳资充值会员', '2', '2020-06-09 11:03:01', '2', '系统自动发送', null);
INSERT INTO `system_notice` VALUES ('9', '发送给所有用户的', '1', '2020-06-09 11:04:14', '2', '测试1', null);
INSERT INTO `system_notice` VALUES ('10', '发送给所有用户的', '2', '2020-06-09 11:04:14', '2', '测试1', null);
INSERT INTO `system_notice` VALUES ('11', '发送给所有用户的', '3', '2020-06-09 11:04:14', '1', '测试1', null);
INSERT INTO `system_notice` VALUES ('12', '发送给所有用户的', '9', '2020-06-09 11:04:14', '1', '测试1', null);
INSERT INTO `system_notice` VALUES ('13', '发送给所有用户的', '10', '2020-06-09 11:04:14', '1', '测试1', null);
INSERT INTO `system_notice` VALUES ('14', '发送给所有用户的', '11', '2020-06-09 11:04:14', '2', '测试1', null);
INSERT INTO `system_notice` VALUES ('15', '发送给所有用户的', '12', '2020-06-09 11:04:14', '1', '测试1', null);
INSERT INTO `system_notice` VALUES ('16', '发送给所有用户的', '13', '2020-06-09 11:04:14', '1', '测试1', null);
INSERT INTO `system_notice` VALUES ('17', '发送给所有用户的', '14', '2020-06-09 11:04:14', '1', '测试1', null);
INSERT INTO `system_notice` VALUES ('18', '发送给所有用户的', '16', '2020-06-09 11:04:14', '1', '测试1', null);
INSERT INTO `system_notice` VALUES ('19', '发送给所有用户的', '17', '2020-06-09 11:04:14', '1', '测试1', null);
INSERT INTO `system_notice` VALUES ('20', '普通会员', '1', '2020-06-09 11:05:13', '2', '测试2', null);
INSERT INTO `system_notice` VALUES ('21', '普通会员', '2', '2020-06-09 11:05:13', '2', '测试2', null);
INSERT INTO `system_notice` VALUES ('22', '普通会员', '2', '2020-06-09 11:05:13', '2', '测试2', null);
INSERT INTO `system_notice` VALUES ('23', '超级会员', '2', '2020-06-09 11:05:13', '2', '测试4', null);
INSERT INTO `system_notice` VALUES ('24', '超级会员', '2', '2020-06-09 11:05:13', '2', '测试4', null);
INSERT INTO `system_notice` VALUES ('25', '超级会员', '11', '2020-06-09 11:05:13', '2', '测试4', null);
INSERT INTO `system_notice` VALUES ('26', '黄金会员购买成功!祝你使用愉快!', '2', '2020-06-09 11:29:35', '2', '会员中心', null);
INSERT INTO `system_notice` VALUES ('28', '黄金会员购买成功!祝你使用愉快!', '1', '2020-06-09 17:19:16', '2', '会员中心', null);
INSERT INTO `system_notice` VALUES ('29', '黄金会员购买成功!祝你使用愉快!', '1', '2020-06-09 20:22:34', '2', '会员中心', null);
INSERT INTO `system_notice` VALUES ('30', '超级会员购买成功!祝你使用愉快!', '1', '2020-06-09 20:28:00', '2', '会员中心', null);
INSERT INTO `system_notice` VALUES ('31', '超级会员购买成功!祝你使用愉快!', '1', '2020-06-09 20:43:19', '2', '会员中心', null);
INSERT INTO `system_notice` VALUES ('32', '超级会员购买成功!祝你使用愉快!', '1', '2020-06-09 21:09:05', '2', '会员中心', null);
INSERT INTO `system_notice` VALUES ('33', '黄金会员购买成功!祝你使用愉快!', '1', '2020-06-10 13:52:50', '2', '会员中心', null);
INSERT INTO `system_notice` VALUES ('34', '快来看看', '1', '2020-06-10 15:28:09', null, '更新系统管理啦', null);
INSERT INTO `system_notice` VALUES ('35', '快来看看', '2', '2020-06-10 15:28:09', null, '更新系统管理啦', null);
INSERT INTO `system_notice` VALUES ('36', '快来看看', '3', '2020-06-10 15:28:09', null, '更新系统管理啦', null);
INSERT INTO `system_notice` VALUES ('37', '快来看看', '9', '2020-06-10 15:28:09', null, '更新系统管理啦', null);
INSERT INTO `system_notice` VALUES ('38', '快来看看', '10', '2020-06-10 15:28:09', null, '更新系统管理啦', null);
INSERT INTO `system_notice` VALUES ('39', '快来看看', '11', '2020-06-10 15:28:09', null, '更新系统管理啦', null);
INSERT INTO `system_notice` VALUES ('40', '快来看看', '12', '2020-06-10 15:28:09', null, '更新系统管理啦', null);
INSERT INTO `system_notice` VALUES ('41', '快来看看', '13', '2020-06-10 15:28:09', null, '更新系统管理啦', null);
INSERT INTO `system_notice` VALUES ('42', '快来看看', '14', '2020-06-10 15:28:09', null, '更新系统管理啦', null);
INSERT INTO `system_notice` VALUES ('43', '快来看看', '16', '2020-06-10 15:28:09', null, '更新系统管理啦', null);
INSERT INTO `system_notice` VALUES ('44', '快来看看', '17', '2020-06-10 15:28:09', null, '更新系统管理啦', null);
INSERT INTO `system_notice` VALUES ('45', '超级会员购买成功!祝你使用愉快!', '1', '2020-06-10 17:51:54', '2', '会员中心', null);
INSERT INTO `system_notice` VALUES ('46', '黄金会员购买成功!祝你使用愉快!', '27', '2020-06-11 13:59:37', '2', '会员中心', null);
INSERT INTO `system_notice` VALUES ('47', '黄金会员购买成功!祝你使用愉快!', '27', '2020-06-11 14:13:40', '2', '会员中心', null);
INSERT INTO `system_notice` VALUES ('48', '黄金会员购买成功!祝你使用愉快!', '28', '2020-06-11 15:59:29', '2', '会员中心', null);
INSERT INTO `system_notice` VALUES ('49', '超级会员购买成功!祝你使用愉快!', '28', '2020-06-11 16:00:33', '2', '会员中心', null);
INSERT INTO `system_notice` VALUES ('50', '黄金会员购买成功!祝你使用愉快!', '27', '2020-06-11 16:24:50', '2', '会员中心', null);
INSERT INTO `system_notice` VALUES ('51', '超级会员购买成功!祝你使用愉快!', '27', '2020-06-11 16:26:01', '2', '会员中心', null);
INSERT INTO `system_notice` VALUES ('112', '充值！！！！', '1', '2020-06-11 16:33:57', null, '', null);
INSERT INTO `system_notice` VALUES ('113', '充值！！！！', '2', '2020-06-11 16:33:57', null, '', null);
INSERT INTO `system_notice` VALUES ('114', '充值！！！！', '3', '2020-06-11 16:33:57', null, '', null);
INSERT INTO `system_notice` VALUES ('115', '充值！！！！', '9', '2020-06-11 16:33:57', null, '', null);
INSERT INTO `system_notice` VALUES ('116', '充值！！！！', '10', '2020-06-11 16:33:57', null, '', null);
INSERT INTO `system_notice` VALUES ('117', '充值！！！！', '11', '2020-06-11 16:33:57', null, '', null);
INSERT INTO `system_notice` VALUES ('118', '充值！！！！', '12', '2020-06-11 16:33:57', null, '', null);
INSERT INTO `system_notice` VALUES ('119', '充值！！！！', '13', '2020-06-11 16:33:57', null, '', null);
INSERT INTO `system_notice` VALUES ('120', '充值！！！！', '14', '2020-06-11 16:33:57', null, '', null);
INSERT INTO `system_notice` VALUES ('121', '充值！！！！', '16', '2020-06-11 16:33:57', null, '', null);
INSERT INTO `system_notice` VALUES ('122', '充值！！！！', '17', '2020-06-11 16:33:57', null, '', null);
INSERT INTO `system_notice` VALUES ('123', '充值！！！！', '20', '2020-06-11 16:33:57', null, '', null);
INSERT INTO `system_notice` VALUES ('124', '充值！！！！', '21', '2020-06-11 16:33:57', null, '', null);
INSERT INTO `system_notice` VALUES ('125', '充值！！！！', '22', '2020-06-11 16:33:57', null, '', null);
INSERT INTO `system_notice` VALUES ('126', '充值！！！！', '23', '2020-06-11 16:33:57', null, '', null);
INSERT INTO `system_notice` VALUES ('127', '充值！！！！', '24', '2020-06-11 16:33:57', null, '', null);
INSERT INTO `system_notice` VALUES ('128', '充值！！！！', '25', '2020-06-11 16:33:57', null, '', null);
INSERT INTO `system_notice` VALUES ('129', '充值！！！！', '26', '2020-06-11 16:33:57', null, '', null);
INSERT INTO `system_notice` VALUES ('130', '充值！！！！', '27', '2020-06-11 16:33:57', null, '', null);
INSERT INTO `system_notice` VALUES ('131', '充值！！！！', '28', '2020-06-11 16:33:57', null, '', null);
INSERT INTO `system_notice` VALUES ('132', '充值！！！！', '1', '2020-06-11 16:33:57', null, '', null);
INSERT INTO `system_notice` VALUES ('133', '充值！！！！', '2', '2020-06-11 16:33:57', null, '', null);
INSERT INTO `system_notice` VALUES ('134', '充值！！！！', '3', '2020-06-11 16:33:57', null, '', null);
INSERT INTO `system_notice` VALUES ('135', '充值！！！！', '27', '2020-06-11 16:33:57', null, '', null);
INSERT INTO `system_notice` VALUES ('136', '充值！！！！', '28', '2020-06-11 16:33:57', null, '', null);
INSERT INTO `system_notice` VALUES ('137', '这只是一个测试而已', '1', '2020-06-11 16:50:17', '2', 'admin测试', null);
INSERT INTO `system_notice` VALUES ('138', '这只是一个测试而已', '2', '2020-06-11 16:50:17', '2', 'admin测试', null);
INSERT INTO `system_notice` VALUES ('139', '这只是一个测试而已', '3', '2020-06-11 16:50:17', '1', 'admin测试', null);
INSERT INTO `system_notice` VALUES ('140', '这只是一个测试而已', '9', '2020-06-11 16:50:17', '1', 'admin测试', null);
INSERT INTO `system_notice` VALUES ('141', '这只是一个测试而已', '10', '2020-06-11 16:50:17', '1', 'admin测试', null);
INSERT INTO `system_notice` VALUES ('142', '这只是一个测试而已', '11', '2020-06-11 16:50:17', '1', 'admin测试', null);
INSERT INTO `system_notice` VALUES ('143', '这只是一个测试而已', '12', '2020-06-11 16:50:17', '1', 'admin测试', null);
INSERT INTO `system_notice` VALUES ('144', '这只是一个测试而已', '13', '2020-06-11 16:50:17', '1', 'admin测试', null);
INSERT INTO `system_notice` VALUES ('145', '这只是一个测试而已', '14', '2020-06-11 16:50:17', '1', 'admin测试', null);
INSERT INTO `system_notice` VALUES ('146', '这只是一个测试而已', '16', '2020-06-11 16:50:17', '1', 'admin测试', null);
INSERT INTO `system_notice` VALUES ('147', '这只是一个测试而已', '17', '2020-06-11 16:50:17', '1', 'admin测试', null);
INSERT INTO `system_notice` VALUES ('148', '这只是一个测试而已', '20', '2020-06-11 16:50:17', '1', 'admin测试', null);
INSERT INTO `system_notice` VALUES ('149', '这只是一个测试而已', '21', '2020-06-11 16:50:17', '1', 'admin测试', null);
INSERT INTO `system_notice` VALUES ('150', '这只是一个测试而已', '22', '2020-06-11 16:50:17', '1', 'admin测试', null);
INSERT INTO `system_notice` VALUES ('151', '这只是一个测试而已', '23', '2020-06-11 16:50:17', '2', 'admin测试', null);
INSERT INTO `system_notice` VALUES ('152', '这只是一个测试而已', '24', '2020-06-11 16:50:17', '1', 'admin测试', null);
INSERT INTO `system_notice` VALUES ('153', '这只是一个测试而已', '25', '2020-06-11 16:50:17', '1', 'admin测试', null);
INSERT INTO `system_notice` VALUES ('154', '这只是一个测试而已', '26', '2020-06-11 16:50:17', '2', 'admin测试', null);
INSERT INTO `system_notice` VALUES ('155', '这只是一个测试而已', '27', '2020-06-11 16:50:17', '2', 'admin测试', null);
INSERT INTO `system_notice` VALUES ('156', '这只是一个测试而已', '28', '2020-06-11 16:50:17', '1', 'admin测试', null);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(255) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `username` varchar(255) DEFAULT NULL COMMENT '用户名',
  `tel` char(11) DEFAULT NULL COMMENT '手机号',
  `password` varchar(255) DEFAULT NULL COMMENT '密码',
  `nickname` varchar(255) DEFAULT NULL COMMENT '昵称',
  `register_time` datetime DEFAULT NULL COMMENT '注册时间',
  `email` varchar(255) DEFAULT NULL COMMENT '邮箱',
  `id_card` varchar(255) DEFAULT NULL COMMENT '身份证号',
  `qq` varchar(255) DEFAULT NULL COMMENT 'QQ',
  `img` varchar(255) DEFAULT NULL COMMENT '头像',
  `skin` varchar(255) DEFAULT NULL COMMENT '皮肤',
  `current_capacity` double(255,0) DEFAULT NULL COMMENT '已用容量，单位M',
  `role` int(255) DEFAULT NULL COMMENT '角色，0为管理员，1为用户',
  `status` int(255) DEFAULT NULL COMMENT '状态',
  `spare1` varchar(255) DEFAULT NULL,
  `spare2` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'zhangsan', '18883147054', '123456', '张三', '2020-04-28 16:36:05', '1220809437@qq.com', '3215213251', '92849242', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/1/b905130d-a39f-450c-a760-0ddb18872263.jpg', '', '0', null, '1', null, '5120');
INSERT INTO `user` VALUES ('2', 'lisi', '17890457899', '123456', '李四', '2020-05-13 12:04:53', '1220809437@qq.com', '23521421421', '818921', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/2/0ac34c8c-70fd-4201-98f4-b187e80bc1f3.jpg', null, '0', null, '1', null, '5120');
INSERT INTO `user` VALUES ('3', 'ccc', '15213202560', '123456', '哈喽', '2020-05-28 10:13:02', '2222@qq.com', '55555555555', '55555555', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/3/02f27db6-570c-4774-8ecc-1c4d6ede481a.jpg', null, '8', null, '1', null, '5120');
INSERT INTO `user` VALUES ('9', 'wangwu', '15730265210', 'QWEasd123@', '阿王', null, null, null, null, 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/img/toux.jpg', null, '0', null, '1', null, '5120');
INSERT INTO `user` VALUES ('10', 'abcd', '15730265211', 'QWEasd123@', '阿百川', null, null, null, null, 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/img/toux.jpg', null, '0', null, '2', null, '5120');
INSERT INTO `user` VALUES ('11', 'cloud', '15223806815', '123456', '马小云', '2020-06-16 15:58:36', '2222@qq.com', '2324242422', '214114142142', '/img/dai/headImg3.jpeg', null, '142', null, '2', null, '5120');
INSERT INTO `user` VALUES ('12', 'qianqian', '13101131396', '123456', '阿千', '2020-06-06 17:22:02', '111@qq.com', '3123213123', '123123123', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/12/52c08a02-9bd3-4c75-907d-2df51a4dd7cb.jpg', null, '100', null, '2', null, '5120');
INSERT INTO `user` VALUES ('13', 'wangmeimei', '13368147879', '123456', 'wangmeimei', '2020-06-06 18:22:05', null, null, null, 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/img/toux.jpg', null, '100', null, '2', null, '5120');
INSERT INTO `user` VALUES ('14', '小哥哥', '13368147879', 'QWEasd123@', '小哥哥', '2020-06-06 18:24:43', null, null, null, 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/img/toux.jpg', null, '100', null, '2', null, '5120');
INSERT INTO `user` VALUES ('16', 'xiaohong', '15730265210', 'QWEasd123@', 'xiaohong', '2020-06-07 22:19:49', null, null, null, 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/img/toux.jpg', null, null, null, '2', null, null);
INSERT INTO `user` VALUES ('17', 'xiaolongnv', '15730265210', 'QWEasd123@!', 'xiaolongnv', '2020-06-08 11:36:26', null, null, null, 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/img/toux.jpg', null, null, null, '2', null, null);
INSERT INTO `user` VALUES ('20', 'woniu', '18883147054', 'Woniu123.', 'woniu', '2020-06-10 17:28:03', null, null, null, 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/img/toux.jpg', null, null, null, '1', null, null);
INSERT INTO `user` VALUES ('21', 'xiaomei', '15730265210', 'QWasd123??', 'xiaomei', '2020-06-10 18:54:56', null, null, null, 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/img/toux.jpg', null, null, null, '1', null, null);
INSERT INTO `user` VALUES ('22', 'xiaolan', '15730265210', 'QWEasd123?', 'xiaolan', '2020-06-10 20:19:57', null, null, null, 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/img/toux.jpg', null, null, null, '1', null, null);
INSERT INTO `user` VALUES ('23', 'xiaolv', '15730265210', 'QWEasd123??', 'xiaolv', '2020-06-10 20:23:37', '                                                                                                                                                                                                                     ', null, null, 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/img/toux.jpg', null, '0', null, '1', null, null);
INSERT INTO `user` VALUES ('24', 'xiaohuang', '15730265210', 'QWEasd123??', 'xiaohuang', '2020-06-10 20:29:18', null, null, null, 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/img/toux.jpg', null, null, null, '1', null, null);
INSERT INTO `user` VALUES ('25', 'xiaoxiao', '15730265210', 'QWEasd123?', 'xiaoxiao', '2020-06-10 20:35:06', '1220809437@qq.com', null, null, 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/img/toux.jpg', null, null, null, '1', null, null);
INSERT INTO `user` VALUES ('26', 'Lyon', '15340417775', 'Cnm123...', 'Lyon', '2020-06-10 22:22:53', '1468359425@qq.com', null, null, 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/26/5203d72d-4f78-448b-bc56-10a96cf01fdf.jpg', null, '0', null, '1', null, null);
INSERT INTO `user` VALUES ('27', 'smallwoniu', '15223806815', '1234567', 'smallwoniu', '2020-06-11 09:26:58', '928874202@qq.com', null, '923432322', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/4bcd4a23-8733-472d-a63b-8a14a75acfc6.jpg', null, '301', null, '1', null, null);
INSERT INTO `user` VALUES ('28', 'xiao', '15223806815', 'Dai123..', 'xiao', '2020-06-11 15:47:15', '928874202@qq.com', null, null, 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/28/94709c89-a4c7-48a6-8f99-5d5fd3ed7711.jpg', null, '0', null, '1', null, null);
INSERT INTO `user` VALUES ('29', 'beiju', '18883147054', 'Beiju951220=-=-', 'beiju', '2020-06-12 16:16:25', '1094732594@qq.com', null, null, 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/29/96afece4-1dec-4bff-a66c-0b25be04a0e6.jpg', null, '0', null, '1', null, null);
INSERT INTO `user` VALUES ('30', 'woshinidie', '15736217502', 'Dw960116.', 'woshinidie', '2020-06-12 20:15:30', '412934707@qq.com', null, null, 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/img/toux.jpg', null, '27', null, '1', null, null);

-- ----------------------------
-- Table structure for user_directory
-- ----------------------------
DROP TABLE IF EXISTS `user_directory`;
CREATE TABLE `user_directory` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` int(11) DEFAULT NULL COMMENT '用户Id',
  `name` varchar(255) DEFAULT NULL COMMENT '文件夹名称',
  `parent_id` int(11) DEFAULT NULL COMMENT '父文件夹Id',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `over_time` datetime DEFAULT NULL COMMENT '过期时间',
  `status` int(255) DEFAULT NULL COMMENT '0为已删除，1位回收站，2位正常',
  `spare1` varchar(255) DEFAULT NULL COMMENT '备用1',
  `spare2` varchar(255) DEFAULT NULL COMMENT '备用2',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=930 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_directory
-- ----------------------------
INSERT INTO `user_directory` VALUES ('1', '1', 'myGirls', '0', '2020-06-08 00:07:22', '2020-06-08 00:07:22', '0', null, null);
INSERT INTO `user_directory` VALUES ('2', '2', '文件夹1', '1', '2020-05-20 11:30:50', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('4', '1', 'video', '1', '2020-06-01 12:05:32', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('5', '1', '陈浩', '0', '2020-06-05 16:38:50', null, null, null, null);
INSERT INTO `user_directory` VALUES ('6', '1', '私密', '4', '2020-06-01 17:59:25', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('11', '1', '电影', '4', '2020-06-03 13:49:03', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('12', '1', '综艺', '4', '2020-06-01 13:49:28', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('13', '1', '神奇四侠全集', '11', '2020-06-02 13:50:07', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('14', '1', '我的视频', '0', '2020-06-09 19:26:11', '2020-06-09 19:26:11', '2', null, null);
INSERT INTO `user_directory` VALUES ('22', '1', '张三的歌', '14', '2020-06-10 19:39:32', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('23', '1', 'abcd', '5', '2020-06-05 16:38:20', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('24', '1', '你好', '14', '2020-06-10 19:06:07', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('25', '1', '哈哈哈哈', '14', '2020-06-11 15:54:47', '2020-06-11 15:54:47', '0', null, null);
INSERT INTO `user_directory` VALUES ('26', '1', '范德萨', '14', '2020-06-10 17:38:40', '2020-06-10 17:38:40', '0', null, null);
INSERT INTO `user_directory` VALUES ('27', null, '新建文件夹1', null, null, null, null, null, null);
INSERT INTO `user_directory` VALUES ('30', '1', '11', '4', '2020-06-04 14:33:46', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('31', '1', '123', '0', '2020-06-08 00:07:22', '2020-06-08 00:07:22', '0', null, null);
INSERT INTO `user_directory` VALUES ('32', '1', '999', '1', '2020-06-04 15:03:59', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('372', '1', 'myGirls', '25', '2020-06-04 17:17:52', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('373', '1', 'video', '372', '2020-06-04 17:17:52', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('374', '1', '私密', '373', '2020-06-04 17:17:52', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('375', '1', '电影', '373', '2020-06-04 17:17:52', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('376', '1', '神奇四侠全集', '375', '2020-06-04 17:17:52', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('377', '1', '综艺', '373', '2020-06-04 17:17:52', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('396', '1', '变形金刚等多个文件', '24', '2020-06-04 18:57:40', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('397', '1', 'myGirls', '396', '2020-06-04 18:57:40', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('400', '1', '变形金刚', '396', '2020-06-04 18:57:40', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('401', '1', '变形金刚等多个文件', '22', '2020-06-04 19:21:08', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('402', '1', 'myGirls', '401', '2020-06-04 19:21:08', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('405', '1', '变形金刚', '401', '2020-06-10 15:23:12', '2020-06-10 15:23:12', '2', null, null);
INSERT INTO `user_directory` VALUES ('406', '2', '我的视频', '7', '2020-06-05 10:10:59', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('407', '2', '小美的私房照', '406', '2020-06-05 10:10:59', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('408', '2', '小美的私房照1', '407', '2020-06-05 10:10:59', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('409', '2', '浴室', '408', '2020-06-05 10:10:59', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('410', '2', '厨房', '408', '2020-06-05 10:10:59', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('411', '2', '沙发', '408', '2020-06-05 10:10:59', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('412', '2', '卧室', '408', '2020-06-05 10:10:59', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('413', '2', '客厅', '408', '2020-06-05 10:10:59', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('414', '2', '小黄', '406', '2020-06-05 10:10:59', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('415', '2', '变形金刚等多个文件', '414', '2020-06-05 10:10:59', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('416', '2', 'myGirls', '415', '2020-06-05 10:10:59', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('417', '2', '文件夹1', '416', '2020-06-05 10:10:59', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('418', '2', '文件夹2', '416', '2020-06-05 10:10:59', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('419', '2', '变形金刚', '415', '2020-06-05 10:10:59', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('420', '2', '你好', '406', '2020-06-05 10:10:59', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('421', '2', '变形金刚等多个文件', '420', '2020-06-05 10:10:59', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('422', '2', 'myGirls', '421', '2020-06-05 10:10:59', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('423', '2', '文件夹1', '422', '2020-06-05 10:10:59', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('424', '2', '文件夹2', '422', '2020-06-05 10:10:59', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('425', '2', '变形金刚', '421', '2020-06-05 10:10:59', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('426', '2', '哈哈哈哈', '406', '2020-06-05 10:10:59', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('427', '2', 'myGirls', '426', '2020-06-05 10:10:59', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('428', '2', 'video', '427', '2020-06-05 10:10:59', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('429', '2', '私密', '428', '2020-06-05 10:10:59', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('430', '2', '电影', '428', '2020-06-05 10:10:59', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('431', '2', '神奇四侠全集', '430', '2020-06-05 10:10:59', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('432', '2', '综艺', '428', '2020-06-05 10:10:59', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('433', '2', '11', '428', '2020-06-05 10:10:59', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('434', '2', '999', '427', '2020-06-05 10:10:59', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('435', '2', '范德萨', '406', '2020-06-05 10:10:59', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('436', '2', '沙发等多个文件', '0', '2020-06-09 19:19:56', '2020-06-09 19:19:56', '2', null, null);
INSERT INTO `user_directory` VALUES ('437', '2', '浴室', '436', '2020-06-07 23:03:54', '2020-06-07 23:03:54', '2', null, null);
INSERT INTO `user_directory` VALUES ('438', '2', '厨房', '436', '2020-06-05 16:03:29', null, '0', null, null);
INSERT INTO `user_directory` VALUES ('439', '2', '沙发', '436', '2020-06-05 16:03:29', null, '0', null, null);
INSERT INTO `user_directory` VALUES ('440', '2', '你好等多个文件', '0', '2020-06-05 16:03:29', null, '0', null, null);
INSERT INTO `user_directory` VALUES ('441', '2', '小黄', '440', '2020-06-05 13:55:04', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('442', '2', '变形金刚等多个文件', '441', '2020-06-05 13:55:04', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('443', '2', 'myGirls', '442', '2020-06-05 13:55:04', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('444', '2', '文件夹1', '443', '2020-06-05 13:55:04', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('445', '2', '文件夹2', '443', '2020-06-05 13:55:04', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('446', '2', '变形金刚', '442', '2020-06-05 13:55:04', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('447', '2', '你好', '440', '2020-06-05 13:55:04', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('448', '2', '变形金刚等多个文件', '447', '2020-06-05 13:55:04', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('449', '2', 'myGirls', '448', '2020-06-05 13:55:04', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('450', '2', '文件夹1', '449', '2020-06-05 13:55:04', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('451', '2', '文件夹2', '449', '2020-06-05 13:55:04', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('452', '2', '变形金刚', '448', '2020-06-05 13:55:04', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('453', '2', 'video等多个文件', '0', '2020-06-05 16:03:29', null, '0', null, null);
INSERT INTO `user_directory` VALUES ('454', '2', 'video', '453', '2020-06-05 13:57:53', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('455', '2', '私密', '454', '2020-06-05 13:57:53', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('456', '2', '电影', '454', '2020-06-05 13:57:53', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('457', '2', '神奇四侠全集', '456', '2020-06-05 13:57:53', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('458', '2', '综艺', '454', '2020-06-05 13:57:53', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('459', '1', '删除文件夹', '0', '2020-06-05 16:03:29', null, '0', null, null);
INSERT INTO `user_directory` VALUES ('460', '1', '你好', '0', '2020-06-05 16:03:29', null, '0', null, null);
INSERT INTO `user_directory` VALUES ('461', '1', 'test1文件夹', '0', '2020-06-05 16:03:29', null, '0', null, null);
INSERT INTO `user_directory` VALUES ('462', '2', '女朋友哦', '0', '2020-06-05 16:14:05', '2020-06-05 16:14:05', '0', null, null);
INSERT INTO `user_directory` VALUES ('463', '2', 'abc', '0', '2020-06-09 17:49:31', '2020-06-09 17:49:31', '0', null, null);
INSERT INTO `user_directory` VALUES ('464', '2', '女朋友哦', '463', '2020-06-09 16:43:46', '2020-06-09 16:43:46', '0', null, null);
INSERT INTO `user_directory` VALUES ('465', '1', '沙发等多个文件等多个文件', '31', '2020-06-05 16:59:05', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('466', '1', '沙发等多个文件', '465', '2020-06-05 16:59:05', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('467', '2', '我的视频', '0', '2020-06-07 23:53:56', '2020-06-07 23:53:56', '0', null, null);
INSERT INTO `user_directory` VALUES ('468', '2', '小黄', '467', '2020-06-05 17:04:40', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('469', '2', '变形金刚等多个文件', '468', '2020-06-05 17:04:40', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('470', '2', 'myGirls', '469', '2020-06-05 17:04:40', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('471', '2', '变形金刚', '469', '2020-06-05 17:04:40', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('472', '2', '你好', '467', '2020-06-05 17:04:40', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('473', '2', '变形金刚等多个文件', '472', '2020-06-05 17:04:40', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('474', '2', 'myGirls', '473', '2020-06-05 17:04:40', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('475', '2', '变形金刚', '473', '2020-06-05 17:04:40', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('476', '2', '哈哈哈哈', '467', '2020-06-05 17:04:40', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('477', '2', 'myGirls', '476', '2020-06-05 17:04:40', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('478', '2', 'video', '477', '2020-06-05 17:04:40', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('479', '2', '私密', '478', '2020-06-05 17:04:40', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('480', '2', '电影', '478', '2020-06-05 17:04:40', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('481', '2', '神奇四侠全集', '480', '2020-06-05 17:04:40', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('482', '2', '综艺', '478', '2020-06-05 17:04:40', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('483', '2', '范德萨', '467', '2020-06-07 22:51:39', '2020-06-07 22:51:39', '2', null, null);
INSERT INTO `user_directory` VALUES ('484', '1', '新建文件夹1', '0', '2020-06-09 16:03:11', '2020-06-09 16:03:11', '0', null, null);
INSERT INTO `user_directory` VALUES ('485', '1', '新建文件夹2', '0', '2020-06-09 16:03:11', '2020-06-09 16:03:11', '0', null, null);
INSERT INTO `user_directory` VALUES ('486', '1', '跑男', '12', '2020-06-05 18:39:47', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('487', '1', '陈浩', '486', '2020-06-05 18:39:54', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('488', '1', 'abcd', '487', '2020-06-05 18:39:55', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('489', '1', '今天是个好日子', '2', '2020-06-05 18:59:36', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('490', '1', '你Tm新建不了?', '464', '2020-06-05 19:23:05', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('491', '1', '幅度为', '485', '2020-06-06 10:44:07', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('492', '1', '新建文件夹2', '31', '2020-06-06 11:05:20', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('493', '1', '幅度为', '492', '2020-06-06 11:05:21', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('494', '1', '你好啊', '0', '2020-06-09 17:48:43', '2020-06-09 17:48:43', '0', null, null);
INSERT INTO `user_directory` VALUES ('495', '1', '你好啊', '1', '2020-06-06 12:51:20', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('496', '3', '你好等多个文件', '0', '2020-06-09 16:03:11', '2020-06-09 16:03:11', '0', null, null);
INSERT INTO `user_directory` VALUES ('497', '3', '小黄', '496', '2020-06-06 14:21:26', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('498', '3', '变形金刚等多个文件', '497', '2020-06-06 14:21:26', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('499', '3', 'myGirls', '498', '2020-06-06 14:21:26', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('500', '3', '变形金刚', '498', '2020-06-06 14:21:26', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('501', '3', '你好', '496', '2020-06-06 14:21:26', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('502', '3', '变形金刚等多个文件', '501', '2020-06-06 14:21:26', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('503', '3', 'myGirls', '502', '2020-06-06 14:21:26', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('504', '3', '变形金刚', '502', '2020-06-06 14:21:26', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('505', '1', '上传不了?', '0', '2020-06-08 13:20:05', '2020-06-08 13:20:05', '0', null, null);
INSERT INTO `user_directory` VALUES ('506', '1', '你好啊', '505', '2020-06-06 16:37:33', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('507', '1', '新建文件夹1', '494', '2020-06-07 00:57:36', '2020-06-07 00:57:36', '2', null, null);
INSERT INTO `user_directory` VALUES ('508', '1', '测试文件夹', '485', '2020-06-08 00:13:27', '2020-06-08 00:06:29', '2', null, null);
INSERT INTO `user_directory` VALUES ('509', '1', '你好', '494', '2020-06-08 13:06:28', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('510', '1', '变形金刚等多个文件', '509', '2020-06-08 13:06:29', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('511', '1', 'myGirls', '510', '2020-06-08 13:06:29', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('512', '1', '变形金刚', '510', '2020-06-08 13:06:29', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('513', '1', '我的资料', '0', '2020-06-09 16:03:11', '2020-06-09 16:03:11', '0', null, null);
INSERT INTO `user_directory` VALUES ('514', '1', '第一阶段', '513', '2020-06-08 15:47:27', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('515', '1', '第二阶段', '513', '2020-06-08 15:47:46', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('516', '1', '第三阶段', '513', '2020-06-08 15:49:17', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('517', '1', '第四阶段', '513', '2020-06-08 15:49:33', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('518', '11', '图片', '0', '2020-06-09 16:03:11', '2020-06-09 16:03:11', '0', null, null);
INSERT INTO `user_directory` VALUES ('519', '11', '哦哦', '518', '2020-06-09 16:03:11', '2020-06-09 16:03:11', '0', null, null);
INSERT INTO `user_directory` VALUES ('520', '11', '哦哦', '0', '2020-06-09 16:03:11', '2020-06-09 16:03:11', '0', null, null);
INSERT INTO `user_directory` VALUES ('521', '11', '哦哦', '0', '2020-06-09 16:03:11', '2020-06-09 16:03:11', '0', null, null);
INSERT INTO `user_directory` VALUES ('522', '11', '哦哦', '0', '2020-06-09 16:03:11', '2020-06-09 16:03:11', '0', null, null);
INSERT INTO `user_directory` VALUES ('523', '11', '哦哦', '0', '2020-06-09 16:03:11', '2020-06-09 16:03:11', '0', null, null);
INSERT INTO `user_directory` VALUES ('524', '11', '新建文件夹', '518', '2020-06-09 16:03:11', '2020-06-09 16:03:11', '0', null, null);
INSERT INTO `user_directory` VALUES ('525', '11', '新建文件夹', '0', '2020-06-09 16:03:11', '2020-06-09 16:03:11', '0', null, null);
INSERT INTO `user_directory` VALUES ('526', '11', '新建文件夹', '0', '2020-06-09 16:03:11', '2020-06-09 16:03:11', '0', null, null);
INSERT INTO `user_directory` VALUES ('527', '11', '音乐', '0', '2020-06-09 16:03:11', '2020-06-09 16:03:11', '0', null, null);
INSERT INTO `user_directory` VALUES ('528', '11', '音乐', '518', '2020-06-08 20:29:53', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('529', '11', '音乐', '0', '2020-06-09 16:03:11', '2020-06-09 16:03:11', '0', null, null);
INSERT INTO `user_directory` VALUES ('530', '11', '音乐', '0', '2020-06-09 16:03:11', '2020-06-09 16:03:11', '0', null, null);
INSERT INTO `user_directory` VALUES ('531', '2', '你好', '0', '2020-06-09 16:03:11', '2020-06-09 16:03:11', '0', null, null);
INSERT INTO `user_directory` VALUES ('532', '2', '变形金刚等多个文件', '531', '2020-06-08 21:11:54', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('533', '2', 'myGirls', '532', '2020-06-08 21:11:54', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('534', '2', '变形金刚', '532', '2020-06-08 21:11:54', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('535', '2', '小姐姐等多个文件', '0', '2020-06-09 17:48:43', '2020-06-09 17:48:43', '0', null, null);
INSERT INTO `user_directory` VALUES ('536', '2', 'myGirls', '535', '2020-06-08 21:21:52', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('537', '2', 'video', '536', '2020-06-08 21:21:52', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('538', '2', '私密', '537', '2020-06-08 21:21:52', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('539', '2', '电影', '537', '2020-06-08 21:21:52', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('540', '2', '神奇四侠全集', '539', '2020-06-08 21:21:52', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('541', '2', '综艺', '537', '2020-06-08 21:21:52', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('542', '2', '跑男', '541', '2020-06-08 21:21:52', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('543', '2', '陈浩', '542', '2020-06-08 21:21:52', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('544', '2', 'abcd', '543', '2020-06-08 21:21:52', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('545', '2', '11', '537', '2020-06-08 21:21:52', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('546', '2', '999', '536', '2020-06-08 21:21:52', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('547', '2', '你好啊', '536', '2020-06-09 16:45:49', '2020-06-09 16:45:49', '0', null, null);
INSERT INTO `user_directory` VALUES ('548', '2', 'myGirls', '0', '2020-06-09 16:03:15', '2020-06-09 16:03:15', '0', null, null);
INSERT INTO `user_directory` VALUES ('549', '2', 'video', '548', '2020-06-08 21:22:05', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('550', '2', '私密', '549', '2020-06-08 21:22:05', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('551', '2', '电影', '549', '2020-06-08 21:22:05', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('552', '2', '神奇四侠全集', '551', '2020-06-08 21:22:05', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('553', '2', '综艺', '549', '2020-06-08 21:22:05', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('554', '2', '跑男', '553', '2020-06-08 21:22:05', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('555', '2', '陈浩', '554', '2020-06-08 21:22:05', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('556', '2', 'abcd', '555', '2020-06-08 21:22:05', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('557', '2', '11', '549', '2020-06-08 21:22:05', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('558', '2', '999', '548', '2020-06-08 21:22:05', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('559', '2', '你好啊', '548', '2020-06-08 21:22:05', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('560', '12', '新建', '0', '2020-06-09 19:51:24', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('561', '1', '这是新建的文件夹', '0', '2020-06-10 17:37:10', '2020-06-10 17:37:10', '0', null, null);
INSERT INTO `user_directory` VALUES ('562', '1', '小黑', '402', '2020-06-10 17:35:19', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('563', '1', '小白', '402', '2020-06-10 17:36:11', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('564', '1', '小小', '22', '2020-06-10 17:36:30', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('565', '1', '神奇四侠', '401', '2020-06-10 17:36:46', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('566', '1', '大大', '564', '2020-06-10 19:07:17', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('567', '1', '你好', '566', '2020-06-10 19:07:25', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('568', '1', '变形金刚等多个文件', '567', '2020-06-10 19:07:26', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('569', '1', 'myGirls', '568', '2020-06-10 19:07:26', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('570', '1', '变形金刚', '568', '2020-06-10 19:07:26', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('571', '1', '小白', '401', '2020-06-10 19:17:49', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('572', '1', '小黑', '401', '2020-06-10 19:18:48', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('573', '1', '小红', '14', '2020-06-10 19:32:58', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('574', '1', '嘿嘿嘿', '14', '2020-06-10 19:42:05', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('575', '3', 'ccc', '0', '2020-06-11 13:18:16', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('576', '3', 'Kalimba.mp3等多个文件', '575', '2020-06-11 13:18:20', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('577', '27', '我的文件', '0', '2020-06-12 16:40:52', '2020-06-12 16:40:52', '0', null, null);
INSERT INTO `user_directory` VALUES ('578', '27', '图片', '577', '2020-06-11 13:23:45', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('579', '27', '音乐', '577', '2020-06-11 13:23:52', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('580', '27', '视频', '577', '2020-06-11 13:24:03', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('581', '27', '文档', '577', '2020-06-11 13:27:30', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('582', '27', '我的分享', '0', '2020-06-11 15:54:47', '2020-06-11 15:54:47', '0', null, null);
INSERT INTO `user_directory` VALUES ('583', '27', 'i4Tools_v7.98.15_Setup.exe等多个文件', '582', '2020-06-11 13:50:13', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('584', '27', 'i4Tools_v7.98.15_Setup.exe等多个文件', '577', '2020-06-11 15:54:47', '2020-06-11 15:54:47', '0', null, null);
INSERT INTO `user_directory` VALUES ('585', '28', '音乐', '0', '2020-06-11 15:51:26', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('586', '28', '我的视频', '585', '2020-06-11 15:51:53', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('587', '28', '新建文件夹', '0', '2020-06-11 15:53:08', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('588', '28', '我的视频', '587', '2020-06-11 15:53:31', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('589', '28', 'video.mp4等多个文件', '0', '2020-06-11 16:06:31', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('590', '27', '随笔', '0', '2020-06-12 16:40:52', '2020-06-12 16:40:52', '0', null, null);
INSERT INTO `user_directory` VALUES ('591', '27', '我的文件', '590', '2020-06-11 16:15:02', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('592', '27', '图片', '591', '2020-06-11 16:15:03', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('593', '27', '音乐', '591', '2020-06-11 16:15:03', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('594', '27', '视频', '591', '2020-06-11 16:15:03', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('595', '27', '文档', '591', '2020-06-11 16:15:03', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('596', '27', '我的文件', '590', '2020-06-11 16:16:03', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('597', '27', '图片', '596', '2020-06-11 16:16:03', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('598', '27', '音乐', '596', '2020-06-11 16:16:03', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('599', '27', '视频', '596', '2020-06-11 16:16:03', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('600', '27', '文档', '596', '2020-06-11 16:16:03', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('601', '27', '随笔', '590', '2020-06-11 16:16:05', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('689', '27', '随笔', '577', '2020-06-11 16:18:35', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('690', '27', '我的文件', '689', '2020-06-11 16:18:35', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('691', '27', '图片', '690', '2020-06-11 16:18:35', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('692', '27', '音乐', '690', '2020-06-11 16:18:35', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('693', '27', '视频', '690', '2020-06-11 16:18:35', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('694', '27', '文档', '690', '2020-06-11 16:18:35', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('695', '27', '我的文件', '689', '2020-06-11 16:18:35', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('696', '27', '图片', '695', '2020-06-11 16:18:35', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('697', '27', '音乐', '695', '2020-06-11 16:18:35', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('698', '27', '视频', '695', '2020-06-11 16:18:35', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('699', '27', '文档', '695', '2020-06-11 16:18:35', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('700', '27', '随笔', '689', '2020-06-11 16:18:35', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('701', '27', '随笔', '577', '2020-06-11 16:19:16', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('702', '27', '我的文件', '701', '2020-06-11 16:19:17', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('703', '27', '图片', '702', '2020-06-11 16:19:17', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('704', '27', '音乐', '702', '2020-06-11 16:19:17', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('705', '27', '视频', '702', '2020-06-11 16:19:17', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('706', '27', '文档', '702', '2020-06-11 16:19:17', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('707', '27', '我的文件', '701', '2020-06-11 16:19:17', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('708', '27', '图片', '707', '2020-06-11 16:19:17', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('709', '27', '音乐', '707', '2020-06-11 16:19:17', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('710', '27', '视频', '707', '2020-06-11 16:19:17', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('711', '27', '文档', '707', '2020-06-11 16:19:17', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('712', '27', '随笔', '701', '2020-06-11 16:19:17', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('713', '27', '随笔', '577', '2020-06-11 16:19:21', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('714', '27', '我的文件', '713', '2020-06-11 16:19:21', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('715', '27', '图片', '714', '2020-06-11 16:19:21', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('716', '27', '音乐', '714', '2020-06-11 16:19:21', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('717', '27', '视频', '714', '2020-06-11 16:19:21', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('718', '27', '文档', '714', '2020-06-11 16:19:21', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('719', '27', '我的文件', '713', '2020-06-11 16:19:21', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('720', '27', '图片', '719', '2020-06-11 16:19:21', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('721', '27', '音乐', '719', '2020-06-11 16:19:21', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('722', '27', '视频', '719', '2020-06-11 16:19:21', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('723', '27', '文档', '719', '2020-06-11 16:19:21', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('724', '27', '随笔', '713', '2020-06-11 16:19:21', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('725', '27', '随笔', '577', '2020-06-11 16:19:38', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('726', '27', '我的文件', '725', '2020-06-11 16:19:38', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('727', '27', '图片', '726', '2020-06-11 16:19:38', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('728', '27', '音乐', '726', '2020-06-11 16:19:38', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('729', '27', '视频', '726', '2020-06-11 16:19:38', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('730', '27', '文档', '726', '2020-06-11 16:19:38', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('731', '27', '我的文件', '725', '2020-06-11 16:19:38', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('732', '27', '图片', '731', '2020-06-11 16:19:38', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('733', '27', '音乐', '731', '2020-06-11 16:19:38', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('734', '27', '视频', '731', '2020-06-11 16:19:38', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('735', '27', '文档', '731', '2020-06-11 16:19:38', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('736', '27', '随笔', '725', '2020-06-11 16:19:38', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('737', '27', '我的文件2', '0', '2020-06-12 16:40:52', '2020-06-12 16:40:52', '0', null, null);
INSERT INTO `user_directory` VALUES ('738', '27', '我的文件', '737', '2020-06-11 16:20:29', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('739', '27', '图片', '738', '2020-06-11 16:20:29', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('740', '27', '音乐', '738', '2020-06-11 16:20:29', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('741', '27', '视频', '738', '2020-06-11 16:20:29', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('742', '27', '文档', '738', '2020-06-11 16:20:29', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('743', '27', '随笔', '738', '2020-06-11 16:20:29', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('744', '27', '我的文件', '743', '2020-06-11 16:20:29', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('745', '27', '图片', '744', '2020-06-11 16:20:29', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('746', '27', '音乐', '744', '2020-06-11 16:20:29', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('747', '27', '视频', '744', '2020-06-11 16:20:29', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('748', '27', '文档', '744', '2020-06-11 16:20:29', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('749', '27', '我的文件', '743', '2020-06-11 16:20:29', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('750', '27', '图片', '749', '2020-06-11 16:20:29', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('751', '27', '音乐', '749', '2020-06-11 16:20:29', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('752', '27', '视频', '749', '2020-06-11 16:20:29', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('753', '27', '文档', '749', '2020-06-11 16:20:29', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('754', '27', '随笔', '743', '2020-06-11 16:20:29', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('755', '27', '随笔', '738', '2020-06-11 16:20:29', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('756', '27', '我的文件', '755', '2020-06-11 16:20:29', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('757', '27', '图片', '756', '2020-06-11 16:20:29', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('758', '27', '音乐', '756', '2020-06-11 16:20:29', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('759', '27', '视频', '756', '2020-06-11 16:20:29', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('760', '27', '文档', '756', '2020-06-11 16:20:29', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('761', '27', '我的文件', '755', '2020-06-11 16:20:29', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('762', '27', '图片', '761', '2020-06-11 16:20:29', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('763', '27', '音乐', '761', '2020-06-11 16:20:29', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('764', '27', '视频', '761', '2020-06-11 16:20:29', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('765', '27', '文档', '761', '2020-06-11 16:20:29', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('766', '27', '随笔', '755', '2020-06-11 16:20:29', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('767', '27', '随笔', '738', '2020-06-11 16:20:29', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('768', '27', '我的文件', '767', '2020-06-11 16:20:29', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('769', '27', '图片', '768', '2020-06-11 16:20:29', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('770', '27', '音乐', '768', '2020-06-11 16:20:29', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('771', '27', '视频', '768', '2020-06-11 16:20:29', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('772', '27', '文档', '768', '2020-06-11 16:20:29', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('773', '27', '我的文件', '767', '2020-06-11 16:20:29', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('774', '27', '图片', '773', '2020-06-11 16:20:29', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('775', '27', '音乐', '773', '2020-06-11 16:20:29', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('776', '27', '视频', '773', '2020-06-11 16:20:29', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('777', '27', '文档', '773', '2020-06-11 16:20:29', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('778', '27', '随笔', '767', '2020-06-11 16:20:29', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('779', '27', '随笔', '738', '2020-06-11 16:20:29', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('780', '27', '我的文件', '779', '2020-06-11 16:20:29', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('781', '27', '图片', '780', '2020-06-11 16:20:29', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('782', '27', '音乐', '780', '2020-06-11 16:20:29', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('783', '27', '视频', '780', '2020-06-11 16:20:29', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('784', '27', '文档', '780', '2020-06-11 16:20:29', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('785', '27', '我的文件', '779', '2020-06-11 16:20:29', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('786', '27', '图片', '785', '2020-06-11 16:20:29', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('787', '27', '音乐', '785', '2020-06-11 16:20:29', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('788', '27', '视频', '785', '2020-06-11 16:20:29', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('789', '27', '文档', '785', '2020-06-11 16:20:29', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('790', '27', '随笔', '779', '2020-06-11 16:20:29', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('791', '27', '我的文件', '737', '2020-06-11 16:21:18', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('792', '27', '图片', '791', '2020-06-11 16:21:18', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('793', '27', '音乐', '791', '2020-06-11 16:21:18', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('794', '27', '视频', '791', '2020-06-11 16:21:18', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('795', '27', '文档', '791', '2020-06-11 16:21:18', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('796', '27', '随笔', '791', '2020-06-11 16:21:18', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('797', '27', '我的文件', '796', '2020-06-11 16:21:18', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('798', '27', '图片', '797', '2020-06-11 16:21:18', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('799', '27', '音乐', '797', '2020-06-11 16:21:18', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('800', '27', '视频', '797', '2020-06-11 16:21:18', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('801', '27', '文档', '797', '2020-06-11 16:21:18', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('802', '27', '我的文件', '796', '2020-06-11 16:21:18', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('803', '27', '图片', '802', '2020-06-11 16:21:18', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('804', '27', '音乐', '802', '2020-06-11 16:21:18', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('805', '27', '视频', '802', '2020-06-11 16:21:18', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('806', '27', '文档', '802', '2020-06-11 16:21:18', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('807', '27', '随笔', '796', '2020-06-11 16:21:18', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('808', '27', '随笔', '791', '2020-06-11 16:21:18', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('809', '27', '我的文件', '808', '2020-06-11 16:21:18', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('810', '27', '图片', '809', '2020-06-11 16:21:18', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('811', '27', '音乐', '809', '2020-06-11 16:21:18', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('812', '27', '视频', '809', '2020-06-11 16:21:18', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('813', '27', '文档', '809', '2020-06-11 16:21:18', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('814', '27', '我的文件', '808', '2020-06-11 16:21:18', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('815', '27', '图片', '814', '2020-06-11 16:21:18', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('816', '27', '音乐', '814', '2020-06-11 16:21:18', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('817', '27', '视频', '814', '2020-06-11 16:21:18', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('818', '27', '文档', '814', '2020-06-11 16:21:18', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('819', '27', '随笔', '808', '2020-06-11 16:21:18', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('820', '27', '随笔', '791', '2020-06-11 16:21:18', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('821', '27', '我的文件', '820', '2020-06-11 16:21:18', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('822', '27', '图片', '821', '2020-06-11 16:21:18', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('823', '27', '音乐', '821', '2020-06-11 16:21:18', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('824', '27', '视频', '821', '2020-06-11 16:21:18', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('825', '27', '文档', '821', '2020-06-11 16:21:18', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('826', '27', '我的文件', '820', '2020-06-11 16:21:18', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('827', '27', '图片', '826', '2020-06-11 16:21:18', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('828', '27', '音乐', '826', '2020-06-11 16:21:18', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('829', '27', '视频', '826', '2020-06-11 16:21:18', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('830', '27', '文档', '826', '2020-06-11 16:21:18', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('831', '27', '随笔', '820', '2020-06-11 16:21:18', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('832', '27', '随笔', '791', '2020-06-11 16:21:18', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('833', '27', '我的文件', '832', '2020-06-11 16:21:18', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('834', '27', '图片', '833', '2020-06-11 16:21:18', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('835', '27', '音乐', '833', '2020-06-11 16:21:18', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('836', '27', '视频', '833', '2020-06-11 16:21:18', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('837', '27', '文档', '833', '2020-06-11 16:21:18', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('838', '27', '我的文件', '832', '2020-06-11 16:21:18', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('839', '27', '图片', '838', '2020-06-11 16:21:18', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('840', '27', '音乐', '838', '2020-06-11 16:21:18', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('841', '27', '视频', '838', '2020-06-11 16:21:18', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('842', '27', '文档', '838', '2020-06-11 16:21:18', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('843', '27', '随笔', '832', '2020-06-11 16:21:18', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('844', '27', '我的文件', '737', '2020-06-11 16:21:45', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('845', '27', '图片', '844', '2020-06-11 16:21:45', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('846', '27', '音乐', '844', '2020-06-11 16:21:45', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('847', '27', '视频', '844', '2020-06-11 16:21:45', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('848', '27', '文档', '844', '2020-06-11 16:21:45', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('849', '27', '随笔', '844', '2020-06-11 16:21:45', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('850', '27', '我的文件', '849', '2020-06-11 16:21:45', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('851', '27', '图片', '850', '2020-06-11 16:21:45', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('852', '27', '音乐', '850', '2020-06-11 16:21:45', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('853', '27', '视频', '850', '2020-06-11 16:21:45', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('854', '27', '文档', '850', '2020-06-11 16:21:45', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('855', '27', '我的文件', '849', '2020-06-11 16:21:45', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('856', '27', '图片', '855', '2020-06-11 16:21:45', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('857', '27', '音乐', '855', '2020-06-11 16:21:45', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('858', '27', '视频', '855', '2020-06-11 16:21:45', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('859', '27', '文档', '855', '2020-06-11 16:21:45', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('860', '27', '随笔', '849', '2020-06-11 16:21:45', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('861', '27', '随笔', '844', '2020-06-11 16:21:45', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('862', '27', '我的文件', '861', '2020-06-11 16:21:45', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('863', '27', '图片', '862', '2020-06-11 16:21:45', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('864', '27', '音乐', '862', '2020-06-11 16:21:45', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('865', '27', '视频', '862', '2020-06-11 16:21:45', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('866', '27', '文档', '862', '2020-06-11 16:21:45', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('867', '27', '我的文件', '861', '2020-06-11 16:21:45', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('868', '27', '图片', '867', '2020-06-11 16:21:45', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('869', '27', '音乐', '867', '2020-06-11 16:21:45', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('870', '27', '视频', '867', '2020-06-11 16:21:45', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('871', '27', '文档', '867', '2020-06-11 16:21:45', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('872', '27', '随笔', '861', '2020-06-11 16:21:45', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('873', '27', '随笔', '844', '2020-06-11 16:21:45', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('874', '27', '我的文件', '873', '2020-06-11 16:21:45', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('875', '27', '图片', '874', '2020-06-11 16:21:45', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('876', '27', '音乐', '874', '2020-06-11 16:21:45', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('877', '27', '视频', '874', '2020-06-11 16:21:45', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('878', '27', '文档', '874', '2020-06-11 16:21:45', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('879', '27', '我的文件', '873', '2020-06-11 16:21:45', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('880', '27', '图片', '879', '2020-06-11 16:21:45', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('881', '27', '音乐', '879', '2020-06-11 16:21:45', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('882', '27', '视频', '879', '2020-06-11 16:21:45', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('883', '27', '文档', '879', '2020-06-11 16:21:45', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('884', '27', '随笔', '873', '2020-06-11 16:21:45', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('885', '27', '随笔', '844', '2020-06-11 16:21:45', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('886', '27', '我的文件', '885', '2020-06-11 16:21:45', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('887', '27', '图片', '886', '2020-06-11 16:21:45', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('888', '27', '音乐', '886', '2020-06-11 16:21:45', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('889', '27', '视频', '886', '2020-06-11 16:21:45', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('890', '27', '文档', '886', '2020-06-11 16:21:45', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('891', '27', '我的文件', '885', '2020-06-11 16:21:45', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('892', '27', '图片', '891', '2020-06-11 16:21:45', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('893', '27', '音乐', '891', '2020-06-11 16:21:45', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('894', '27', '视频', '891', '2020-06-11 16:21:45', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('895', '27', '文档', '891', '2020-06-11 16:21:45', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('896', '27', '随笔', '885', '2020-06-11 16:21:45', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('897', '27', '随便', '0', '2020-06-11 16:50:09', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('898', '27', '随便', '0', '2020-06-12 16:40:52', '2020-06-12 16:40:52', '0', null, null);
INSERT INTO `user_directory` VALUES ('899', '27', '新建文件夹', '0', '2020-06-12 16:40:52', '2020-06-12 16:40:52', '0', null, null);
INSERT INTO `user_directory` VALUES ('900', '27', '随便', '899', '2020-06-11 16:56:54', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('901', '27', '随便', '899', '2020-06-11 16:56:55', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('902', '27', '复制', '0', '2020-06-12 16:40:52', '2020-06-12 16:40:52', '0', null, null);
INSERT INTO `user_directory` VALUES ('903', '27', '随便', '902', '2020-06-11 17:04:55', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('904', '27', '随便', '902', '2020-06-11 17:05:19', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('905', '27', '随便', '902', '2020-06-11 17:06:32', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('906', '27', '随便', '902', '2020-06-11 17:21:17', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('907', '27', '随便', '902', '2020-06-11 17:46:59', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('908', '27', '随便', '902', '2020-06-11 18:33:55', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('909', '27', '随便', '902', '2020-06-11 18:35:51', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('910', '27', '随便', '902', '2020-06-11 18:37:24', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('911', '27', '随便', '902', '2020-06-11 18:38:07', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('912', '27', '随便', '902', '2020-06-11 18:39:08', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('913', '27', '随便', '902', '2020-06-11 18:42:34', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('914', '27', '随便', '902', '2020-06-11 18:43:46', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('915', '27', '随便', '902', '2020-06-11 18:56:19', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('916', '27', '随便', '902', '2020-06-11 19:01:37', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('917', '27', '随便', '902', '2020-06-11 19:06:02', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('918', '27', '随便', '902', '2020-06-11 19:06:30', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('919', '27', '小黄', '0', '2020-06-11 19:07:23', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('920', '27', '新建文件夹', '0', '2020-06-11 19:08:31', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('921', '27', '随便', '920', '2020-06-11 19:09:16', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('922', '27', '小黄', '920', '2020-06-11 19:09:17', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('923', '27', '随便', '920', '2020-06-11 19:13:03', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('924', '27', '随便', '920', '2020-06-11 19:13:35', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('925', '27', '小黄', '920', '2020-06-11 19:13:36', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('926', '27', '随便', '920', '2020-06-11 19:18:05', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('927', '27', '小黄', '920', '2020-06-11 19:18:07', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('928', '27', '随便', '920', '2020-06-11 19:19:19', null, '2', null, null);
INSERT INTO `user_directory` VALUES ('929', '27', '小黄', '920', '2020-06-11 19:19:20', null, '2', null, null);

-- ----------------------------
-- Table structure for user_file
-- ----------------------------
DROP TABLE IF EXISTS `user_file`;
CREATE TABLE `user_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `file_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '文件名',
  `path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '路径（实际）',
  `user_directory_id` int(11) DEFAULT NULL COMMENT '文件夹id',
  `size` double DEFAULT NULL COMMENT '大小（单位：kb）',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `type` int(11) DEFAULT NULL COMMENT '0为其他，1为图片，2位音频，3为视频，4位文档',
  `overdue_time` datetime DEFAULT NULL COMMENT '过期时间',
  `status` int(11) DEFAULT '2' COMMENT '0为已删，1为回收站，2为正常，3为不合法',
  `file_md5` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '文件的md5（每个文件有且只有一个md5）',
  `spare2` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备用2',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1031 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_file
-- ----------------------------
INSERT INTO `user_file` VALUES ('39', '2', 'fileImg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/no85-05.jpg', '32', '4569', '2020-05-14 10:46:22', '1', '2020-05-12 10:48:47', '2', null, null);
INSERT INTO `user_file` VALUES ('41', '2', '小姐姐', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/no85-05.jpg', '0', '4645', '2020-06-09 16:03:11', '1', '2020-06-09 16:03:11', '0', '', '不合格');
INSERT INTO `user_file` VALUES ('42', '2', '小姐姐', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/no85-05.jpg', '1', '4566', '2020-06-01 12:04:51', '1', '2020-06-19 13:40:40', '2', null, null);
INSERT INTO `user_file` VALUES ('53', '2', '图片1', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/no85-05.jpg', '1', '2341', '2020-06-03 17:56:39', '1', '2020-06-08 17:25:19', '2', null, null);
INSERT INTO `user_file` VALUES ('54', '2', '美女', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/no85-05.jpg', '1', '3048', '2020-05-04 15:44:50', '1', '2020-05-28 17:04:26', '2', null, null);
INSERT INTO `user_file` VALUES ('55', '2', '美女哟', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/no85-05.jpg', '1', '3048', '2020-06-04 21:16:20', '1', '2020-06-04 21:16:20', '2', '', null);
INSERT INTO `user_file` VALUES ('60', '1', 'fileImg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/no85-05.jpg', '5', '4569', '2020-06-04 15:58:54', '1', '2020-05-12 10:48:47', '2', '', '不合格');
INSERT INTO `user_file` VALUES ('62', '1', '国旗', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/no85-05.jpg', '14', '4569', '2020-06-10 19:41:35', '1', '2020-06-10 19:11:09', '2', '', '不合格');
INSERT INTO `user_file` VALUES ('74', '1', 'logo.png', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/logo.png', '198', '15.51171875', '2020-06-04 18:56:26', '1', null, '2', '6c2dc712874e3ccdca676ff4272c3d36', null);
INSERT INTO `user_file` VALUES ('110', '1', 'Xshell-6.0.0125_wm.exe', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/Xshell-6.0.0125_wm.exe', '0', '43573.9', '2020-06-05 16:03:29', '0', null, '0', 'd4694b56c5e62d43a0c16db7821fe34d', null);
INSERT INTO `user_file` VALUES ('188', '2', 'elasticsearch-0-入门及安装.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/elasticsearch-0-入门及安装.mp4', '464', '32142', '2020-06-07 23:53:56', '3', null, '0', 'eeff3961150aa013cbe8bbf58a78969d', null);
INSERT INTO `user_file` VALUES ('220', '2', 'elasticsearch-0-入门及安装.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/elasticsearch-0-入门及安装.mp4', '477', '32142', '2020-06-07 23:38:51', '3', null, '2', null, null);
INSERT INTO `user_file` VALUES ('251', '1', 'elasticsearch-0-入门及安装.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/elasticsearch-0-入门及安装.mp4', '506', '161034.52', '2020-06-08 00:22:47', '3', null, '2', 'eeff3961150aa013cbe8bbf58a78969d', null);
INSERT INTO `user_file` VALUES ('252', '1', 'elasticsearch-0-入门及安装.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/elasticsearch-0-入门及安装.mp4', '494', '161034.513671875', '2020-06-06 19:36:17', '3', null, '2', 'eeff3961150aa013cbe8bbf58a78969d', null);
INSERT INTO `user_file` VALUES ('254', '1', 'elasticsearch-0-入门及安装.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/elasticsearch-0-入门及安装.mp4', '508', '161034.513671875', '2020-06-07 22:23:46', '3', null, '2', 'eeff3961150aa013cbe8bbf58a78969d', null);
INSERT INTO `user_file` VALUES ('255', '1', 'i4Tools_v7.98.15_Setup.exe', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/i4Tools_v7.98.15_Setup.exe', '508', '138362.9296875', '2020-06-07 22:38:31', '0', null, '2', 'e70b9b3956d085dc5e97b0d8b3aa471d', null);
INSERT INTO `user_file` VALUES ('256', '1', '什么.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/什么.mp4', '508', '12783.7568359375', '2020-06-07 23:13:51', '3', null, '2', '4d3aa5431f05489c6f3d86e07ea48c8b', null);
INSERT INTO `user_file` VALUES ('257', '1', 'JM0350.zip', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/JM0350.zip', '508', '1526.8701171875', '2020-06-07 23:20:42', '0', null, '2', '978fcb1503e052cf9aa0ba93b5ddcffb', null);
INSERT INTO `user_file` VALUES ('258', '1', 'index.js', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/1/index.js', '508', '553.865234375', '2020-06-07 23:42:46', '0', null, '2', '683205a61373b1b08d6fefac93619c6e', null);
INSERT INTO `user_file` VALUES ('259', '1', '陈浩简历.pdf', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/1/陈浩简历.pdf', '508', '138.455078125', '2020-06-07 23:52:47', '4', null, '2', 'e0837c219e7bff9483b6e71ed671c8ed', null);
INSERT INTO `user_file` VALUES ('260', '1', '20200518180626.csv', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/1/20200518180626.csv', '508', '0.0869140625', '2020-06-07 23:52:57', '0', null, '2', 'c6e39ea1a316c9589a5f47d802fe1402', null);
INSERT INTO `user_file` VALUES ('297', '1', 'C3P0连接池.zip', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/1/C3P0连接池.zip', '0', '2017.6221', '2020-06-11 15:54:46', '0', null, '0', '49ea5a5810f374cb01bb885eccfae900', '正常');
INSERT INTO `user_file` VALUES ('302', '2', '小姐姐', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/no85-05.jpg', '535', '4645', '2020-06-09 17:03:54', '1', null, '2', null, null);
INSERT INTO `user_file` VALUES ('307', '1', 'qian-cloud-0.0.1-SNAPSHOT.jar', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/1/qian-cloud-0.0.1-SNAPSHOT.jar', '0', '115848.8', '2020-06-10 17:37:09', '0', null, '0', 'd6750749198b346fc51fe40d2d060a83', '正常');
INSERT INTO `user_file` VALUES ('308', '1', 'qian-cloud-0.0.1-SNAPSHOT.jar', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/1/qian-cloud-0.0.1-SNAPSHOT.jar', '513', '115848.7978515625', '2020-06-08 21:55:16', '0', null, '2', 'd6750749198b346fc51fe40d2d060a83', null);
INSERT INTO `user_file` VALUES ('309', '1', 'LoginLogsController.java', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/1/LoginLogsController.java', '0', '1.6943359', '2020-06-10 17:37:09', '4', null, '0', 'eea2e47a6107f79ffeebe6442661bead', '正常');
INSERT INTO `user_file` VALUES ('310', '1', 'VipController.java', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/1/VipController.java', '0', '10.775391', '2020-06-11 15:54:46', '4', null, '0', 'fc20247b6a058801f888dba28a8af448', '正常');
INSERT INTO `user_file` VALUES ('311', '1', 'ChenFrendsService.java', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/1/ChenFrendsService.java', '0', '1.9492188', '2020-06-10 17:37:09', '4', null, '0', '35309473246f361a3499b68f794d5d0e', '正常');
INSERT INTO `user_file` VALUES ('312', '1', 'VipQueryService.java', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/1/VipQueryService.java', '0', '0.60058594', '2020-06-10 17:37:09', '4', null, '0', '89edf263597f4cd393c968bad9609125', '正常');
INSERT INTO `user_file` VALUES ('317', '1', 'fileImg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/no85-05.jpg', '0', '4569', '2020-06-09 12:02:35', '1', '2020-05-12 10:48:47', '2', null, '不合格');
INSERT INTO `user_file` VALUES ('318', '1', 'fileImg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/no85-05.jpg', '0', '4569', '2020-06-10 17:37:09', '1', '2020-06-10 17:37:09', '0', null, '正常');
INSERT INTO `user_file` VALUES ('322', '1', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/1/Kalimba.mp3', '0', '8217.235', '2020-06-10 19:49:47', '2', null, '2', '5a9a91184e611dae3fed162b8787ce5f', '正常');
INSERT INTO `user_file` VALUES ('324', '1', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/1/video.mp4', '0', '12783.757', '2020-06-11 15:54:46', '3', null, '0', '4d3aa5431f05489c6f3d86e07ea48c8b', '正常');
INSERT INTO `user_file` VALUES ('326', '1', '问题30.txt', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/1/问题30.txt', '310', '1.4912109375', '2020-06-10 15:53:42', '4', null, '2', '415851cb3549bce9af34434cdb93be31', '正常');
INSERT INTO `user_file` VALUES ('327', '1', 'Firefox-latest.exe', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/1/Firefox-latest.exe', '0', '368.8672', '2020-06-11 15:54:46', '0', null, '0', 'c5ff94a9fefaf9f12fc50c0d90b03d97', '正常');
INSERT INTO `user_file` VALUES ('332', '1', 'i4Tools_v7.98.15_Setup.exe', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/1/i4Tools_v7.98.15_Setup.exe', '0', '138362.94', '2020-06-11 15:54:46', '0', null, '0', 'e70b9b3956d085dc5e97b0d8b3aa471d', '正常');
INSERT INTO `user_file` VALUES ('352', '1', 'elasticsearch-2-spring-boot集成elasticsearch.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/1/elasticsearch-2-spring-boot集成elasticsearch.mp4', '0', '161034.52', '2020-06-11 15:54:46', '3', null, '0', 'eeff3961150aa013cbe8bbf58a78969d', '正常');
INSERT INTO `user_file` VALUES ('355', '1', 'Illegal.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/1/Illegal.jpg', '0', '16.1123046875', '2020-06-10 21:47:12', '1', null, '2', '6656e447c10a979fc2c1dff14f372744', '正常');
INSERT INTO `user_file` VALUES ('356', '1', 'wangpan-master.zip', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/1/wangpan-master.zip', '0', '786.0341796875', '2020-06-10 22:44:58', '0', null, '2', '76000b167da72f6f5152f6adf8c427a2', '正常');
INSERT INTO `user_file` VALUES ('357', '2', 'elasticsearch-2-spring-boot集成elasticsearch.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/2/elasticsearch-2-spring-boot集成elasticsearch.mp4', '0', '161034.513671875', '2020-06-11 09:10:40', '3', null, '2', 'eeff3961150aa013cbe8bbf58a78969d', '正常');
INSERT INTO `user_file` VALUES ('358', '27', 'headImg3.jpeg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/headImg3.jpeg', '0', '51.3076171875', '2020-06-11 09:27:46', '1', null, '2', '108f9419e8de534da010052bf52debb5', '正常');
INSERT INTO `user_file` VALUES ('359', '2', 'i4Tools_v7.98.15_Setup.exe', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/2/i4Tools_v7.98.15_Setup.exe', '0', '138362.9296875', '2020-06-11 09:46:08', '0', null, '2', 'e70b9b3956d085dc5e97b0d8b3aa471d', '正常');
INSERT INTO `user_file` VALUES ('360', '2', 'index.js', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/2/index.js', '437', '553.865234375', '2020-06-11 09:46:32', '0', null, '2', '683205a61373b1b08d6fefac93619c6e', '正常');
INSERT INTO `user_file` VALUES ('361', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '0', '8217.2353515625', '2020-06-11 10:20:13', '2', null, '2', '5a9a91184e611dae3fed162b8787ce5f', '正常');
INSERT INTO `user_file` VALUES ('362', '26', '常用css属性.html', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/26/常用css属性.html', '0', '3.126953125', '2020-06-11 13:16:03', '4', null, '2', 'a2fbbf7adba4ade6e2dfba88a5ee04ab', '正常');
INSERT INTO `user_file` VALUES ('363', '3', 'headImg3.jpeg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/headImg3.jpeg', '576', '51.3076171875', '2020-06-11 13:18:20', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('364', '3', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '576', '8217.2353515625', '2020-06-11 13:18:20', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('365', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '0', '12783.7568359375', '2020-06-11 13:21:38', '3', null, '2', '4d3aa5431f05489c6f3d86e07ea48c8b', '正常');
INSERT INTO `user_file` VALUES ('366', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '0', '14.7353515625', '2020-06-11 13:23:20', '4', null, '2', '17dbfb8a1a75422f711cafdb591c4ec0', '正常');
INSERT INTO `user_file` VALUES ('367', '27', 'Jellyfish.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Jellyfish.jpg', '578', '128.4267578125', '2020-06-11 13:24:19', '1', null, '2', '5a44c7ba5bbe4ec867233d67e4806848', '正常');
INSERT INTO `user_file` VALUES ('368', '27', 'Lighthouse.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Lighthouse.jpg', '578', '164.98828125', '2020-06-11 13:24:28', '1', null, '2', '8969288f4245120e7c3870287cce0ff3', '正常');
INSERT INTO `user_file` VALUES ('369', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '579', '8217.2353515625', '2020-06-11 13:25:14', '2', null, '2', '5a9a91184e611dae3fed162b8787ce5f', '正常');
INSERT INTO `user_file` VALUES ('370', '27', 'Maid with the Flaxen Hair.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Maid with the Flaxen Hair.mp3', '579', '4729.0869140625', '2020-06-11 13:26:10', '2', null, '2', 'b8b5bd53e23dd40b5c4a650e272f2e19', '正常');
INSERT INTO `user_file` VALUES ('371', '27', 'Sleep Away.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Sleep Away.mp3', '579', '4729.0869140625', '2020-06-11 13:26:34', '2', null, '2', 'b8b5bd53e23dd40b5c4a650e272f2e19', '正常');
INSERT INTO `user_file` VALUES ('372', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '580', '12783.7568359375', '2020-06-11 13:27:08', '3', null, '2', '4d3aa5431f05489c6f3d86e07ea48c8b', '正常');
INSERT INTO `user_file` VALUES ('373', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '581', '14.7353515625', '2020-06-11 13:27:37', '4', null, '2', '17dbfb8a1a75422f711cafdb591c4ec0', '正常');
INSERT INTO `user_file` VALUES ('374', '27', '问题30.txt', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/问题30.txt', '581', '1.4912109375', '2020-06-11 13:27:53', '4', null, '2', '415851cb3549bce9af34434cdb93be31', '正常');
INSERT INTO `user_file` VALUES ('375', '27', 'wechat_devtools_1.02.2004020_x64.exe', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/wechat_devtools_1.02.2004020_x64.exe', '0', '132546.75', '2020-06-11 15:54:46', '0', null, '0', 'a4a3fd0f851d9a1afc66852ad6b0eb25', '正常');
INSERT INTO `user_file` VALUES ('376', '27', 'elasticsearch-2-spring-boot集成elasticsearch.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/2/elasticsearch-2-spring-boot集成elasticsearch.mp4', '583', '161034.513671875', '2020-06-11 13:50:13', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('377', '27', 'i4Tools_v7.98.15_Setup.exe', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/2/i4Tools_v7.98.15_Setup.exe', '583', '138362.9296875', '2020-06-11 13:50:13', '6', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('378', '27', 'elasticsearch-2-spring-boot集成elasticsearch.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/2/elasticsearch-2-spring-boot集成elasticsearch.mp4', '584', '161034.513671875', '2020-06-11 13:50:58', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('379', '27', 'i4Tools_v7.98.15_Setup.exe', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/2/i4Tools_v7.98.15_Setup.exe', '584', '138362.9296875', '2020-06-11 13:50:58', '6', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('380', '27', 'Git-2.26.0-64-bit.exe', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Git-2.26.0-64-bit.exe', '0', '45575.3828125', '2020-06-11 13:55:06', '0', null, '2', 'ae0805558a1614fa54d02ccbd5d56a69', '正常');
INSERT INTO `user_file` VALUES ('381', '1', 'fileImg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/no85-05.jpg', '14', '4569', '2020-06-11 14:47:00', '1', '2020-05-12 10:48:47', '2', null, '不合格');
INSERT INTO `user_file` VALUES ('382', '1', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/1/Kalimba.mp3', '14', '8217.235', '2020-06-11 14:47:01', '2', null, '2', '5a9a91184e611dae3fed162b8787ce5f', '正常');
INSERT INTO `user_file` VALUES ('383', '1', 'fileImg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/no85-05.jpg', '573', '4569', '2020-06-11 14:48:22', '1', '2020-05-12 10:48:47', '2', null, '不合格');
INSERT INTO `user_file` VALUES ('384', '1', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/1/Kalimba.mp3', '573', '8217.235', '2020-06-11 14:48:23', '2', null, '2', '5a9a91184e611dae3fed162b8787ce5f', '正常');
INSERT INTO `user_file` VALUES ('385', '1', 'FoxmailSetup_7.2.16.188.exe', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/1/FoxmailSetup_7.2.16.188.exe', '0', '44240.0859375', '2020-06-11 14:52:04', '0', null, '2', '2958595dad48a6e9786bfb6bd9b48e3c', '正常');
INSERT INTO `user_file` VALUES ('386', '1', 'GeoLite2-City.mmdb', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/1/GeoLite2-City.mmdb', '0', '61278.712890625', '2020-06-11 14:54:49', '0', null, '2', '88d49b59348ebfdc157a208c11d568e8', '正常');
INSERT INTO `user_file` VALUES ('387', '1', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/1/Kalimba.mp3', '14', '8217.235', '2020-06-11 14:56:17', '2', null, '2', '5a9a91184e611dae3fed162b8787ce5f', '正常');
INSERT INTO `user_file` VALUES ('388', '28', 'headImg3.jpeg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/28/headImg3.jpeg', '585', '51.3076171875', '2020-06-11 15:53:57', '1', null, '2', '108f9419e8de534da010052bf52debb5', '正常');
INSERT INTO `user_file` VALUES ('389', '28', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/28/video.mp4', '0', '12783.7568359375', '2020-06-11 15:48:38', '3', null, '2', '4d3aa5431f05489c6f3d86e07ea48c8b', '正常');
INSERT INTO `user_file` VALUES ('390', '28', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/28/Kalimba.mp3', '388', '8217.2353515625', '2020-06-11 15:50:24', '2', null, '2', '5a9a91184e611dae3fed162b8787ce5f', '正常');
INSERT INTO `user_file` VALUES ('391', '28', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/28/简历.docx', '0', '14.735352', '2020-06-11 15:54:37', '4', null, '2', '17dbfb8a1a75422f711cafdb591c4ec0', '正常');
INSERT INTO `user_file` VALUES ('392', '28', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/28/video.mp4', '586', '12783.7568359375', '2020-06-11 15:52:52', '3', null, '2', '4d3aa5431f05489c6f3d86e07ea48c8b', '正常');
INSERT INTO `user_file` VALUES ('393', '28', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/28/简历.docx', '586', '14.7353515625', '2020-06-11 15:52:53', '4', null, '2', '17dbfb8a1a75422f711cafdb591c4ec0', '正常');
INSERT INTO `user_file` VALUES ('394', '28', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/28/video.mp4', '588', '12783.7568359375', '2020-06-11 15:53:31', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('395', '28', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/28/简历.docx', '588', '14.7353515625', '2020-06-11 15:53:31', '5', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('396', '2', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/28/video.mp4', '437', '12783.7568359375', '2020-06-11 15:57:36', '3', null, '2', '4d3aa5431f05489c6f3d86e07ea48c8b', '正常');
INSERT INTO `user_file` VALUES ('397', '2', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/28/简历.docx', '437', '14.735352', '2020-06-11 15:57:37', '4', null, '2', '17dbfb8a1a75422f711cafdb591c4ec0', '正常');
INSERT INTO `user_file` VALUES ('398', '28', 'headImg3.jpeg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/headImg3.jpeg', '589', '51.3076171875', '2020-06-11 16:06:31', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('399', '28', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '589', '8217.2353515625', '2020-06-11 16:06:31', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('400', '28', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '589', '12783.7568359375', '2020-06-11 16:06:31', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('401', '27', 'Jellyfish.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Jellyfish.jpg', '592', '128.4267578125', '2020-06-11 16:15:03', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('402', '27', 'Lighthouse.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Lighthouse.jpg', '592', '164.98828125', '2020-06-11 16:15:03', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('403', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '593', '8217.2353515625', '2020-06-11 16:15:03', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('404', '27', 'Maid with the Flaxen Hair.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Maid with the Flaxen Hair.mp3', '593', '4729.0869140625', '2020-06-11 16:15:03', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('405', '27', 'Sleep Away.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Sleep Away.mp3', '593', '4729.0869140625', '2020-06-11 16:15:03', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('406', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '594', '12783.7568359375', '2020-06-11 16:15:03', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('407', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '595', '14.7353515625', '2020-06-11 16:15:03', '5', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('408', '27', '问题30.txt', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/问题30.txt', '595', '1.4912109375', '2020-06-11 16:15:03', '5', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('409', '27', 'headImg3.jpeg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/headImg3.jpeg', '590', '51.3076171875', '2020-06-11 16:15:04', '1', null, '2', '108f9419e8de534da010052bf52debb5', '正常');
INSERT INTO `user_file` VALUES ('410', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '590', '8217.2353515625', '2020-06-11 16:15:05', '2', null, '2', '5a9a91184e611dae3fed162b8787ce5f', '正常');
INSERT INTO `user_file` VALUES ('411', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '590', '12783.7568359375', '2020-06-11 16:15:06', '3', null, '2', '4d3aa5431f05489c6f3d86e07ea48c8b', '正常');
INSERT INTO `user_file` VALUES ('412', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '590', '14.7353515625', '2020-06-11 16:15:07', '4', null, '2', '17dbfb8a1a75422f711cafdb591c4ec0', '正常');
INSERT INTO `user_file` VALUES ('413', '27', 'Git-2.26.0-64-bit.exe', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Git-2.26.0-64-bit.exe', '590', '45575.3828125', '2020-06-11 16:15:08', '0', null, '2', 'ae0805558a1614fa54d02ccbd5d56a69', '正常');
INSERT INTO `user_file` VALUES ('414', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '590', '8217.2353515625', '2020-06-11 16:15:50', '2', null, '2', '5a9a91184e611dae3fed162b8787ce5f', '正常');
INSERT INTO `user_file` VALUES ('415', '27', 'Jellyfish.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Jellyfish.jpg', '597', '128.4267578125', '2020-06-11 16:16:03', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('416', '27', 'Lighthouse.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Lighthouse.jpg', '597', '164.98828125', '2020-06-11 16:16:03', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('417', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '598', '8217.2353515625', '2020-06-11 16:16:03', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('418', '27', 'Maid with the Flaxen Hair.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Maid with the Flaxen Hair.mp3', '598', '4729.0869140625', '2020-06-11 16:16:03', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('419', '27', 'Sleep Away.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Sleep Away.mp3', '598', '4729.0869140625', '2020-06-11 16:16:03', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('420', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '599', '12783.7568359375', '2020-06-11 16:16:03', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('421', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '600', '14.7353515625', '2020-06-11 16:16:03', '5', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('422', '27', '问题30.txt', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/问题30.txt', '600', '1.4912109375', '2020-06-11 16:16:03', '5', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('549', '27', 'Jellyfish.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Jellyfish.jpg', '691', '128.4267578125', '2020-06-11 16:18:35', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('550', '27', 'Lighthouse.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Lighthouse.jpg', '691', '164.98828125', '2020-06-11 16:18:35', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('551', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '692', '8217.2353515625', '2020-06-11 16:18:35', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('552', '27', 'Maid with the Flaxen Hair.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Maid with the Flaxen Hair.mp3', '692', '4729.0869140625', '2020-06-11 16:18:35', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('553', '27', 'Sleep Away.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Sleep Away.mp3', '692', '4729.0869140625', '2020-06-11 16:18:35', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('554', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '693', '12783.7568359375', '2020-06-11 16:18:35', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('555', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '694', '14.7353515625', '2020-06-11 16:18:35', '0', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('556', '27', '问题30.txt', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/问题30.txt', '694', '1.4912109375', '2020-06-11 16:18:35', '0', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('557', '27', 'Jellyfish.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Jellyfish.jpg', '696', '128.4267578125', '2020-06-11 16:18:35', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('558', '27', 'Lighthouse.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Lighthouse.jpg', '696', '164.98828125', '2020-06-11 16:18:35', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('559', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '697', '8217.2353515625', '2020-06-11 16:18:35', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('560', '27', 'Maid with the Flaxen Hair.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Maid with the Flaxen Hair.mp3', '697', '4729.0869140625', '2020-06-11 16:18:35', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('561', '27', 'Sleep Away.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Sleep Away.mp3', '697', '4729.0869140625', '2020-06-11 16:18:35', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('562', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '698', '12783.7568359375', '2020-06-11 16:18:35', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('563', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '699', '14.7353515625', '2020-06-11 16:18:35', '0', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('564', '27', '问题30.txt', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/问题30.txt', '699', '1.4912109375', '2020-06-11 16:18:35', '0', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('565', '27', 'headImg3.jpeg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/headImg3.jpeg', '689', '51.3076171875', '2020-06-11 16:18:35', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('566', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '689', '8217.2353515625', '2020-06-11 16:18:35', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('567', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '689', '12783.7568359375', '2020-06-11 16:18:35', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('568', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '689', '14.7353515625', '2020-06-11 16:18:35', '5', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('569', '27', 'Git-2.26.0-64-bit.exe', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Git-2.26.0-64-bit.exe', '689', '45575.3828125', '2020-06-11 16:18:35', '6', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('570', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '689', '8217.2353515625', '2020-06-11 16:18:35', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('571', '27', 'headImg3.jpeg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/headImg3.jpeg', '577', '51.3076171875', '2020-06-11 16:18:40', '1', null, '2', '108f9419e8de534da010052bf52debb5', '正常');
INSERT INTO `user_file` VALUES ('572', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '577', '8217.2353515625', '2020-06-11 16:18:41', '2', null, '2', '5a9a91184e611dae3fed162b8787ce5f', '正常');
INSERT INTO `user_file` VALUES ('573', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '577', '12783.7568359375', '2020-06-11 16:18:42', '3', null, '2', '4d3aa5431f05489c6f3d86e07ea48c8b', '正常');
INSERT INTO `user_file` VALUES ('574', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '577', '14.7353515625', '2020-06-11 16:18:43', '4', null, '2', '17dbfb8a1a75422f711cafdb591c4ec0', '正常');
INSERT INTO `user_file` VALUES ('575', '27', 'Git-2.26.0-64-bit.exe', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Git-2.26.0-64-bit.exe', '577', '45575.3828125', '2020-06-11 16:18:44', '0', null, '2', 'ae0805558a1614fa54d02ccbd5d56a69', '正常');
INSERT INTO `user_file` VALUES ('576', '27', 'Jellyfish.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Jellyfish.jpg', '703', '128.4267578125', '2020-06-11 16:19:17', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('577', '27', 'Lighthouse.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Lighthouse.jpg', '703', '164.98828125', '2020-06-11 16:19:17', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('578', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '704', '8217.2353515625', '2020-06-11 16:19:17', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('579', '27', 'Maid with the Flaxen Hair.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Maid with the Flaxen Hair.mp3', '704', '4729.0869140625', '2020-06-11 16:19:17', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('580', '27', 'Sleep Away.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Sleep Away.mp3', '704', '4729.0869140625', '2020-06-11 16:19:17', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('581', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '705', '12783.7568359375', '2020-06-11 16:19:17', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('582', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '706', '14.7353515625', '2020-06-11 16:19:17', '0', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('583', '27', '问题30.txt', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/问题30.txt', '706', '1.4912109375', '2020-06-11 16:19:17', '0', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('584', '27', 'Jellyfish.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Jellyfish.jpg', '708', '128.4267578125', '2020-06-11 16:19:17', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('585', '27', 'Lighthouse.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Lighthouse.jpg', '708', '164.98828125', '2020-06-11 16:19:17', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('586', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '709', '8217.2353515625', '2020-06-11 16:19:17', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('587', '27', 'Maid with the Flaxen Hair.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Maid with the Flaxen Hair.mp3', '709', '4729.0869140625', '2020-06-11 16:19:17', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('588', '27', 'Sleep Away.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Sleep Away.mp3', '709', '4729.0869140625', '2020-06-11 16:19:17', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('589', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '710', '12783.7568359375', '2020-06-11 16:19:17', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('590', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '711', '14.7353515625', '2020-06-11 16:19:17', '0', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('591', '27', '问题30.txt', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/问题30.txt', '711', '1.4912109375', '2020-06-11 16:19:17', '0', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('592', '27', 'headImg3.jpeg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/headImg3.jpeg', '701', '51.3076171875', '2020-06-11 16:19:17', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('593', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '701', '8217.2353515625', '2020-06-11 16:19:17', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('594', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '701', '12783.7568359375', '2020-06-11 16:19:17', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('595', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '701', '14.7353515625', '2020-06-11 16:19:17', '5', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('596', '27', 'Git-2.26.0-64-bit.exe', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Git-2.26.0-64-bit.exe', '701', '45575.3828125', '2020-06-11 16:19:17', '6', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('597', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '701', '8217.2353515625', '2020-06-11 16:19:17', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('598', '27', 'Jellyfish.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Jellyfish.jpg', '715', '128.4267578125', '2020-06-11 16:19:21', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('599', '27', 'Lighthouse.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Lighthouse.jpg', '715', '164.98828125', '2020-06-11 16:19:21', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('600', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '716', '8217.2353515625', '2020-06-11 16:19:21', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('601', '27', 'Maid with the Flaxen Hair.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Maid with the Flaxen Hair.mp3', '716', '4729.0869140625', '2020-06-11 16:19:21', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('602', '27', 'Sleep Away.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Sleep Away.mp3', '716', '4729.0869140625', '2020-06-11 16:19:21', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('603', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '717', '12783.7568359375', '2020-06-11 16:19:21', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('604', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '718', '14.7353515625', '2020-06-11 16:19:21', '0', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('605', '27', '问题30.txt', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/问题30.txt', '718', '1.4912109375', '2020-06-11 16:19:21', '0', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('606', '27', 'Jellyfish.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Jellyfish.jpg', '720', '128.4267578125', '2020-06-11 16:19:21', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('607', '27', 'Lighthouse.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Lighthouse.jpg', '720', '164.98828125', '2020-06-11 16:19:21', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('608', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '721', '8217.2353515625', '2020-06-11 16:19:21', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('609', '27', 'Maid with the Flaxen Hair.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Maid with the Flaxen Hair.mp3', '721', '4729.0869140625', '2020-06-11 16:19:21', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('610', '27', 'Sleep Away.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Sleep Away.mp3', '721', '4729.0869140625', '2020-06-11 16:19:21', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('611', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '722', '12783.7568359375', '2020-06-11 16:19:21', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('612', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '723', '14.7353515625', '2020-06-11 16:19:21', '0', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('613', '27', '问题30.txt', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/问题30.txt', '723', '1.4912109375', '2020-06-11 16:19:21', '0', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('614', '27', 'headImg3.jpeg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/headImg3.jpeg', '713', '51.3076171875', '2020-06-11 16:19:21', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('615', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '713', '8217.2353515625', '2020-06-11 16:19:21', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('616', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '713', '12783.7568359375', '2020-06-11 16:19:21', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('617', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '713', '14.7353515625', '2020-06-11 16:19:21', '5', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('618', '27', 'Git-2.26.0-64-bit.exe', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Git-2.26.0-64-bit.exe', '713', '45575.3828125', '2020-06-11 16:19:21', '6', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('619', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '713', '8217.2353515625', '2020-06-11 16:19:21', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('620', '27', 'Jellyfish.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Jellyfish.jpg', '727', '128.4267578125', '2020-06-11 16:19:38', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('621', '27', 'Lighthouse.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Lighthouse.jpg', '727', '164.98828125', '2020-06-11 16:19:38', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('622', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '728', '8217.2353515625', '2020-06-11 16:19:38', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('623', '27', 'Maid with the Flaxen Hair.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Maid with the Flaxen Hair.mp3', '728', '4729.0869140625', '2020-06-11 16:19:38', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('624', '27', 'Sleep Away.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Sleep Away.mp3', '728', '4729.0869140625', '2020-06-11 16:19:38', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('625', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '729', '12783.7568359375', '2020-06-11 16:19:38', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('626', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '730', '14.7353515625', '2020-06-11 16:19:38', '0', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('627', '27', '问题30.txt', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/问题30.txt', '730', '1.4912109375', '2020-06-11 16:19:38', '0', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('628', '27', 'Jellyfish.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Jellyfish.jpg', '732', '128.4267578125', '2020-06-11 16:19:38', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('629', '27', 'Lighthouse.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Lighthouse.jpg', '732', '164.98828125', '2020-06-11 16:19:38', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('630', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '733', '8217.2353515625', '2020-06-11 16:19:38', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('631', '27', 'Maid with the Flaxen Hair.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Maid with the Flaxen Hair.mp3', '733', '4729.0869140625', '2020-06-11 16:19:38', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('632', '27', 'Sleep Away.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Sleep Away.mp3', '733', '4729.0869140625', '2020-06-11 16:19:38', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('633', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '734', '12783.7568359375', '2020-06-11 16:19:38', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('634', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '735', '14.7353515625', '2020-06-11 16:19:38', '0', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('635', '27', '问题30.txt', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/问题30.txt', '735', '1.4912109375', '2020-06-11 16:19:38', '0', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('636', '27', 'headImg3.jpeg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/headImg3.jpeg', '725', '51.3076171875', '2020-06-11 16:19:38', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('637', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '725', '8217.2353515625', '2020-06-11 16:19:38', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('638', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '725', '12783.7568359375', '2020-06-11 16:19:38', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('639', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '725', '14.7353515625', '2020-06-11 16:19:38', '5', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('640', '27', 'Git-2.26.0-64-bit.exe', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Git-2.26.0-64-bit.exe', '725', '45575.3828125', '2020-06-11 16:19:38', '6', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('641', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '725', '8217.2353515625', '2020-06-11 16:19:38', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('642', '27', 'Jellyfish.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Jellyfish.jpg', '739', '128.4267578125', '2020-06-11 16:20:29', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('643', '27', 'Lighthouse.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Lighthouse.jpg', '739', '164.98828125', '2020-06-11 16:20:29', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('644', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '740', '8217.2353515625', '2020-06-11 16:20:29', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('645', '27', 'Maid with the Flaxen Hair.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Maid with the Flaxen Hair.mp3', '740', '4729.0869140625', '2020-06-11 16:20:29', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('646', '27', 'Sleep Away.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Sleep Away.mp3', '740', '4729.0869140625', '2020-06-11 16:20:29', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('647', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '741', '12783.7568359375', '2020-06-11 16:20:29', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('648', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '742', '14.7353515625', '2020-06-11 16:20:29', '5', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('649', '27', '问题30.txt', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/问题30.txt', '742', '1.4912109375', '2020-06-11 16:20:29', '5', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('650', '27', 'Jellyfish.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Jellyfish.jpg', '745', '128.4267578125', '2020-06-11 16:20:29', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('651', '27', 'Lighthouse.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Lighthouse.jpg', '745', '164.98828125', '2020-06-11 16:20:29', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('652', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '746', '8217.2353515625', '2020-06-11 16:20:29', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('653', '27', 'Maid with the Flaxen Hair.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Maid with the Flaxen Hair.mp3', '746', '4729.0869140625', '2020-06-11 16:20:29', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('654', '27', 'Sleep Away.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Sleep Away.mp3', '746', '4729.0869140625', '2020-06-11 16:20:29', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('655', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '747', '12783.7568359375', '2020-06-11 16:20:29', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('656', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '748', '14.7353515625', '2020-06-11 16:20:29', '6', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('657', '27', '问题30.txt', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/问题30.txt', '748', '1.4912109375', '2020-06-11 16:20:29', '6', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('658', '27', 'Jellyfish.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Jellyfish.jpg', '750', '128.4267578125', '2020-06-11 16:20:29', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('659', '27', 'Lighthouse.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Lighthouse.jpg', '750', '164.98828125', '2020-06-11 16:20:29', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('660', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '751', '8217.2353515625', '2020-06-11 16:20:29', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('661', '27', 'Maid with the Flaxen Hair.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Maid with the Flaxen Hair.mp3', '751', '4729.0869140625', '2020-06-11 16:20:29', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('662', '27', 'Sleep Away.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Sleep Away.mp3', '751', '4729.0869140625', '2020-06-11 16:20:29', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('663', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '752', '12783.7568359375', '2020-06-11 16:20:29', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('664', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '753', '14.7353515625', '2020-06-11 16:20:29', '6', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('665', '27', '问题30.txt', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/问题30.txt', '753', '1.4912109375', '2020-06-11 16:20:29', '6', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('666', '27', 'headImg3.jpeg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/headImg3.jpeg', '743', '51.3076171875', '2020-06-11 16:20:29', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('667', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '743', '8217.2353515625', '2020-06-11 16:20:29', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('668', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '743', '12783.7568359375', '2020-06-11 16:20:29', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('669', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '743', '14.7353515625', '2020-06-11 16:20:29', '0', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('670', '27', 'Git-2.26.0-64-bit.exe', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Git-2.26.0-64-bit.exe', '743', '45575.3828125', '2020-06-11 16:20:29', '0', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('671', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '743', '8217.2353515625', '2020-06-11 16:20:29', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('672', '27', 'Jellyfish.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Jellyfish.jpg', '757', '128.4267578125', '2020-06-11 16:20:29', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('673', '27', 'Lighthouse.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Lighthouse.jpg', '757', '164.98828125', '2020-06-11 16:20:29', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('674', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '758', '8217.2353515625', '2020-06-11 16:20:29', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('675', '27', 'Maid with the Flaxen Hair.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Maid with the Flaxen Hair.mp3', '758', '4729.0869140625', '2020-06-11 16:20:29', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('676', '27', 'Sleep Away.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Sleep Away.mp3', '758', '4729.0869140625', '2020-06-11 16:20:29', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('677', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '759', '12783.7568359375', '2020-06-11 16:20:29', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('678', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '760', '14.7353515625', '2020-06-11 16:20:29', '6', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('679', '27', '问题30.txt', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/问题30.txt', '760', '1.4912109375', '2020-06-11 16:20:29', '6', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('680', '27', 'Jellyfish.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Jellyfish.jpg', '762', '128.4267578125', '2020-06-11 16:20:29', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('681', '27', 'Lighthouse.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Lighthouse.jpg', '762', '164.98828125', '2020-06-11 16:20:29', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('682', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '763', '8217.2353515625', '2020-06-11 16:20:29', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('683', '27', 'Maid with the Flaxen Hair.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Maid with the Flaxen Hair.mp3', '763', '4729.0869140625', '2020-06-11 16:20:29', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('684', '27', 'Sleep Away.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Sleep Away.mp3', '763', '4729.0869140625', '2020-06-11 16:20:29', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('685', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '764', '12783.7568359375', '2020-06-11 16:20:29', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('686', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '765', '14.7353515625', '2020-06-11 16:20:29', '6', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('687', '27', '问题30.txt', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/问题30.txt', '765', '1.4912109375', '2020-06-11 16:20:29', '6', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('688', '27', 'headImg3.jpeg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/headImg3.jpeg', '755', '51.3076171875', '2020-06-11 16:20:29', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('689', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '755', '8217.2353515625', '2020-06-11 16:20:29', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('690', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '755', '12783.7568359375', '2020-06-11 16:20:29', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('691', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '755', '14.7353515625', '2020-06-11 16:20:29', '0', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('692', '27', 'Git-2.26.0-64-bit.exe', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Git-2.26.0-64-bit.exe', '755', '45575.3828125', '2020-06-11 16:20:29', '0', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('693', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '755', '8217.2353515625', '2020-06-11 16:20:29', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('694', '27', 'Jellyfish.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Jellyfish.jpg', '769', '128.4267578125', '2020-06-11 16:20:29', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('695', '27', 'Lighthouse.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Lighthouse.jpg', '769', '164.98828125', '2020-06-11 16:20:29', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('696', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '770', '8217.2353515625', '2020-06-11 16:20:29', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('697', '27', 'Maid with the Flaxen Hair.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Maid with the Flaxen Hair.mp3', '770', '4729.0869140625', '2020-06-11 16:20:29', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('698', '27', 'Sleep Away.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Sleep Away.mp3', '770', '4729.0869140625', '2020-06-11 16:20:29', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('699', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '771', '12783.7568359375', '2020-06-11 16:20:29', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('700', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '772', '14.7353515625', '2020-06-11 16:20:29', '6', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('701', '27', '问题30.txt', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/问题30.txt', '772', '1.4912109375', '2020-06-11 16:20:29', '6', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('702', '27', 'Jellyfish.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Jellyfish.jpg', '774', '128.4267578125', '2020-06-11 16:20:29', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('703', '27', 'Lighthouse.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Lighthouse.jpg', '774', '164.98828125', '2020-06-11 16:20:29', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('704', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '775', '8217.2353515625', '2020-06-11 16:20:29', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('705', '27', 'Maid with the Flaxen Hair.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Maid with the Flaxen Hair.mp3', '775', '4729.0869140625', '2020-06-11 16:20:29', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('706', '27', 'Sleep Away.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Sleep Away.mp3', '775', '4729.0869140625', '2020-06-11 16:20:29', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('707', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '776', '12783.7568359375', '2020-06-11 16:20:29', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('708', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '777', '14.7353515625', '2020-06-11 16:20:29', '6', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('709', '27', '问题30.txt', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/问题30.txt', '777', '1.4912109375', '2020-06-11 16:20:29', '6', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('710', '27', 'headImg3.jpeg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/headImg3.jpeg', '767', '51.3076171875', '2020-06-11 16:20:29', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('711', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '767', '8217.2353515625', '2020-06-11 16:20:29', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('712', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '767', '12783.7568359375', '2020-06-11 16:20:29', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('713', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '767', '14.7353515625', '2020-06-11 16:20:29', '0', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('714', '27', 'Git-2.26.0-64-bit.exe', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Git-2.26.0-64-bit.exe', '767', '45575.3828125', '2020-06-11 16:20:29', '0', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('715', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '767', '8217.2353515625', '2020-06-11 16:20:29', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('716', '27', 'Jellyfish.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Jellyfish.jpg', '781', '128.4267578125', '2020-06-11 16:20:29', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('717', '27', 'Lighthouse.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Lighthouse.jpg', '781', '164.98828125', '2020-06-11 16:20:29', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('718', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '782', '8217.2353515625', '2020-06-11 16:20:29', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('719', '27', 'Maid with the Flaxen Hair.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Maid with the Flaxen Hair.mp3', '782', '4729.0869140625', '2020-06-11 16:20:29', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('720', '27', 'Sleep Away.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Sleep Away.mp3', '782', '4729.0869140625', '2020-06-11 16:20:29', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('721', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '783', '12783.7568359375', '2020-06-11 16:20:29', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('722', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '784', '14.7353515625', '2020-06-11 16:20:29', '6', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('723', '27', '问题30.txt', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/问题30.txt', '784', '1.4912109375', '2020-06-11 16:20:29', '6', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('724', '27', 'Jellyfish.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Jellyfish.jpg', '786', '128.4267578125', '2020-06-11 16:20:29', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('725', '27', 'Lighthouse.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Lighthouse.jpg', '786', '164.98828125', '2020-06-11 16:20:29', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('726', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '787', '8217.2353515625', '2020-06-11 16:20:29', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('727', '27', 'Maid with the Flaxen Hair.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Maid with the Flaxen Hair.mp3', '787', '4729.0869140625', '2020-06-11 16:20:29', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('728', '27', 'Sleep Away.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Sleep Away.mp3', '787', '4729.0869140625', '2020-06-11 16:20:29', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('729', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '788', '12783.7568359375', '2020-06-11 16:20:29', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('730', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '789', '14.7353515625', '2020-06-11 16:20:29', '6', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('731', '27', '问题30.txt', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/问题30.txt', '789', '1.4912109375', '2020-06-11 16:20:29', '6', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('732', '27', 'headImg3.jpeg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/headImg3.jpeg', '779', '51.3076171875', '2020-06-11 16:20:29', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('733', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '779', '8217.2353515625', '2020-06-11 16:20:29', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('734', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '779', '12783.7568359375', '2020-06-11 16:20:29', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('735', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '779', '14.7353515625', '2020-06-11 16:20:29', '0', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('736', '27', 'Git-2.26.0-64-bit.exe', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Git-2.26.0-64-bit.exe', '779', '45575.3828125', '2020-06-11 16:20:29', '0', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('737', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '779', '8217.2353515625', '2020-06-11 16:20:29', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('738', '27', 'headImg3.jpeg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/headImg3.jpeg', '738', '51.3076171875', '2020-06-11 16:20:29', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('739', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '738', '8217.2353515625', '2020-06-11 16:20:29', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('740', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '738', '12783.7568359375', '2020-06-11 16:20:29', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('741', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '738', '14.7353515625', '2020-06-11 16:20:29', '5', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('742', '27', 'Git-2.26.0-64-bit.exe', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Git-2.26.0-64-bit.exe', '738', '45575.3828125', '2020-06-11 16:20:29', '6', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('743', '27', 'Jellyfish.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Jellyfish.jpg', '792', '128.4267578125', '2020-06-11 16:21:18', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('744', '27', 'Lighthouse.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Lighthouse.jpg', '792', '164.98828125', '2020-06-11 16:21:18', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('745', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '793', '8217.2353515625', '2020-06-11 16:21:18', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('746', '27', 'Maid with the Flaxen Hair.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Maid with the Flaxen Hair.mp3', '793', '4729.0869140625', '2020-06-11 16:21:18', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('747', '27', 'Sleep Away.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Sleep Away.mp3', '793', '4729.0869140625', '2020-06-11 16:21:18', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('748', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '794', '12783.7568359375', '2020-06-11 16:21:18', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('749', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '795', '14.7353515625', '2020-06-11 16:21:18', '5', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('750', '27', '问题30.txt', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/问题30.txt', '795', '1.4912109375', '2020-06-11 16:21:18', '5', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('751', '27', 'Jellyfish.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Jellyfish.jpg', '798', '128.4267578125', '2020-06-11 16:21:18', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('752', '27', 'Lighthouse.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Lighthouse.jpg', '798', '164.98828125', '2020-06-11 16:21:18', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('753', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '799', '8217.2353515625', '2020-06-11 16:21:18', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('754', '27', 'Maid with the Flaxen Hair.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Maid with the Flaxen Hair.mp3', '799', '4729.0869140625', '2020-06-11 16:21:18', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('755', '27', 'Sleep Away.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Sleep Away.mp3', '799', '4729.0869140625', '2020-06-11 16:21:18', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('756', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '800', '12783.7568359375', '2020-06-11 16:21:18', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('757', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '801', '14.7353515625', '2020-06-11 16:21:18', '6', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('758', '27', '问题30.txt', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/问题30.txt', '801', '1.4912109375', '2020-06-11 16:21:18', '6', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('759', '27', 'Jellyfish.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Jellyfish.jpg', '803', '128.4267578125', '2020-06-11 16:21:18', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('760', '27', 'Lighthouse.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Lighthouse.jpg', '803', '164.98828125', '2020-06-11 16:21:18', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('761', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '804', '8217.2353515625', '2020-06-11 16:21:18', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('762', '27', 'Maid with the Flaxen Hair.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Maid with the Flaxen Hair.mp3', '804', '4729.0869140625', '2020-06-11 16:21:18', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('763', '27', 'Sleep Away.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Sleep Away.mp3', '804', '4729.0869140625', '2020-06-11 16:21:18', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('764', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '805', '12783.7568359375', '2020-06-11 16:21:18', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('765', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '806', '14.7353515625', '2020-06-11 16:21:18', '6', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('766', '27', '问题30.txt', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/问题30.txt', '806', '1.4912109375', '2020-06-11 16:21:18', '6', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('767', '27', 'headImg3.jpeg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/headImg3.jpeg', '796', '51.3076171875', '2020-06-11 16:21:18', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('768', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '796', '8217.2353515625', '2020-06-11 16:21:18', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('769', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '796', '12783.7568359375', '2020-06-11 16:21:18', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('770', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '796', '14.7353515625', '2020-06-11 16:21:18', '0', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('771', '27', 'Git-2.26.0-64-bit.exe', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Git-2.26.0-64-bit.exe', '796', '45575.3828125', '2020-06-11 16:21:18', '0', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('772', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '796', '8217.2353515625', '2020-06-11 16:21:18', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('773', '27', 'Jellyfish.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Jellyfish.jpg', '810', '128.4267578125', '2020-06-11 16:21:18', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('774', '27', 'Lighthouse.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Lighthouse.jpg', '810', '164.98828125', '2020-06-11 16:21:18', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('775', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '811', '8217.2353515625', '2020-06-11 16:21:18', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('776', '27', 'Maid with the Flaxen Hair.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Maid with the Flaxen Hair.mp3', '811', '4729.0869140625', '2020-06-11 16:21:18', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('777', '27', 'Sleep Away.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Sleep Away.mp3', '811', '4729.0869140625', '2020-06-11 16:21:18', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('778', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '812', '12783.7568359375', '2020-06-11 16:21:18', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('779', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '813', '14.7353515625', '2020-06-11 16:21:18', '6', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('780', '27', '问题30.txt', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/问题30.txt', '813', '1.4912109375', '2020-06-11 16:21:18', '6', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('781', '27', 'Jellyfish.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Jellyfish.jpg', '815', '128.4267578125', '2020-06-11 16:21:18', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('782', '27', 'Lighthouse.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Lighthouse.jpg', '815', '164.98828125', '2020-06-11 16:21:18', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('783', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '816', '8217.2353515625', '2020-06-11 16:21:18', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('784', '27', 'Maid with the Flaxen Hair.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Maid with the Flaxen Hair.mp3', '816', '4729.0869140625', '2020-06-11 16:21:18', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('785', '27', 'Sleep Away.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Sleep Away.mp3', '816', '4729.0869140625', '2020-06-11 16:21:18', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('786', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '817', '12783.7568359375', '2020-06-11 16:21:18', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('787', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '818', '14.7353515625', '2020-06-11 16:21:18', '6', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('788', '27', '问题30.txt', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/问题30.txt', '818', '1.4912109375', '2020-06-11 16:21:18', '6', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('789', '27', 'headImg3.jpeg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/headImg3.jpeg', '808', '51.3076171875', '2020-06-11 16:21:18', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('790', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '808', '8217.2353515625', '2020-06-11 16:21:18', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('791', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '808', '12783.7568359375', '2020-06-11 16:21:18', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('792', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '808', '14.7353515625', '2020-06-11 16:21:18', '0', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('793', '27', 'Git-2.26.0-64-bit.exe', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Git-2.26.0-64-bit.exe', '808', '45575.3828125', '2020-06-11 16:21:18', '0', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('794', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '808', '8217.2353515625', '2020-06-11 16:21:18', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('795', '27', 'Jellyfish.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Jellyfish.jpg', '822', '128.4267578125', '2020-06-11 16:21:18', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('796', '27', 'Lighthouse.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Lighthouse.jpg', '822', '164.98828125', '2020-06-11 16:21:18', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('797', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '823', '8217.2353515625', '2020-06-11 16:21:18', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('798', '27', 'Maid with the Flaxen Hair.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Maid with the Flaxen Hair.mp3', '823', '4729.0869140625', '2020-06-11 16:21:18', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('799', '27', 'Sleep Away.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Sleep Away.mp3', '823', '4729.0869140625', '2020-06-11 16:21:18', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('800', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '824', '12783.7568359375', '2020-06-11 16:21:18', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('801', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '825', '14.7353515625', '2020-06-11 16:21:18', '6', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('802', '27', '问题30.txt', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/问题30.txt', '825', '1.4912109375', '2020-06-11 16:21:18', '6', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('803', '27', 'Jellyfish.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Jellyfish.jpg', '827', '128.4267578125', '2020-06-11 16:21:18', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('804', '27', 'Lighthouse.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Lighthouse.jpg', '827', '164.98828125', '2020-06-11 16:21:18', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('805', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '828', '8217.2353515625', '2020-06-11 16:21:18', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('806', '27', 'Maid with the Flaxen Hair.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Maid with the Flaxen Hair.mp3', '828', '4729.0869140625', '2020-06-11 16:21:18', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('807', '27', 'Sleep Away.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Sleep Away.mp3', '828', '4729.0869140625', '2020-06-11 16:21:18', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('808', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '829', '12783.7568359375', '2020-06-11 16:21:18', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('809', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '830', '14.7353515625', '2020-06-11 16:21:18', '6', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('810', '27', '问题30.txt', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/问题30.txt', '830', '1.4912109375', '2020-06-11 16:21:18', '6', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('811', '27', 'headImg3.jpeg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/headImg3.jpeg', '820', '51.3076171875', '2020-06-11 16:21:18', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('812', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '820', '8217.2353515625', '2020-06-11 16:21:18', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('813', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '820', '12783.7568359375', '2020-06-11 16:21:18', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('814', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '820', '14.7353515625', '2020-06-11 16:21:18', '0', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('815', '27', 'Git-2.26.0-64-bit.exe', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Git-2.26.0-64-bit.exe', '820', '45575.3828125', '2020-06-11 16:21:18', '0', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('816', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '820', '8217.2353515625', '2020-06-11 16:21:18', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('817', '27', 'Jellyfish.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Jellyfish.jpg', '834', '128.4267578125', '2020-06-11 16:21:18', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('818', '27', 'Lighthouse.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Lighthouse.jpg', '834', '164.98828125', '2020-06-11 16:21:18', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('819', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '835', '8217.2353515625', '2020-06-11 16:21:18', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('820', '27', 'Maid with the Flaxen Hair.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Maid with the Flaxen Hair.mp3', '835', '4729.0869140625', '2020-06-11 16:21:18', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('821', '27', 'Sleep Away.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Sleep Away.mp3', '835', '4729.0869140625', '2020-06-11 16:21:18', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('822', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '836', '12783.7568359375', '2020-06-11 16:21:18', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('823', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '837', '14.7353515625', '2020-06-11 16:21:18', '6', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('824', '27', '问题30.txt', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/问题30.txt', '837', '1.4912109375', '2020-06-11 16:21:18', '6', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('825', '27', 'Jellyfish.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Jellyfish.jpg', '839', '128.4267578125', '2020-06-11 16:21:18', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('826', '27', 'Lighthouse.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Lighthouse.jpg', '839', '164.98828125', '2020-06-11 16:21:18', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('827', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '840', '8217.2353515625', '2020-06-11 16:21:18', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('828', '27', 'Maid with the Flaxen Hair.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Maid with the Flaxen Hair.mp3', '840', '4729.0869140625', '2020-06-11 16:21:18', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('829', '27', 'Sleep Away.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Sleep Away.mp3', '840', '4729.0869140625', '2020-06-11 16:21:18', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('830', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '841', '12783.7568359375', '2020-06-11 16:21:18', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('831', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '842', '14.7353515625', '2020-06-11 16:21:18', '6', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('832', '27', '问题30.txt', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/问题30.txt', '842', '1.4912109375', '2020-06-11 16:21:18', '6', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('833', '27', 'headImg3.jpeg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/headImg3.jpeg', '832', '51.3076171875', '2020-06-11 16:21:18', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('834', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '832', '8217.2353515625', '2020-06-11 16:21:18', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('835', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '832', '12783.7568359375', '2020-06-11 16:21:18', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('836', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '832', '14.7353515625', '2020-06-11 16:21:18', '0', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('837', '27', 'Git-2.26.0-64-bit.exe', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Git-2.26.0-64-bit.exe', '832', '45575.3828125', '2020-06-11 16:21:18', '0', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('838', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '832', '8217.2353515625', '2020-06-11 16:21:18', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('839', '27', 'headImg3.jpeg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/headImg3.jpeg', '791', '51.3076171875', '2020-06-11 16:21:18', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('840', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '791', '8217.2353515625', '2020-06-11 16:21:18', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('841', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '791', '12783.7568359375', '2020-06-11 16:21:18', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('842', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '791', '14.7353515625', '2020-06-11 16:21:18', '5', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('843', '27', 'Git-2.26.0-64-bit.exe', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Git-2.26.0-64-bit.exe', '791', '45575.3828125', '2020-06-11 16:21:18', '6', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('844', '27', 'Jellyfish.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Jellyfish.jpg', '845', '128.4267578125', '2020-06-11 16:21:45', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('845', '27', 'Lighthouse.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Lighthouse.jpg', '845', '164.98828125', '2020-06-11 16:21:45', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('846', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '846', '8217.2353515625', '2020-06-11 16:21:45', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('847', '27', 'Maid with the Flaxen Hair.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Maid with the Flaxen Hair.mp3', '846', '4729.0869140625', '2020-06-11 16:21:45', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('848', '27', 'Sleep Away.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Sleep Away.mp3', '846', '4729.0869140625', '2020-06-11 16:21:45', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('849', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '847', '12783.7568359375', '2020-06-11 16:21:45', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('850', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '848', '14.7353515625', '2020-06-11 16:21:45', '5', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('851', '27', '问题30.txt', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/问题30.txt', '848', '1.4912109375', '2020-06-11 16:21:45', '5', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('852', '27', 'Jellyfish.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Jellyfish.jpg', '851', '128.4267578125', '2020-06-11 16:21:45', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('853', '27', 'Lighthouse.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Lighthouse.jpg', '851', '164.98828125', '2020-06-11 16:21:45', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('854', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '852', '8217.2353515625', '2020-06-11 16:21:45', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('855', '27', 'Maid with the Flaxen Hair.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Maid with the Flaxen Hair.mp3', '852', '4729.0869140625', '2020-06-11 16:21:45', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('856', '27', 'Sleep Away.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Sleep Away.mp3', '852', '4729.0869140625', '2020-06-11 16:21:45', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('857', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '853', '12783.7568359375', '2020-06-11 16:21:45', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('858', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '854', '14.7353515625', '2020-06-11 16:21:45', '6', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('859', '27', '问题30.txt', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/问题30.txt', '854', '1.4912109375', '2020-06-11 16:21:45', '6', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('860', '27', 'Jellyfish.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Jellyfish.jpg', '856', '128.4267578125', '2020-06-11 16:21:45', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('861', '27', 'Lighthouse.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Lighthouse.jpg', '856', '164.98828125', '2020-06-11 16:21:45', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('862', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '857', '8217.2353515625', '2020-06-11 16:21:45', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('863', '27', 'Maid with the Flaxen Hair.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Maid with the Flaxen Hair.mp3', '857', '4729.0869140625', '2020-06-11 16:21:45', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('864', '27', 'Sleep Away.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Sleep Away.mp3', '857', '4729.0869140625', '2020-06-11 16:21:45', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('865', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '858', '12783.7568359375', '2020-06-11 16:21:45', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('866', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '859', '14.7353515625', '2020-06-11 16:21:45', '6', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('867', '27', '问题30.txt', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/问题30.txt', '859', '1.4912109375', '2020-06-11 16:21:45', '6', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('868', '27', 'headImg3.jpeg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/headImg3.jpeg', '849', '51.3076171875', '2020-06-11 16:21:45', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('869', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '849', '8217.2353515625', '2020-06-11 16:21:45', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('870', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '849', '12783.7568359375', '2020-06-11 16:21:45', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('871', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '849', '14.7353515625', '2020-06-11 16:21:45', '0', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('872', '27', 'Git-2.26.0-64-bit.exe', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Git-2.26.0-64-bit.exe', '849', '45575.3828125', '2020-06-11 16:21:45', '0', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('873', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '849', '8217.2353515625', '2020-06-11 16:21:45', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('874', '27', 'Jellyfish.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Jellyfish.jpg', '863', '128.4267578125', '2020-06-11 16:21:45', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('875', '27', 'Lighthouse.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Lighthouse.jpg', '863', '164.98828125', '2020-06-11 16:21:45', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('876', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '864', '8217.2353515625', '2020-06-11 16:21:45', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('877', '27', 'Maid with the Flaxen Hair.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Maid with the Flaxen Hair.mp3', '864', '4729.0869140625', '2020-06-11 16:21:45', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('878', '27', 'Sleep Away.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Sleep Away.mp3', '864', '4729.0869140625', '2020-06-11 16:21:45', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('879', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '865', '12783.7568359375', '2020-06-11 16:21:45', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('880', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '866', '14.7353515625', '2020-06-11 16:21:45', '6', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('881', '27', '问题30.txt', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/问题30.txt', '866', '1.4912109375', '2020-06-11 16:21:45', '6', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('882', '27', 'Jellyfish.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Jellyfish.jpg', '868', '128.4267578125', '2020-06-11 16:21:45', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('883', '27', 'Lighthouse.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Lighthouse.jpg', '868', '164.98828125', '2020-06-11 16:21:45', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('884', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '869', '8217.2353515625', '2020-06-11 16:21:45', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('885', '27', 'Maid with the Flaxen Hair.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Maid with the Flaxen Hair.mp3', '869', '4729.0869140625', '2020-06-11 16:21:45', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('886', '27', 'Sleep Away.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Sleep Away.mp3', '869', '4729.0869140625', '2020-06-11 16:21:45', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('887', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '870', '12783.7568359375', '2020-06-11 16:21:45', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('888', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '871', '14.7353515625', '2020-06-11 16:21:45', '6', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('889', '27', '问题30.txt', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/问题30.txt', '871', '1.4912109375', '2020-06-11 16:21:45', '6', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('890', '27', 'headImg3.jpeg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/headImg3.jpeg', '861', '51.3076171875', '2020-06-11 16:21:45', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('891', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '861', '8217.2353515625', '2020-06-11 16:21:45', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('892', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '861', '12783.7568359375', '2020-06-11 16:21:45', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('893', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '861', '14.7353515625', '2020-06-11 16:21:45', '0', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('894', '27', 'Git-2.26.0-64-bit.exe', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Git-2.26.0-64-bit.exe', '861', '45575.3828125', '2020-06-11 16:21:45', '0', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('895', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '861', '8217.2353515625', '2020-06-11 16:21:45', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('896', '27', 'Jellyfish.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Jellyfish.jpg', '875', '128.4267578125', '2020-06-11 16:21:45', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('897', '27', 'Lighthouse.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Lighthouse.jpg', '875', '164.98828125', '2020-06-11 16:21:45', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('898', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '876', '8217.2353515625', '2020-06-11 16:21:45', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('899', '27', 'Maid with the Flaxen Hair.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Maid with the Flaxen Hair.mp3', '876', '4729.0869140625', '2020-06-11 16:21:45', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('900', '27', 'Sleep Away.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Sleep Away.mp3', '876', '4729.0869140625', '2020-06-11 16:21:45', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('901', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '877', '12783.7568359375', '2020-06-11 16:21:45', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('902', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '878', '14.7353515625', '2020-06-11 16:21:45', '6', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('903', '27', '问题30.txt', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/问题30.txt', '878', '1.4912109375', '2020-06-11 16:21:45', '6', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('904', '27', 'Jellyfish.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Jellyfish.jpg', '880', '128.4267578125', '2020-06-11 16:21:45', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('905', '27', 'Lighthouse.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Lighthouse.jpg', '880', '164.98828125', '2020-06-11 16:21:45', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('906', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '881', '8217.2353515625', '2020-06-11 16:21:45', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('907', '27', 'Maid with the Flaxen Hair.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Maid with the Flaxen Hair.mp3', '881', '4729.0869140625', '2020-06-11 16:21:45', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('908', '27', 'Sleep Away.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Sleep Away.mp3', '881', '4729.0869140625', '2020-06-11 16:21:45', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('909', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '882', '12783.7568359375', '2020-06-11 16:21:45', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('910', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '883', '14.7353515625', '2020-06-11 16:21:45', '6', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('911', '27', '问题30.txt', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/问题30.txt', '883', '1.4912109375', '2020-06-11 16:21:45', '6', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('912', '27', 'headImg3.jpeg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/headImg3.jpeg', '873', '51.3076171875', '2020-06-11 16:21:45', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('913', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '873', '8217.2353515625', '2020-06-11 16:21:45', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('914', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '873', '12783.7568359375', '2020-06-11 16:21:45', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('915', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '873', '14.7353515625', '2020-06-11 16:21:45', '0', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('916', '27', 'Git-2.26.0-64-bit.exe', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Git-2.26.0-64-bit.exe', '873', '45575.3828125', '2020-06-11 16:21:45', '0', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('917', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '873', '8217.2353515625', '2020-06-11 16:21:45', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('918', '27', 'Jellyfish.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Jellyfish.jpg', '887', '128.4267578125', '2020-06-11 16:21:45', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('919', '27', 'Lighthouse.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Lighthouse.jpg', '887', '164.98828125', '2020-06-11 16:21:45', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('920', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '888', '8217.2353515625', '2020-06-11 16:21:45', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('921', '27', 'Maid with the Flaxen Hair.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Maid with the Flaxen Hair.mp3', '888', '4729.0869140625', '2020-06-11 16:21:45', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('922', '27', 'Sleep Away.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Sleep Away.mp3', '888', '4729.0869140625', '2020-06-11 16:21:45', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('923', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '889', '12783.7568359375', '2020-06-11 16:21:45', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('924', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '890', '14.7353515625', '2020-06-11 16:21:45', '6', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('925', '27', '问题30.txt', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/问题30.txt', '890', '1.4912109375', '2020-06-11 16:21:45', '6', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('926', '27', 'Jellyfish.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Jellyfish.jpg', '892', '128.4267578125', '2020-06-11 16:21:45', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('927', '27', 'Lighthouse.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Lighthouse.jpg', '892', '164.98828125', '2020-06-11 16:21:45', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('928', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '893', '8217.2353515625', '2020-06-11 16:21:45', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('929', '27', 'Maid with the Flaxen Hair.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Maid with the Flaxen Hair.mp3', '893', '4729.0869140625', '2020-06-11 16:21:45', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('930', '27', 'Sleep Away.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Sleep Away.mp3', '893', '4729.0869140625', '2020-06-11 16:21:45', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('931', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '894', '12783.7568359375', '2020-06-11 16:21:45', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('932', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '895', '14.7353515625', '2020-06-11 16:21:45', '6', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('933', '27', '问题30.txt', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/问题30.txt', '895', '1.4912109375', '2020-06-11 16:21:45', '6', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('934', '27', 'headImg3.jpeg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/headImg3.jpeg', '885', '51.3076171875', '2020-06-11 16:21:45', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('935', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '885', '8217.2353515625', '2020-06-11 16:21:45', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('936', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '885', '12783.7568359375', '2020-06-11 16:21:45', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('937', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '885', '14.7353515625', '2020-06-11 16:21:45', '0', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('938', '27', 'Git-2.26.0-64-bit.exe', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Git-2.26.0-64-bit.exe', '885', '45575.3828125', '2020-06-11 16:21:45', '0', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('939', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '885', '8217.2353515625', '2020-06-11 16:21:45', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('940', '27', 'headImg3.jpeg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/headImg3.jpeg', '844', '51.3076171875', '2020-06-11 16:21:45', '1', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('941', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '844', '8217.2353515625', '2020-06-11 16:21:45', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('942', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '844', '12783.7568359375', '2020-06-11 16:21:45', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('943', '27', '简历.docx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/简历.docx', '844', '14.7353515625', '2020-06-11 16:21:45', '5', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('944', '27', 'Git-2.26.0-64-bit.exe', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Git-2.26.0-64-bit.exe', '844', '45575.3828125', '2020-06-11 16:21:45', '6', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('945', '27', 'Git-2.26.0-64-bit.exe', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Git-2.26.0-64-bit.exe', '577', '45575.3828125', '2020-06-11 16:22:50', '0', null, '2', 'ae0805558a1614fa54d02ccbd5d56a69', '正常');
INSERT INTO `user_file` VALUES ('946', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '897', '8217.2353515625', '2020-06-11 16:52:02', '2', null, '2', '5a9a91184e611dae3fed162b8787ce5f', '正常');
INSERT INTO `user_file` VALUES ('947', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '898', '8217.2353515625', '2020-06-11 16:52:26', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('948', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '0', '8217.2353515625', '2020-06-11 16:52:27', '2', null, '2', '5a9a91184e611dae3fed162b8787ce5f', '正常');
INSERT INTO `user_file` VALUES ('949', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '900', '8217.2353515625', '2020-06-11 16:56:54', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('950', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '901', '8217.2353515625', '2020-06-11 16:56:55', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('951', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '897', '8217.2353515625', '2020-06-11 17:04:27', '2', null, '2', '5a9a91184e611dae3fed162b8787ce5f', '正常');
INSERT INTO `user_file` VALUES ('952', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '903', '8217.2353515625', '2020-06-11 17:04:55', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('953', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '903', '8217.2353515625', '2020-06-11 17:04:55', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('954', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '904', '8217.2353515625', '2020-06-11 17:05:19', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('955', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '904', '8217.2353515625', '2020-06-11 17:05:19', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('956', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '902', '8217.2353515625', '2020-06-11 17:05:20', '2', null, '2', '5a9a91184e611dae3fed162b8787ce5f', '正常');
INSERT INTO `user_file` VALUES ('957', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '905', '8217.2353515625', '2020-06-11 17:06:32', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('958', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '905', '8217.2353515625', '2020-06-11 17:06:32', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('959', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '902', '8217.2353515625', '2020-06-11 17:06:33', '2', null, '2', '5a9a91184e611dae3fed162b8787ce5f', '正常');
INSERT INTO `user_file` VALUES ('960', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '906', '8217.2353515625', '2020-06-11 17:21:17', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('961', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '906', '8217.2353515625', '2020-06-11 17:21:17', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('962', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '902', '8217.2353515625', '2020-06-11 17:21:18', '2', null, '2', '5a9a91184e611dae3fed162b8787ce5f', '正常');
INSERT INTO `user_file` VALUES ('963', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '907', '8217.2353515625', '2020-06-11 17:46:59', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('964', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '907', '8217.2353515625', '2020-06-11 17:46:59', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('965', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '902', '8217.2353515625', '2020-06-11 17:47:00', '2', null, '2', '5a9a91184e611dae3fed162b8787ce5f', '正常');
INSERT INTO `user_file` VALUES ('966', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '908', '8217.2353515625', '2020-06-11 18:33:55', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('967', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '908', '8217.2353515625', '2020-06-11 18:33:55', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('968', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '902', '8217.2353515625', '2020-06-11 18:33:56', '2', null, '2', '5a9a91184e611dae3fed162b8787ce5f', '正常');
INSERT INTO `user_file` VALUES ('969', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '909', '8217.2353515625', '2020-06-11 18:35:51', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('970', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '909', '8217.2353515625', '2020-06-11 18:35:51', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('971', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '902', '8217.2353515625', '2020-06-11 18:35:52', '2', null, '2', '5a9a91184e611dae3fed162b8787ce5f', '正常');
INSERT INTO `user_file` VALUES ('972', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '910', '8217.2353515625', '2020-06-11 18:37:24', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('973', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '910', '8217.2353515625', '2020-06-11 18:37:24', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('974', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '902', '8217.2353515625', '2020-06-11 18:37:25', '2', null, '2', '5a9a91184e611dae3fed162b8787ce5f', '正常');
INSERT INTO `user_file` VALUES ('975', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '911', '8217.2353515625', '2020-06-11 18:38:07', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('976', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '911', '8217.2353515625', '2020-06-11 18:38:07', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('977', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '902', '8217.2353515625', '2020-06-11 18:38:08', '2', null, '2', '5a9a91184e611dae3fed162b8787ce5f', '正常');
INSERT INTO `user_file` VALUES ('978', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '912', '8217.2353515625', '2020-06-11 18:39:09', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('979', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '912', '8217.2353515625', '2020-06-11 18:39:09', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('980', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '902', '8217.2353515625', '2020-06-11 18:39:09', '2', null, '2', '5a9a91184e611dae3fed162b8787ce5f', '正常');
INSERT INTO `user_file` VALUES ('981', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '913', '8217.2353515625', '2020-06-11 18:42:35', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('982', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '913', '8217.2353515625', '2020-06-11 18:42:35', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('983', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '902', '8217.2353515625', '2020-06-11 18:43:22', '2', null, '2', '5a9a91184e611dae3fed162b8787ce5f', '正常');
INSERT INTO `user_file` VALUES ('984', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '902', '8217.2353515625', '2020-06-11 18:43:23', '2', null, '2', '5a9a91184e611dae3fed162b8787ce5f', '正常');
INSERT INTO `user_file` VALUES ('985', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '914', '8217.2353515625', '2020-06-11 18:43:46', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('986', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '914', '8217.2353515625', '2020-06-11 18:43:46', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('987', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '902', '8217.2353515625', '2020-06-11 18:43:47', '2', null, '2', '5a9a91184e611dae3fed162b8787ce5f', '正常');
INSERT INTO `user_file` VALUES ('988', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '915', '8217.2353515625', '2020-06-11 18:56:19', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('989', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '915', '8217.2353515625', '2020-06-11 18:56:19', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('990', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '902', '8217.2353515625', '2020-06-11 18:56:20', '2', null, '2', '5a9a91184e611dae3fed162b8787ce5f', '正常');
INSERT INTO `user_file` VALUES ('991', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '916', '8217.2353515625', '2020-06-11 19:01:37', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('992', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '916', '8217.2353515625', '2020-06-11 19:01:37', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('993', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '902', '8217.2353515625', '2020-06-11 19:01:38', '2', null, '2', '5a9a91184e611dae3fed162b8787ce5f', '正常');
INSERT INTO `user_file` VALUES ('994', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '917', '8217.2353515625', '2020-06-11 19:06:02', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('995', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '917', '8217.2353515625', '2020-06-11 19:06:02', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('996', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '902', '8217.2353515625', '2020-06-11 19:06:03', '2', null, '2', '5a9a91184e611dae3fed162b8787ce5f', '正常');
INSERT INTO `user_file` VALUES ('997', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '918', '8217.2353515625', '2020-06-11 19:06:30', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('998', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '918', '8217.2353515625', '2020-06-11 19:06:30', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('999', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '902', '8217.2353515625', '2020-06-11 19:06:31', '2', null, '2', '5a9a91184e611dae3fed162b8787ce5f', '正常');
INSERT INTO `user_file` VALUES ('1000', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '919', '8217.2353515625', '2020-06-11 19:07:41', '2', null, '2', '5a9a91184e611dae3fed162b8787ce5f', '正常');
INSERT INTO `user_file` VALUES ('1001', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '919', '12783.7568359375', '2020-06-11 19:08:04', '3', null, '2', '4d3aa5431f05489c6f3d86e07ea48c8b', '正常');
INSERT INTO `user_file` VALUES ('1002', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '921', '8217.2353515625', '2020-06-11 19:09:16', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('1003', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '921', '8217.2353515625', '2020-06-11 19:09:16', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('1004', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '922', '8217.2353515625', '2020-06-11 19:09:18', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('1005', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '922', '12783.7568359375', '2020-06-11 19:09:18', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('1006', '27', 'Git-2.26.0-64-bit.exe', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Git-2.26.0-64-bit.exe', '920', '45575.3828125', '2020-06-11 19:09:19', '0', null, '2', 'ae0805558a1614fa54d02ccbd5d56a69', '正常');
INSERT INTO `user_file` VALUES ('1007', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '923', '8217.2353515625', '2020-06-11 19:13:03', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('1008', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '923', '8217.2353515625', '2020-06-11 19:13:03', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('1009', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '920', '8217.2353515625', '2020-06-11 19:13:04', '2', null, '2', '5a9a91184e611dae3fed162b8787ce5f', '正常');
INSERT INTO `user_file` VALUES ('1010', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '924', '8217.2353515625', '2020-06-11 19:13:35', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('1011', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '924', '8217.2353515625', '2020-06-11 19:13:35', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('1012', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '925', '8217.2353515625', '2020-06-11 19:13:36', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('1013', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '925', '12783.7568359375', '2020-06-11 19:13:36', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('1014', '27', 'Git-2.26.0-64-bit.exe', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Git-2.26.0-64-bit.exe', '920', '45575.3828125', '2020-06-11 19:13:37', '0', null, '2', 'ae0805558a1614fa54d02ccbd5d56a69', '正常');
INSERT INTO `user_file` VALUES ('1015', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '926', '8217.2353515625', '2020-06-11 19:18:06', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('1016', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '926', '8217.2353515625', '2020-06-11 19:18:06', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('1017', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '927', '8217.2353515625', '2020-06-11 19:18:07', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('1018', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '927', '12783.7568359375', '2020-06-11 19:18:07', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('1019', '27', 'Git-2.26.0-64-bit.exe', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Git-2.26.0-64-bit.exe', '920', '45575.3828125', '2020-06-11 19:18:08', '0', null, '2', 'ae0805558a1614fa54d02ccbd5d56a69', '正常');
INSERT INTO `user_file` VALUES ('1020', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '919', '12783.7568359375', '2020-06-11 19:18:33', '3', null, '2', '4d3aa5431f05489c6f3d86e07ea48c8b', '正常');
INSERT INTO `user_file` VALUES ('1021', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '928', '8217.2353515625', '2020-06-11 19:19:19', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('1022', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '928', '8217.2353515625', '2020-06-11 19:19:19', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('1023', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '929', '8217.2353515625', '2020-06-11 19:19:20', '2', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('1024', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '929', '12783.7568359375', '2020-06-11 19:19:20', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('1025', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '929', '12783.7568359375', '2020-06-11 19:19:20', '3', null, '2', null, '正常');
INSERT INTO `user_file` VALUES ('1026', '27', 'Kalimba.mp3', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/Kalimba.mp3', '920', '8217.2353515625', '2020-06-11 19:19:21', '2', null, '2', '5a9a91184e611dae3fed162b8787ce5f', '正常');
INSERT INTO `user_file` VALUES ('1027', '27', 'video.mp4', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/27/video.mp4', '920', '12783.7568359375', '2020-06-11 19:19:22', '3', null, '2', '4d3aa5431f05489c6f3d86e07ea48c8b', '正常');
INSERT INTO `user_file` VALUES ('1028', '1', 'toux.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/1/toux.jpg', '573', '7.42578125', '2020-06-12 16:11:58', '1', null, '2', '71c26a3e72413ec58b0983533fc3f47c', '正常');
INSERT INTO `user_file` VALUES ('1029', '23', 'Illegal.jpg', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/23/Illegal.jpg', '0', '16.1123046875', '2020-06-12 16:59:06', '1', null, '2', '6656e447c10a979fc2c1dff14f372744', '正常');
INSERT INTO `user_file` VALUES ('1030', '30', '小区用户.xlsx', 'http://woniuxy.oss-cn-hangzhou.aliyuncs.com/user/hhh/30/小区用户.xlsx', '0', '27226.224609375', '2020-06-12 20:21:36', '4', null, '2', '9f2f2e2586b3165df0c47df57de01bfd', '正常');

-- ----------------------------
-- Table structure for user_session
-- ----------------------------
DROP TABLE IF EXISTS `user_session`;
CREATE TABLE `user_session` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user1_id` int(11) DEFAULT NULL COMMENT '用户1id',
  `user2_id` int(11) DEFAULT NULL COMMENT '用户2id',
  `user1_status` int(11) DEFAULT NULL COMMENT '0表示不存在，1表示存在',
  `user2_status` int(11) DEFAULT NULL COMMENT '0表示不存在，1表示存在',
  `status` int(11) DEFAULT NULL,
  `group_id` int(255) DEFAULT NULL COMMENT '群组id',
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_session
-- ----------------------------
INSERT INTO `user_session` VALUES ('27', '3', '27', '1', '1', null, null, '2020-06-11 14:16:36');
INSERT INTO `user_session` VALUES ('28', '26', '3', '1', '0', null, null, '2020-06-11 13:25:37');
INSERT INTO `user_session` VALUES ('29', '27', '2', '1', '1', null, null, '2020-06-11 16:47:57');
INSERT INTO `user_session` VALUES ('30', null, null, '1', '1', null, '16', '2020-06-11 13:47:23');
INSERT INTO `user_session` VALUES ('31', '27', '28', '1', '1', null, null, '2020-06-11 16:47:05');
INSERT INTO `user_session` VALUES ('32', null, null, null, null, null, '17', '2020-06-11 16:09:15');
INSERT INTO `user_session` VALUES ('33', null, null, null, null, null, '18', '2020-06-11 16:09:46');
INSERT INTO `user_session` VALUES ('34', null, null, null, null, null, '19', '2020-06-11 16:10:04');

-- ----------------------------
-- Table structure for user_vip
-- ----------------------------
DROP TABLE IF EXISTS `user_vip`;
CREATE TABLE `user_vip` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `vip_id` int(11) DEFAULT NULL COMMENT 'vip_id',
  `start_time` datetime DEFAULT NULL COMMENT '购买时间',
  `end_time` datetime DEFAULT NULL COMMENT '到期时间',
  `status` int(255) DEFAULT NULL COMMENT '状态',
  `spare1` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `spare2` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=136 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_vip
-- ----------------------------
INSERT INTO `user_vip` VALUES ('1', '1', '1', '2020-06-10 17:51:52', '2021-06-10 17:51:52', '1', '70418989711310281', null);
INSERT INTO `user_vip` VALUES ('118', '2', '1', '2020-06-09 11:37:41', '2021-06-09 11:37:41', '1', '0013302598103866', null);
INSERT INTO `user_vip` VALUES ('120', '3', '2', '2020-06-09 15:22:29', '2020-07-17 15:22:34', '1', '0013302598103466', null);
INSERT INTO `user_vip` VALUES ('125', '21', '7', null, null, '1', null, null);
INSERT INTO `user_vip` VALUES ('126', '22', '7', null, null, '1', null, null);
INSERT INTO `user_vip` VALUES ('127', '23', '7', null, null, '1', null, null);
INSERT INTO `user_vip` VALUES ('128', '24', '7', null, null, '1', null, null);
INSERT INTO `user_vip` VALUES ('129', '25', '7', null, null, '1', null, null);
INSERT INTO `user_vip` VALUES ('130', '26', '7', null, null, '1', null, null);
INSERT INTO `user_vip` VALUES ('132', '27', '1', '2020-06-11 16:25:59', '2021-06-11 16:25:59', '1', '12316174561454431', null);
INSERT INTO `user_vip` VALUES ('133', '28', '1', '2020-06-11 16:00:32', '2021-06-11 16:00:32', '1', '89026416755774151', null);
INSERT INTO `user_vip` VALUES ('134', '29', '7', null, null, '1', null, null);
INSERT INTO `user_vip` VALUES ('135', '30', '7', null, null, '1', null, null);

-- ----------------------------
-- Table structure for vip
-- ----------------------------
DROP TABLE IF EXISTS `vip`;
CREATE TABLE `vip` (
  `id` int(255) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `grade` int(11) DEFAULT NULL COMMENT '等级',
  `name` varchar(255) DEFAULT NULL COMMENT 'VIP名字',
  `capacity` int(11) DEFAULT NULL COMMENT '容量',
  `speed` int(11) DEFAULT NULL COMMENT '速度',
  `price` double DEFAULT NULL COMMENT '价格',
  `status` int(255) DEFAULT NULL COMMENT '0位包月，1位包季，2位包年',
  `spare1` varchar(255) DEFAULT NULL,
  `spare2` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of vip
-- ----------------------------
INSERT INTO `vip` VALUES ('1', '1', '包年', '20', '-1', '264', null, '10240', '');
INSERT INTO `vip` VALUES ('2', '1', '包季', '20', '-1', '69', null, '10240', null);
INSERT INTO `vip` VALUES ('3', '1', '包月', '20', '-1', '25', null, '10240', null);
INSERT INTO `vip` VALUES ('4', '2', '包年', '10', '300', '144', null, '5120', null);
INSERT INTO `vip` VALUES ('5', '2', '包季', '10', '300', '42', null, '5120', '');
INSERT INTO `vip` VALUES ('6', '2', '包月', '10', '300', '15', null, '5120', '');
INSERT INTO `vip` VALUES ('7', '0', '永久', '5', '100', '0', null, '5120', null);
