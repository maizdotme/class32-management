/*
Navicat MySQL Data Transfer

Source Server         : mysql-01
Source Server Version : 50644
Source Host           : localhost:3306
Source Database       : fresh_market

Target Server Type    : MYSQL
Target Server Version : 50644
File Encoding         : 65001

Date: 2020-06-12 16:01:13
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for address
-- ----------------------------
DROP TABLE IF EXISTS `address`;
CREATE TABLE `address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `recipient` varchar(255) DEFAULT NULL COMMENT '收货人',
  `tel` varchar(255) DEFAULT NULL COMMENT '电话',
  `default_status` int(255) DEFAULT NULL COMMENT '是否为默认地址：1是0否',
  `user_id` int(11) DEFAULT NULL,
  `province_name` varchar(255) DEFAULT NULL COMMENT '省',
  `city_name` varchar(255) DEFAULT NULL COMMENT '市',
  `county_name` varchar(255) DEFAULT NULL COMMENT '区',
  `detail_info` varchar(255) DEFAULT NULL COMMENT '详细地址',
  `is_del` int(11) DEFAULT NULL COMMENT '是否删除',
  `add_time` datetime DEFAULT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of address
-- ----------------------------
INSERT INTO `address` VALUES ('1', '陈刚', '17623186681', '1', '1', '天津市', '市辖区', '和平区', '龙山路73号', '0', '2020-06-10 14:51:43');
INSERT INTO `address` VALUES ('2', '陈刚', '17623186678', '0', '1', '重庆市', '市辖区', '江北区', '盘溪路416号', '0', '2020-06-10 14:05:34');
INSERT INTO `address` VALUES ('3', '陈伟', '17623186662', '1', '3', null, '重庆', '北碚区', '云华路500号', '0', '2020-04-29 21:12:43');
INSERT INTO `address` VALUES ('4', '郭颖', '17623186688', '1', '4', null, '重庆', '沙坪坝区', '西盛路66号', '0', '2020-04-30 21:12:47');
INSERT INTO `address` VALUES ('5', '姜明', '17623186686', '1', '5', null, '重庆', '九龙坡区', '九滨路2号', '0', '2020-05-02 21:12:52');
INSERT INTO `address` VALUES ('6', '江涛', '17623186664', '1', '6', null, '重庆', '南岸区', '亚太路1号', '0', '2020-05-10 21:12:56');
INSERT INTO `address` VALUES ('7', '李刚', '17623186691', '1', '7', null, '重庆', '巴南区', '巴滨路888号', '0', '2020-05-12 21:13:00');
INSERT INTO `address` VALUES ('8', '李军', '17623186699', '1', '8', '', '重庆', '大渡口区', '思源路20号', '0', '2020-06-10 21:13:04');
INSERT INTO `address` VALUES ('9', '李军', '17623186695', '0', '8', null, '重庆', '渝中区', '医学院路1号', '0', '2020-05-03 21:13:08');
INSERT INTO `address` VALUES ('10', '李军', '17623186698', '0', '8', null, '重庆', '渝北区', '松石支路6号', '0', '2020-02-11 21:13:14');
INSERT INTO `address` VALUES ('11', '安伟东', '13364016535', '1', '1', '北京市', '市辖区', '东城区', '王府井', '0', '2020-06-12 14:19:09');
INSERT INTO `address` VALUES ('12', 'guoguo', '13970471234', '0', '1', '重庆市', '市辖区', '沙坪坝区', 'uuu', '0', '2020-06-12 14:19:21');
INSERT INTO `address` VALUES ('13', '安伟东', '13364016535', '1', '17', '北京市', '市辖区', '东城区', '王府井', '0', '2020-06-12 14:23:15');
INSERT INTO `address` VALUES ('14', '张三', '13312344321', '1', '16', '重庆市', '市辖区', '江北区', '理想大厦', '0', '2020-06-12 14:23:34');
INSERT INTO `address` VALUES ('15', 'gg', '13812345678', '0', '18', '重庆市', '市辖区', '沙坪坝区', 'gg', '0', '2020-06-12 14:23:35');
INSERT INTO `address` VALUES ('16', 'hh', '15112345678', '1', '18', '重庆市', '市辖区', '沙坪坝区', 'jj', '0', '2020-06-12 14:23:53');
INSERT INTO `address` VALUES ('17', '刘乾广', '15902381845', '0', '17', '重庆市', '市辖区', '沙坪坝区', '垃圾站', '0', '2020-06-12 14:28:47');
INSERT INTO `address` VALUES ('18', '李思', '13378963541', '1', '20', '重庆市', '市辖区', '江北区', '红旗河沟', '0', '2020-06-12 14:44:24');
INSERT INTO `address` VALUES ('19', '狂少', '13232115863', '1', '24', '重庆市', '市辖区', '沙坪坝区', '红旗河沟理想大厦a栋4楼', '0', '2020-06-12 14:57:13');

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '管理员',
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES ('1', 'admin', 'admin');

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `uri` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL COMMENT '父级菜单,0位最高级',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES ('1', '新鲜蔬菜', 'http://qaw8ata12.bkt.clouddn.com/%E7%94%9F%E9%B2%9C-%E8%94%AC%E8%8F%9C.png', '0');
INSERT INTO `category` VALUES ('2', '肉蛋食材', 'http://qaw8ata12.bkt.clouddn.com/%E7%94%9F%E9%B2%9C-%E8%82%89%E7%B1%BB.png', '0');
INSERT INTO `category` VALUES ('3', '酒饮冲调', 'http://qaw8ata12.bkt.clouddn.com/%E9%85%92%E6%B0%B4.png', '0');
INSERT INTO `category` VALUES ('4', '休闲零食', 'http://qaw8ata12.bkt.clouddn.com/%E4%BC%91%E9%97%B2%E9%9B%B6%E9%A3%9F.png', '0');
INSERT INTO `category` VALUES ('5', '安心乳品', 'http://qaw8ata12.bkt.clouddn.com/%E7%89%9B%E5%A5%B6%E4%B9%B3%E5%93%81.png', '0');
INSERT INTO `category` VALUES ('6', '菌菇类', 'http://qaw8ata12.bkt.clouddn.com/%E3%80%90%E9%98%BF%E6%9E%9C%E7%93%9C%E7%93%9C%E3%80%91%E5%9B%BE%E6%A0%87-%E8%8F%8C%E7%B1%BB-01.png', '0');
INSERT INTO `category` VALUES ('7', '粮油速食', 'http://qaw8ata12.bkt.clouddn.com/%E9%80%9F%E9%A3%9F.png', '0');
INSERT INTO `category` VALUES ('8', '坚果炒货', 'http://qaw8ata12.bkt.clouddn.com/%E5%9D%9A%E6%9E%9C.png', '0');

-- ----------------------------
-- Table structure for coupon
-- ----------------------------
DROP TABLE IF EXISTS `coupon`;
CREATE TABLE `coupon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `coupon_template_id` int(11) DEFAULT NULL COMMENT '优惠券模版id',
  `start_time` datetime DEFAULT NULL COMMENT '领取时间',
  `end_time` datetime DEFAULT NULL COMMENT '领取结束时间',
  `total_count` int(11) DEFAULT NULL COMMENT '总数量',
  `remain_count` int(11) DEFAULT NULL COMMENT '剩余数量',
  `price` double DEFAULT NULL COMMENT '优惠券金额',
  `time` int(11) DEFAULT NULL COMMENT '有效天数',
  `kind` int(255) DEFAULT NULL COMMENT '0代表全平台1代表分类2代表某产品id',
  `kind_details` int(11) DEFAULT NULL COMMENT '配合kind使用',
  `status` int(11) DEFAULT NULL COMMENT '0关闭1开启',
  `coupon_name` varchar(255) DEFAULT NULL COMMENT '通用券名称',
  `type` int(255) DEFAULT NULL COMMENT '类型:0无门槛1满减',
  `conditions` double(255,0) DEFAULT NULL COMMENT '条件， 满减时 为满减金额，无门槛为null',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of coupon
-- ----------------------------
INSERT INTO `coupon` VALUES ('1', '1', '2020-05-25 21:00:21', '2020-12-25 21:00:44', '999999', '999999', '15', '10', '0', '0', '1', '新人优惠券', '0', '0');
INSERT INTO `coupon` VALUES ('2', '2', '2020-06-05 10:41:11', '2020-06-13 10:41:23', '9999', '999', '20', '10', '0', '0', '1', '满减折扣', '1', '100');

-- ----------------------------
-- Table structure for coupon_issue
-- ----------------------------
DROP TABLE IF EXISTS `coupon_issue`;
CREATE TABLE `coupon_issue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '领劵用户id',
  `add_time` datetime DEFAULT NULL COMMENT '领取时间',
  `coupon_sn` varchar(255) DEFAULT NULL COMMENT '优惠券编号',
  `coupon_id` int(11) DEFAULT NULL COMMENT '优惠券id',
  `status` int(255) DEFAULT NULL COMMENT '0过期1正常2已使用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of coupon_issue
-- ----------------------------
INSERT INTO `coupon_issue` VALUES ('15', '2', '2020-06-12 10:55:32', 'e5e5b56832134b059078638230f4477a', '1', '2');
INSERT INTO `coupon_issue` VALUES ('16', '18', '2020-06-12 14:16:57', '2e0fa28992d64c2999443b83ef09729e', '1', '1');
INSERT INTO `coupon_issue` VALUES ('17', '16', '2020-06-12 14:23:48', '13827b222b52422aa7ffa2ef9dd37487', '1', '2');
INSERT INTO `coupon_issue` VALUES ('18', '20', '2020-06-12 14:39:26', '7418a840dc4c48bc9163393415d0f004', '2', '1');
INSERT INTO `coupon_issue` VALUES ('19', '20', '2020-06-12 14:43:32', '483abf5131ed4882b0e449c6dd00a768', '1', '1');
INSERT INTO `coupon_issue` VALUES ('20', '18', '2020-06-12 15:00:38', '3559ba60f6a449d592ed81e7879698ee', '2', '1');
INSERT INTO `coupon_issue` VALUES ('21', '17', '2020-06-12 15:00:40', 'fe339d896e05477d836aae602c4f413e', '2', '1');
INSERT INTO `coupon_issue` VALUES ('22', '17', '2020-06-12 15:00:43', '09c507341b97402298881d127e25a089', '1', '1');

-- ----------------------------
-- Table structure for coupon_template
-- ----------------------------
DROP TABLE IF EXISTS `coupon_template`;
CREATE TABLE `coupon_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '模版名称',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `time` int(11) DEFAULT NULL COMMENT '有效天数',
  `type` varchar(255) DEFAULT NULL COMMENT '类型:0无条件1满减2打折',
  `conditions` double(255,0) DEFAULT NULL COMMENT '条件，配合类型使用',
  `status` int(255) DEFAULT NULL COMMENT '0关闭1开启',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of coupon_template
-- ----------------------------
INSERT INTO `coupon_template` VALUES ('1', '无门槛券', '2020-05-25 20:59:31', '10', '0', '0', '1');
INSERT INTO `coupon_template` VALUES ('2', '满减卷', '2020-05-30 10:40:46', '10', '0', '100', '0');

-- ----------------------------
-- Table structure for coupon_use
-- ----------------------------
DROP TABLE IF EXISTS `coupon_use`;
CREATE TABLE `coupon_use` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `coupon_id` int(11) DEFAULT NULL COMMENT '优惠券id',
  `coupon_sn` varchar(255) DEFAULT NULL COMMENT '优惠券编号',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `use_time` datetime DEFAULT NULL COMMENT '使用时间',
  `coupon_price` double DEFAULT NULL COMMENT '优惠券价格',
  `order_sn` varchar(255) DEFAULT NULL COMMENT '订单编号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of coupon_use
-- ----------------------------

-- ----------------------------
-- Table structure for goods
-- ----------------------------
DROP TABLE IF EXISTS `goods`;
CREATE TABLE `goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `price` double(255,2) DEFAULT NULL,
  `retail_price` double(255,2) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `unit_name` varchar(255) DEFAULT NULL,
  `stock` int(255) DEFAULT NULL,
  `sales` int(11) DEFAULT NULL,
  `is_hot` int(11) DEFAULT '1',
  `is_new` int(11) DEFAULT '1',
  `c_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  `groupby_status` int(11) DEFAULT '0',
  `seckill_status` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of goods
-- ----------------------------
INSERT INTO `goods` VALUES ('1', '现挖现发 沙地小紫薯5斤装 香甜粉糯', '21.90', '19.90', '香甜糯', '5斤/份', '24', '8', '1', '1', '1', '1', '1', '0');
INSERT INTO `goods` VALUES ('2', '沙瓤西红柿 番茄自然熟 3斤 现摘现发', '29.90', '23.90', '酸甜可口', '三斤/箱', '60', '23', '1', '1', '1', '1', '1', '0');
INSERT INTO `goods` VALUES ('3', '美淘时光初生蛋30枚', '35.90', '33.90', '口感绵糯，蛋黄鲜香', '30枚/箱', '100', '11', '1', '1', '2', '1', '0', '1');
INSERT INTO `goods` VALUES ('4', '恒都进口牛腱子1kg*2', '179.90', '159.90', '自然放养，肉香浓郁', '2kg/箱', '18', '2', '0', '1', '2', '1', '0', '0');
INSERT INTO `goods` VALUES ('5', '爱丽丝桃红葡萄酒750ml', '88.00', '79.00', '网红美酒', '750ml/瓶', '198', '23', '1', '1', '3', '1', '0', '0');
INSERT INTO `goods` VALUES ('6', '名仁 苏打水375ml*24瓶', '59.90', '55.90', '弱碱0脂肪', '24瓶/箱', '98', '11', '1', '1', '3', '1', '0', '0');
INSERT INTO `goods` VALUES ('7', '享食者 山楂条108g/袋', '12.90', '11.90', '享食者 山楂条108g/袋', '108g/袋', '499', '124', '1', '1', '4', '1', '0', '0');
INSERT INTO `goods` VALUES ('8', '香当当肉松饼1000g约30个', '20.90', '19.90', '饱满肉松，可甜可咸', '1000g/箱', '50', '5', '1', '1', '4', '1', '0', '0');
INSERT INTO `goods` VALUES ('9', '香当当盐津橄榄258g*1袋', '12.90', '11.90', '老少咸宜', '258g/袋', '100', '10', '0', '1', '4', '1', '0', '0');
INSERT INTO `goods` VALUES ('10', '豆本豆豆奶250ml*12', '35.90', '29.90', '豆香浓郁', '12盒/箱', '50', '30', '1', '1', '5', '1', '0', '0');
INSERT INTO `goods` VALUES ('11', '现挖高山土豆 约1500g', '6.20', '4.49', '又大又圆', '1500g/份', '299', '50', '0', '1', '1', '1', '0', '0');
INSERT INTO `goods` VALUES ('12', '云南紫皮洋葱5斤', '15.90', '12.90', '云白相间 色泽艳丽 大小均匀', '5斤/箱', '799', '236', '1', '1', '1', '1', '0', '0');
INSERT INTO `goods` VALUES ('13', '脆甜水果小黄瓜5斤装', '29.90', '22.90', '瓜香四溢 崔嫩皮薄 汁水丰盈', '5斤/箱', '2000', '976', '1', '1', '1', '1', '0', '0');
INSERT INTO `goods` VALUES ('14', '小牛凯西澳洲进口眼肉牛排套餐 10片装 赠送刀叉+黑胡椒', '149.00', '139.00', '进口原肉 手工整切 口感韧强', '10片/组', '1500', '805', '1', '1', '2', '1', '0', '0');
INSERT INTO `goods` VALUES ('15', '吉林松原黑糯玉米10根装约2kg', '39.90', '37.90', '营养丰富 香甜可口 最易鲜食', '2kg/袋', '1196', '910', '1', '1', '1', '1', '0', '0');
INSERT INTO `goods` VALUES ('16', '山东贝贝南瓜（国产）250克起3斤装', '27.80', '26.80', '南瓜中的 玛莎拉蒂 老熟粉糯', '3斤/箱', '1000', '585', '1', '1', '1', '1', '0', '0');
INSERT INTO `goods` VALUES ('17', '湖北恩施黄心高山洋芋马尔科迷你小土豆2.5kg', '31.00', '22.90', '富晒 小土豆 淀粉充足', '2.5kg/箱', '800', '510', '1', '1', '1', '1', '0', '0');
INSERT INTO `goods` VALUES ('18', '古寺郎 胡萝卜精选800g单根160g以上', '31.90', '27.90', '农业部认定绿色达标食品', '800g/箱', '200', '120', '1', '1', '1', '1', '0', '0');
INSERT INTO `goods` VALUES ('19', '龙大肉食 精品五花肉块500g （买一送一）实发2袋', '99.00', '98.00', '由精选带皮五花肉切割而成免洗切烹饪方便', '500g/组', '500', '367', '1', '1', '2', '1', '0', '0');
INSERT INTO `goods` VALUES ('20', '非转水果玉米8根*260g普通装', '45.90', '40.90', '皮薄 汁多 质脆而甜', '2kg/箱', '300', '272', '1', '1', '1', '1', '0', '0');

-- ----------------------------
-- Table structure for goods_img
-- ----------------------------
DROP TABLE IF EXISTS `goods_img`;
CREATE TABLE `goods_img` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uri` varchar(255) DEFAULT NULL,
  `goods_id` int(11) DEFAULT NULL,
  `title` int(11) DEFAULT NULL COMMENT '1为首页展示、0为商品详情轮播图',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of goods_img
-- ----------------------------
INSERT INTO `goods_img` VALUES ('1', 'https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=3301483224,1552578290&fm=26&gp=0.jpg', '1', '1');
INSERT INTO `goods_img` VALUES ('2', 'https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=3547028188,3485039032&fm=26&gp=0.jpg', '2', '1');
INSERT INTO `goods_img` VALUES ('3', 'https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=96896619,2399842093&fm=26&gp=0.jpg', '3', '1');
INSERT INTO `goods_img` VALUES ('4', 'https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=1260648153,2037098308&fm=26&gp=0.jpg', '4', '1');
INSERT INTO `goods_img` VALUES ('5', 'https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=3938600266,1720055558&fm=26&gp=0.jpg', '5', '1');
INSERT INTO `goods_img` VALUES ('6', 'https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=1136692519,4291711211&fm=26&gp=0.jpg', '6', '1');
INSERT INTO `goods_img` VALUES ('7', 'https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=2648696946,256870674&fm=15&gp=0.jpg', '7', '1');
INSERT INTO `goods_img` VALUES ('8', 'https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=3592059148,1303460508&fm=26&gp=0.jpg', '8', '1');
INSERT INTO `goods_img` VALUES ('9', 'https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=1301382423,3914412287&fm=26&gp=0.jpg', '9', '1');
INSERT INTO `goods_img` VALUES ('10', 'https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=3729726951,3490244173&fm=26&gp=0.jpg', '10', '1');
INSERT INTO `goods_img` VALUES ('11', 'https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=1259512507,3423651803&fm=26&gp=0.jpg', '1', '0');
INSERT INTO `goods_img` VALUES ('12', 'https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=2854509990,904020969&fm=26&gp=0.jpg', '1', '0');
INSERT INTO `goods_img` VALUES ('13', 'https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=1276857883,186916467&fm=26&gp=0.jpg', '2', '0');
INSERT INTO `goods_img` VALUES ('14', 'http://qaw8ata12.bkt.clouddn.com/%E7%8E%B0%E6%8C%96%E5%9C%9F%E8%B1%86.jpg', '11', '1');
INSERT INTO `goods_img` VALUES ('15', 'http://qaw8ata12.bkt.clouddn.com/%E6%B4%8B%E8%91%B1.jpg', '12', '1');
INSERT INTO `goods_img` VALUES ('16', 'http://qaw8ata12.bkt.clouddn.com/%E8%84%86%E7%94%9C%E6%B0%B4%E6%9E%9C%E5%B0%8F%E9%BB%84%E7%93%9C.jpg', '13', '1');
INSERT INTO `goods_img` VALUES ('17', 'https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=2183294904,2874744629&fm=26&gp=0.jpg', '14', '1');
INSERT INTO `goods_img` VALUES ('18', 'https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=3961050646,3941565213&fm=15&gp=0.jpg', '15', '1');
INSERT INTO `goods_img` VALUES ('19', 'https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=2467636205,453856851&fm=26&gp=0.jpg', '16', '1');
INSERT INTO `goods_img` VALUES ('20', 'https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=3717014536,1782396496&fm=26&gp=0.jpg', '17', '1');
INSERT INTO `goods_img` VALUES ('21', 'https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=2577106136,1413341028&fm=15&gp=0.jpg', '18', '1');
INSERT INTO `goods_img` VALUES ('22', 'https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=1810901680,1219172974&fm=11&gp=0.jpg', '19', '1');
INSERT INTO `goods_img` VALUES ('23', 'https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=2804028598,2130207313&fm=26&gp=0.jpg', '20', '1');

-- ----------------------------
-- Table structure for groupby
-- ----------------------------
DROP TABLE IF EXISTS `groupby`;
CREATE TABLE `groupby` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `groupby_sn` varchar(32) COLLATE utf8_bin DEFAULT NULL COMMENT '拼团编号',
  `header` int(11) DEFAULT NULL COMMENT '0位普通人员,1为团长',
  `add_time` datetime DEFAULT NULL COMMENT '拼团开始时间',
  `status` int(11) DEFAULT NULL COMMENT '0拼团失败 1拼团中 2拼团成功',
  `groupby_goods_id` int(11) DEFAULT NULL COMMENT '拼团商品id',
  `current_count` int(11) DEFAULT NULL COMMENT '当前拼团人数',
  `people_count` int(11) DEFAULT NULL COMMENT '团需要人数',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of groupby
-- ----------------------------

-- ----------------------------
-- Table structure for groupby_goods
-- ----------------------------
DROP TABLE IF EXISTS `groupby_goods`;
CREATE TABLE `groupby_goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `start_time` datetime DEFAULT NULL COMMENT '拼团开始时间',
  `end_time` datetime DEFAULT NULL COMMENT '拼团结束时间',
  `goods_id` int(11) DEFAULT NULL COMMENT '商品id',
  `retail_price` double DEFAULT NULL COMMENT '拼团价格',
  `sales` int(11) DEFAULT NULL COMMENT '销量',
  `stock` int(11) DEFAULT NULL COMMENT '库存',
  `add_time` datetime DEFAULT NULL COMMENT '拼团商品添加时间',
  `status` int(11) DEFAULT NULL COMMENT '0拼团结束1拼团开启',
  `people_count` int(11) DEFAULT NULL COMMENT '拼团人数',
  `brokerage_price` double DEFAULT NULL COMMENT '佣金',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of groupby_goods
-- ----------------------------
INSERT INTO `groupby_goods` VALUES ('4', '2020-06-09 09:50:32', '2020-06-17 09:50:34', '1', '18', '44', '19', '2020-06-09 09:50:54', '1', '2', '1');
INSERT INTO `groupby_goods` VALUES ('5', '2020-06-09 09:51:06', '2020-06-16 09:51:09', '2', '20', '6', '19', '2020-06-08 09:51:20', '1', '4', '2');

-- ----------------------------
-- Table structure for order
-- ----------------------------
DROP TABLE IF EXISTS `order`;
CREATE TABLE `order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address_id` int(11) DEFAULT NULL COMMENT '收货地址id',
  `user_id` int(11) DEFAULT NULL COMMENT '收货人',
  `status` int(11) DEFAULT NULL COMMENT '订单状态：0.已取消1.待付款2.待发货 3.待收货.4.已签收',
  `order_sn` varchar(32) DEFAULT NULL COMMENT '订单编号',
  `groupby_sn` varchar(32) DEFAULT NULL COMMENT '拼团编号，null 为正常订单',
  `coupon_id` int(11) DEFAULT NULL COMMENT '优惠券id，0为未使用',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `pay_type` int(11) DEFAULT NULL COMMENT '支付方式：0钱包、1支付宝',
  `pay_time` datetime DEFAULT NULL COMMENT '支付时间',
  `ship_type` int(11) DEFAULT NULL COMMENT '0自提1配送',
  `total_price` double DEFAULT NULL COMMENT '订单总价',
  `pay_price` double DEFAULT NULL COMMENT '实际付款',
  `deduction_price` double DEFAULT NULL COMMENT '抵扣金额',
  `delivery_name` varchar(64) DEFAULT NULL COMMENT '快递名称/送货人姓名',
  `delivery_type` varchar(32) DEFAULT NULL COMMENT '发货类型',
  `delivery_id` varchar(64) DEFAULT NULL COMMENT '快递单号',
  `store_id` int(11) DEFAULT '0' COMMENT '门店id，0为快递形式',
  `province_name` varchar(255) DEFAULT NULL COMMENT '省',
  `city_name` varchar(255) DEFAULT NULL COMMENT '市',
  `county_name` varchar(255) DEFAULT NULL COMMENT '区',
  `detail_info` varchar(255) DEFAULT NULL COMMENT '详细地址',
  `tel` varchar(25) DEFAULT NULL COMMENT '联系人电话',
  `seckill_id` int(11) DEFAULT '0' COMMENT '秒杀产品id',
  `groupby_goods_id` int(11) DEFAULT '0' COMMENT '拼团商品id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=163 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of order
-- ----------------------------

-- ----------------------------
-- Table structure for order_goods
-- ----------------------------
DROP TABLE IF EXISTS `order_goods`;
CREATE TABLE `order_goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `goods_id` int(11) DEFAULT NULL COMMENT '商品id',
  `order_id` int(11) DEFAULT NULL,
  `number` int(11) DEFAULT NULL COMMENT '商品数量',
  `img_url` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '指向商品太图片',
  `price` double DEFAULT NULL COMMENT '商品单价',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=123 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of order_goods
-- ----------------------------

-- ----------------------------
-- Table structure for recommend
-- ----------------------------
DROP TABLE IF EXISTS `recommend`;
CREATE TABLE `recommend` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `record` varchar(255) DEFAULT NULL COMMENT '搜索记录',
  `status` int(11) DEFAULT '1' COMMENT '0不显示1显示',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of recommend
-- ----------------------------

-- ----------------------------
-- Table structure for seckill
-- ----------------------------
DROP TABLE IF EXISTS `seckill`;
CREATE TABLE `seckill` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `start_time` datetime DEFAULT NULL COMMENT '秒杀开始时间',
  `end_time` datetime DEFAULT NULL COMMENT '秒杀结束时间',
  `goods_id` int(11) DEFAULT NULL COMMENT '商品id',
  `retail_price` double DEFAULT NULL COMMENT '秒杀价格',
  `sales` int(11) DEFAULT NULL COMMENT '销量',
  `stock` int(11) DEFAULT NULL COMMENT '库存',
  `add_time` datetime DEFAULT NULL COMMENT '秒杀商品添加时间',
  `status` int(11) DEFAULT '1' COMMENT '0秒杀结束1秒杀开启',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of seckill
-- ----------------------------
INSERT INTO `seckill` VALUES ('2', '2020-06-03 00:19:01', '2020-06-13 00:19:04', '3', '30', '21', '18', '2020-06-01 00:19:37', '1');

-- ----------------------------
-- Table structure for shopcart
-- ----------------------------
DROP TABLE IF EXISTS `shopcart`;
CREATE TABLE `shopcart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of shopcart
-- ----------------------------

-- ----------------------------
-- Table structure for shopcart_goods
-- ----------------------------
DROP TABLE IF EXISTS `shopcart_goods`;
CREATE TABLE `shopcart_goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `goods_id` int(11) DEFAULT NULL,
  `shopcart_id` int(11) DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of shopcart_goods
-- ----------------------------

-- ----------------------------
-- Table structure for store
-- ----------------------------
DROP TABLE IF EXISTS `store`;
CREATE TABLE `store` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL COMMENT '门店名称',
  `phone` char(25) DEFAULT NULL COMMENT '电话',
  `latitude` char(50) DEFAULT NULL COMMENT '维度',
  `logitude` varchar(255) DEFAULT NULL COMMENT '经度',
  `add_time` datetime DEFAULT NULL COMMENT '添加时间',
  `detailed_address` varchar(255) DEFAULT NULL COMMENT '详细地址',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of store
-- ----------------------------
INSERT INTO `store` VALUES ('1', '一门店', '17023145236', '29.588494599999997', '106.5222772', '2020-06-02 20:04:55', '红旗河沟理想大厦对面');
INSERT INTO `store` VALUES ('2', '二门店', '17023145236', '29.588994599999997', '106.5222772', '2020-06-02 20:06:41', '红旗河沟加州花园小区');
INSERT INTO `store` VALUES ('3', '三门店', '17023145236', '29.588994599999997', '106.5210772', '2020-06-02 20:08:32', '红旗河沟理想大厦旁');

-- ----------------------------
-- Table structure for swiper
-- ----------------------------
DROP TABLE IF EXISTS `swiper`;
CREATE TABLE `swiper` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `img_url` varchar(255) DEFAULT NULL,
  `background_color` varchar(255) DEFAULT NULL COMMENT '背景图颜色要与背景图搭配',
  `status` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of swiper
-- ----------------------------
INSERT INTO `swiper` VALUES ('1', 'http://qaw8ata12.bkt.clouddn.com/banner1.jpg', '#EE635C', '1');
INSERT INTO `swiper` VALUES ('2', 'http://qaw8ata12.bkt.clouddn.com/banner2.jpg', '#cdd7da', '1');
INSERT INTO `swiper` VALUES ('3', 'http://qaw8ata12.bkt.clouddn.com/banner3.jpg', '#cb573c', '1');
INSERT INTO `swiper` VALUES ('4', 'http://qaw8ata12.bkt.clouddn.com/banner4.jpg', '#b74945', '1');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `money` double(255,0) DEFAULT '0',
  `pay_password` varchar(255) DEFAULT NULL,
  `brokerage_price` double(255,0) DEFAULT '0',
  `real_name` varchar(255) DEFAULT NULL,
  `birthday` datetime DEFAULT NULL,
  `card_id` varchar(20) DEFAULT NULL,
  `add_time` datetime DEFAULT NULL,
  `tel` char(11) DEFAULT NULL COMMENT '电话',
  `appId` varchar(255) DEFAULT NULL COMMENT '用于推送的用户id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', '陈刚', '12345', 'https://dss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=3140403455,2984550794&fm=26&gp=0.jpg', '928', '12345', '3', '陈刚', '2002-12-30 00:00:00', '500109200212302618', '2020-05-04 18:26:17', '10086', '29252b41d93b5e285e33dd868997fa17');
INSERT INTO `user` VALUES ('2', '陈强', '23456', null, '1311', '23456', '1', '陈强', '1988-01-25 00:00:00', '500109198801252345', '2020-05-04 18:26:17', null, '5b7a959eed04b954c02e39baf66ad2bf');
INSERT INTO `user` VALUES ('3', '陈伟', '34567', null, '1800', '34567', '1', '陈伟', '1978-07-28 00:00:00', '500109197807283478', '2020-05-04 18:26:17', null, null);
INSERT INTO `user` VALUES ('4', '郭颖', '45678', null, '3801', '45678', '4', '郭颖', '2000-06-03 00:00:00', '500109200006030876', '2020-05-04 18:26:17', null, null);
INSERT INTO `user` VALUES ('5', '姜明', '56789', null, '500', '56789', '5', '姜明', '1992-07-15 00:00:00', '500109199207151237', '2020-05-04 18:26:17', null, null);
INSERT INTO `user` VALUES ('6', '江涛', '67890', null, '999', '67890', '9', '江涛', '1995-02-21 00:00:00', '500109199502210785', '2020-05-04 18:26:17', null, null);
INSERT INTO `user` VALUES ('7', '李刚', '78901', null, '8905', '78901', '5', '李刚', '1996-07-02 00:00:00', '500109199607027854', '2020-05-04 18:26:17', null, null);
INSERT INTO `user` VALUES ('8', '李军', '89012', null, '2001', '89012', '1', '李军', '1966-06-13 00:00:00', '500109196606132785', '2020-05-04 18:26:17', null, null);
INSERT INTO `user` VALUES ('9', '李强', '90123', null, '1500', '90123', '1', '李强', '1977-01-24 00:00:00', '500109197701248653', '2020-05-04 18:26:17', null, null);
INSERT INTO `user` VALUES ('10', '刘刚', '01234', null, '1800', '01234', '0', '刘刚', '1989-04-23 00:00:00', '500109198904230789', '2020-05-04 18:26:17', null, null);
INSERT INTO `user` VALUES ('11', '刘辉', '11235', null, '3801', '11235', '1', '刘辉', '1990-05-06 00:00:00', '500109199005067623', '2020-05-04 18:26:17', null, null);
INSERT INTO `user` VALUES ('12', '王建', '12358', null, '500', '12358', '5', '王建', '1978-06-24 00:00:00', '500109197806242467', '2020-05-04 18:26:17', null, null);
INSERT INTO `user` VALUES ('13', '王勇', '123456', null, '981', '123456', '9', '王勇', '1979-07-09 00:00:00', '500109197907099763', '2020-05-04 18:26:17', null, '5b7a959eed04b954c02e39baf66ad2bf');
INSERT INTO `user` VALUES ('14', '赵斌', '35819', null, '8905', '35819', '5', '赵斌', '1983-08-25 00:00:00', '500109198308252619', '2020-05-04 18:26:17', null, null);
INSERT INTO `user` VALUES ('15', '赵兵', '58134', null, '7895', '58134', '8', '赵兵', '1987-09-27 00:00:00', '500109198709274780', '2020-05-04 18:26:17', null, null);
INSERT INTO `user` VALUES ('16', '张三', '123456', null, '1000', '123456', '0', '张三', null, null, null, '18983284813', '');
INSERT INTO `user` VALUES ('17', '安伟东', '666666', null, '99999999999', '666666', '0', '安伟东', null, null, null, '13364016535', '8c6c348f9cb3df482daaee80180121eb');
INSERT INTO `user` VALUES ('18', 'iguoguo', '111111', null, '9667', '111111', '0', 'guoguo', null, null, null, '15170472672', '29252b41d93b5e285e33dd868997fa17');
INSERT INTO `user` VALUES ('19', 'guoguo1', '111111', null, '99999999', null, '0', 'gg', null, null, null, '15170472672', '29252b41d93b5e285e33dd868997fa17');
INSERT INTO `user` VALUES ('20', '李思', '123456', null, '99999', null, '0', '李思', null, null, null, '18983284813', '5b7a959eed04b954c02e39baf66ad2bf');
INSERT INTO `user` VALUES ('21', 'beiju', '951220', null, '0', null, '0', 'Beiju', null, null, null, '18883147054', '64f5eb9ec3d1ce7ba5d114cfff954447');
INSERT INTO `user` VALUES ('22', '瑞萌萌', 'ruimeng', null, '0', null, '0', '瑞萌萌', null, null, null, '17783700194', null);
INSERT INTO `user` VALUES ('23', '瑞萌萌', 'ruimeng', null, '0', null, '0', '瑞萌萌', null, null, null, '17783700194', null);
INSERT INTO `user` VALUES ('24', '狂少', '123456', null, '0', null, '0', '狂少', null, null, null, '13320204926', '08b17bbefcb63c93f49c667896ad05dc');
INSERT INTO `user` VALUES ('25', '小美', '123456', null, '0', null, '0', '小美', null, null, null, '15730265210', '1cb9edb0916f9e3a35e74603d42fb604');

-- ----------------------------
-- Table structure for user_recharge
-- ----------------------------
DROP TABLE IF EXISTS `user_recharge`;
CREATE TABLE `user_recharge` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `pay_time` datetime DEFAULT NULL,
  `add_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_recharge
-- ----------------------------

-- ----------------------------
-- Table structure for user_token
-- ----------------------------
DROP TABLE IF EXISTS `user_token`;
CREATE TABLE `user_token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `expires_time` datetime DEFAULT NULL,
  `login_ip` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_token
-- ----------------------------
